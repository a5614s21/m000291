﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Web.Models.ViewModels;

namespace Web.Controllers
{
    public static class CultureHelper
    {
        /// <summary>
        /// 取得合法語系名稱(尚未實作則給予預設值)
        /// </summary>
        /// <param name="name">語系名稱 (e.g. en-US)</param>
        public static string GetImplementedCulture(string name)
        {
            // give a default culture just in case
            string cultureName = GetDefaultCulture();

            // check if it's implemented
            if (EnumHelper.TryGetValueFromDescription<LanguageEnum>(name))
                cultureName = name;

            return cultureName;
        }

        /// <summary>
        /// 取得預設 語系名稱
        /// </summary>
        /// <returns></returns>
        public static string GetDefaultCulture()
        {
            //return LanguageEnum.English.GetDescription();
            return LanguageEnum.Chinese.GetDescription();
        }

        /// <summary>
        /// 取得目前 語系
        /// </summary>
        /// <returns></returns>
        public static LanguageEnum GetCurrentLanguage()
        {
            //var route = RouteTable.Routes.GetRouteData(new HttpContextWrapper(HttpContext.Current));
            //var c = route.Values["controller"];
            //var v = route.Values["action"];

            var currentCulture = Thread.CurrentThread.CurrentCulture.Name;

            // get implemented culture name
            currentCulture = GetImplementedCulture(currentCulture);

            // get language by implemented culture name
            return EnumHelper.GetValueFromDescription<LanguageEnum>(currentCulture);
        }

        //取得當前語系
        public static string GetLanguage()
        {
            var lang = "";
            var language = CultureHelper.GetCurrentLanguage();
            switch (language.ToString())
            {
                case "English":
                    lang = "en";
                    break;

                default:
                    lang = "tw";
                    break;
            }

            return lang;
        }

        public static string GetUrlLanguage()
        {
            var lang = "";
            var language = CultureHelper.GetCurrentLanguage();
            switch (language.ToString())
            {
                case "English":
                    lang = "en-US";
                    break;

                default:
                    lang = "zh-TW";
                    break;
            }

            return lang;
        }

        //更換網址的語系
        public static string ReplaceLanguage(string cultrue, string returnUrl)
        {
            if (returnUrl.Contains("zh-TW"))
            {
                returnUrl = returnUrl.Replace("zh-TW", cultrue);
            }
            else if (returnUrl.Contains("en-US"))
            {
                returnUrl = returnUrl.Replace("en-US", cultrue);
            }
            else
            {
                returnUrl = returnUrl + cultrue;
            }

            return returnUrl;
        }
    }

    public static class EnumExtension
    {
        /// <summary>
        /// 取得Enum的描述標籤內容
        /// </summary>
        /// <returns></returns>
        public static string GetDescription(this Enum self)
        {
            FieldInfo fi = self.GetType().GetField(self.ToString());
            DescriptionAttribute[] attributes = null;

            if (fi != null)
            {
                attributes =
                    (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes != null && attributes.Length > 0)
                    return attributes[0].Description;
            }

            return self.ToString();
        }
    }

    public class EnumHelper
    {
        /// <summary>
        /// 透過標籤描述內容反射出Enum數值
        /// </summary>
        /// <typeparam name="T">Enum類別</typeparam>
        /// <param name="description">Enum描述標籤</param>
        /// <returns>Enum數值</returns>
        public static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();

            foreach (var field in type.GetFields())
            {
                var attribute = System.Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }

            throw new ArgumentException("Not found.", "description");
        }

        /// <summary>
        /// 是否能透過標籤描述內容反射出Enum數值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="description"></param>
        /// <returns></returns>
        public static bool TryGetValueFromDescription<T>(string description)
        {
            bool isOkToGetValueFromDescription = true;

            try
            {
                GetValueFromDescription<T>(description);
            }
            catch (Exception)
            { isOkToGetValueFromDescription = false; }

            return isOkToGetValueFromDescription;
        }
    }
}