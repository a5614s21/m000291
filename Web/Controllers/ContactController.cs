﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models.Repository;
using Web.Models.ViewModels;

namespace Web.Controllers
{
    public class ContactController : BaseController
    {
        //銷售據點
        public ActionResult Service()
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            //Model
            ContactViewModel Model = new ContactViewModel();
            PageBannerService PageBanner = new PageBannerService();
            BusinessLocationService BusinessLocation = new BusinessLocationService();

            //Content
            Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Contact_Service").SingleOrDefault();
            Model.BusinessLocation_ListViewModel = BusinessLocation.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

            //Other
            ViewBag.LayoutID = "service";

            if (lang == "en")
            {
                ViewBag.ViewTitle = "Where to Buy | ";
            }
            else
            {
                ViewBag.ViewTitle = "銷售據點 | ";
            }
            return View(Model);
        }

        //線上諮詢
        public ActionResult Inquiry()
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            //Model
            ContactViewModel Model = new ContactViewModel();
            PageBannerService pageBanner = new PageBannerService();
            CompanyInfoService CompanyInfo = new CompanyInfoService();
            ProductDataService ProductData = new ProductDataService();

            //Content
            Model.PageBanner_ViewModel = pageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Contact_Inquiry").SingleOrDefault();
            Model.CompanyInfo_ViewModel = CompanyInfo.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").SingleOrDefault();

            HttpCookie cookie = Request.Cookies["p_guid"];
            List<string> product_guidlist = new List<string>();
            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                product_guidlist = cookie.Value.Split(',').ToList();
            }
            else
            {
                product_guidlist = null;
            }

            Model.ContactProduct_ListViewModel = ProductData.GetContactProduct(lang, product_guidlist);

            //Other
            ViewBag.LayoutID = "service";
            if (lang == "en")
            {
                ViewBag.ViewTitle = "Contact R&R | ";
            }
            else
            {
                ViewBag.ViewTitle = "線上諮詢 | ";
            }
            return View(Model);
        }

        [HttpPost]
        public ActionResult Inquiry(ContactViewModel Model, string verification)
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            //Model
            ContactService Contact = new ContactService();
            PageBannerService pageBanner = new PageBannerService();
            CompanyInfoService CompanyInfo = new CompanyInfoService();
            ProductDataService ProductData = new ProductDataService();

            HttpCookie cookie = Request.Cookies["p_guid"];

            //Post
            if (verification.Equals(Session["_ValCheckCode"].ToString(), StringComparison.OrdinalIgnoreCase) && ModelState.IsValid)
            {
                var success = Contact.InserData(lang, Model.Contact_PostViewModel);
                if (success == true)
                {
                    if (cookie != null)
                    {
                        cookie.Expires = DateTime.Now.AddDays(-7);
                        Response.Cookies.Add(cookie);
                    }

                    string subjectTitle = "";
                    string metaFile_admin = "";
                    string metaFile_user = "";
                    string web_title = "";

                    if (lang == "tw")
                    {
                        subjectTitle = "【萊禮生醫科技股份有限公司】線上諮詢表單";
                        metaFile_admin = "Contact_tw.html";
                        metaFile_user = "Confirmation_tw.html";
                        web_title = "萊禮生醫科技股份有限公司";
                    }
                    else if (lang == "en")
                    {
                        subjectTitle = "【R&R】Contact Form";
                        metaFile_admin = "Contact_en.html";
                        metaFile_user = "Confirmation_en.html";
                        web_title = "R&R Medical Corporation Ltd.";
                    }

                    string sMessage_admin = System.IO.File.ReadAllText(Server.MapPath("~/Content/Mail") + "\\" + metaFile_admin);
                    sMessage_admin = sMessage_admin.Replace("{$web_title}", web_title);
                    sMessage_admin = sMessage_admin.Replace("{$web_url}", "http://" + Request.ServerVariables["Server_Name"]);
                    sMessage_admin = sMessage_admin.Replace("{$use_title}", subjectTitle);
                    Contact.SendMailToAdmin(lang, Model.Contact_PostViewModel, sMessage_admin);

                    string sMessage_user = System.IO.File.ReadAllText(Server.MapPath("~/Content/Mail") + "\\" + metaFile_user);
                    sMessage_user = sMessage_user.Replace("{$web_title}", web_title);
                    sMessage_user = sMessage_user.Replace("{$web_url}", "http://" + Request.ServerVariables["Server_Name"]);
                    sMessage_user = sMessage_user.Replace("{$use_title}", subjectTitle);
                    Contact.SendMailToUser(lang, Model.Contact_PostViewModel, sMessage_user);

                    return RedirectToAction("Inquiry_OK");
                }
                else
                {
                    ViewBag.isError = "InsertError";
                }
            }
            else
            {
                ViewBag.isError = "codeError";
            }

            //Content
            Model.PageBanner_ViewModel = pageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Contact_Inquiry").SingleOrDefault();
            Model.CompanyInfo_ViewModel = CompanyInfo.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").SingleOrDefault();

            List<string> product_guidlist = new List<string>();
            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                product_guidlist = cookie.Value.Split(',').ToList();
            }
            else
            {
                product_guidlist = null;
            }

            Model.ContactProduct_ListViewModel = ProductData.GetContactProduct(lang, product_guidlist);

            //Other
            ViewBag.LayoutID = "service";
            return View(Model);
        }

        [HttpPost]
        public ActionResult DeleteCookie(string guid)
        {
            HttpCookie cookie = Request.Cookies["p_guid"];
            var result = "";
            if (cookie == null)
            {
                result = "success";
            }
            else
            {
                if (cookie.Value.ToString().Contains(guid) == true)
                {
                    var list = cookie.Value.Split(',').ToList();
                    var val = "";
                    foreach (var item in list)
                    {
                        if (item != guid)
                        {
                            val = item + ",";
                        }
                    }

                    cookie.Expires = DateTime.Now.AddDays(-7);
                    Response.Cookies.Add(cookie);

                    if (!string.IsNullOrEmpty(val))
                    {
                        HttpCookie c = new HttpCookie("p_guid");
                        c.Value = val.TrimEnd(',');
                        c.Expires = DateTime.Now.AddDays(7);
                        Response.Cookies.Add(c);
                    }
                }

                result = "success";
            }

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Inquiry_OK()
        {
            //Other
            ViewBag.LayoutID = "inquiry";

            return View();
        }

        public void SendConatctReply(string guid)
        {
            try
            {
                //Model
                ContactService Contact = new ContactService();

                //Content
                var result = Contact.GetAllDataByPredicate(x => x.guid == guid).SingleOrDefault();
                string lang = result.form_lang;

                string subjectTitle = "";
                string metaFile = "";
                string web_title = "";

                if (lang == "tw")
                {
                    subjectTitle = "【萊禮生醫科技股份有限公司】線上諮詢回覆表單";
                    metaFile = "ContactReply_tw.html";
                    web_title = "萊禮生醫科技股份有限公司";
                }
                else if (lang == "en")
                {
                    subjectTitle = "【R&R】Contact Form";
                    metaFile = "ContactReply_en.html";
                    web_title = "R&R Medical Corporation Ltd.";
                }

                string sMessage = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Content/Mail" + "\\" + metaFile));
                sMessage = sMessage.Replace("{$web_title}", web_title);
                sMessage = sMessage.Replace("{$web_url}", "http://" + System.Web.HttpContext.Current.Request.ServerVariables["Server_Name"]);
                sMessage = sMessage.Replace("{$use_title}", subjectTitle);
                Contact.SendContactReplyToUser(lang, result, sMessage);
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }
    }
}