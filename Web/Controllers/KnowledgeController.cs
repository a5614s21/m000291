﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models.Repository;
using Web.Models.ViewModels;
using Web.Service;

namespace Web.Controllers
{
    public class KnowledgeController : BaseController
    {
        //常見問題
        public ActionResult Faq(int? page)
        {
            //Language
            string lang = CultureHelper.GetLanguage();
            string langcode = CultureHelper.GetUrlLanguage();

            //Model
            KnowledgeViewModel Model = new KnowledgeViewModel();
            PageBannerService PageBanner = new PageBannerService();
            KnowledgePageService KnowledgePage = new KnowledgePageService();
            KnowledgeDataService KnowledgeData = new KnowledgeDataService();

            //Content
            Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Knowledge_Faq").SingleOrDefault();
            Model.KnowledgePage_ViewModel = KnowledgePage.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").SingleOrDefault();
            Model.KnowledgeData_ListViewModel = KnowledgeData.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

            if (Model.KnowledgeData_ListViewModel != null && Model.KnowledgeData_ListViewModel.Count != 0)
            {
                var pageNumber = page ?? 1;
                var pageSize = 10;
                Model.KnowledgeData_PagedListViewModel = Model.KnowledgeData_ListViewModel.ToPagedList(pageNumber, pageSize);
                int Allpage = Convert.ToInt16(Math.Ceiling((double)Model.KnowledgeData_ListViewModel.Count / pageSize));
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + langcode + "/Knowledge/Faq?", Allpage, pageNumber, Model.KnowledgeData_ListViewModel.Count, "");
            }

            //Other
            ViewBag.LayoutID = "knowledge";
            if (lang == "en")
            {
                ViewBag.ViewTitle = "FAQs  | ";
            }
            else
            {
                ViewBag.ViewTitle = "常見問題  | ";
            }

            return View(Model);
        }

        //萊禮知識庫
        public ActionResult Lifestyle(int? page)
        {
            //Language
            string lang = CultureHelper.GetLanguage();
            string langcode = CultureHelper.GetUrlLanguage();

            //Model
            KnowledgeViewModel Model = new KnowledgeViewModel();
            PageBannerService PageBanner = new PageBannerService();
            KnowledgeLifeStyleService KnowledgeLifeStyle = new KnowledgeLifeStyleService();

            //Content
            Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Knowledge_Lifestyle").SingleOrDefault();
            Model.KnowledgeLifestyle_ListViewModel = KnowledgeLifeStyle.GetAllData_KnowledgeLifestyle(lang);

            if (Model.KnowledgeLifestyle_ListViewModel != null && Model.KnowledgeLifestyle_ListViewModel.Count != 0)
            {
                var pageNumber = page ?? 1;
                var pageSize = 5;
                Model.KnowledgeLifestyle_PagedListViewModel = Model.KnowledgeLifestyle_ListViewModel.ToPagedList(pageNumber, pageSize);
                int Allpage = Convert.ToInt16(Math.Ceiling((double)Model.KnowledgeLifestyle_ListViewModel.Count / pageSize));
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + langcode + "/Knowledge/Lifestyle?", Allpage, pageNumber, Model.KnowledgeLifestyle_PagedListViewModel.Count, "");
            }

            //Other
            ViewBag.LayoutID = "knowledge";
            if (lang == "en")
            {
                ViewBag.ViewTitle = "Tips of R&R  | ";
            }
            else
            {
                ViewBag.ViewTitle = "萊禮知識庫  | ";
            }
            return View(Model);
        }

        public ActionResult LifestyleData(string guid)
        {
            try
            {
                //Language
                string lang = CultureHelper.GetLanguage();

                //Model
                KnowledgeViewModel Model = new KnowledgeViewModel();
                KnowledgeLifeStyleService KnowledgeLifeStyle = new KnowledgeLifeStyleService();

                //Content
                Model.KnowledgeLifestyle_ViewModel = KnowledgeLifeStyle.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == guid).SingleOrDefault();
                var NextPrev = KnowledgeLifeStyle.GetNextPrev(lang, Model.KnowledgeLifestyle_ViewModel.guid);
                Model.KnowledgeLifestyle_NextViewModel = NextPrev["Next"];
                Model.KnowledgeLifestyle_PrevViewModel = NextPrev["Prev"];

                //Other
                ViewBag.LayoutID = "knowledge";
                ViewBag.ViewTitle = Model.KnowledgeLifestyle_ViewModel.title + " | ";
                return View(Model);
            }
            catch
            {
                return RedirectToAction("List");
            }
        }
    }
}