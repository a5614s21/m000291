﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models.Repository;
using Web.Models.ViewModels;
using Web.Service;

namespace Web.Controllers
{
    public class NewsController : BaseController
    {
        //列表
        public ActionResult List(string category, int? page)
        {
            //Language
            string lang = CultureHelper.GetLanguage();
            string langcode = CultureHelper.GetUrlLanguage();

            //Model
            NewsViewModel Model = new NewsViewModel();
            PageBannerService PageBanner = new PageBannerService();
            NewsCategoryService NewsCategory = new NewsCategoryService();
            NewsService News = new NewsService();

            //Content
            Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "News_List").SingleOrDefault();
            Model.NewsCategory_ListViewModel = NewsCategory.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

            if (!string.IsNullOrEmpty(category))
            {
                Model.NewsCategory_ViewModel = NewsCategory.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == category).SingleOrDefault();
            }
            else
            {
                Model.NewsCategory_ViewModel = Model.NewsCategory_ListViewModel.FirstOrDefault();
                category = Model.NewsCategory_ViewModel.guid;
            }

            Model.News_ListViewModel = News.GetAllDataByCategory(lang, category);

            if (Model.News_ListViewModel != null && Model.News_ListViewModel.Count != 0)
            {
                var pageNumber = page ?? 1;
                var pageSize = 8;
                Model.News_PagedListViewModel = Model.News_ListViewModel.ToPagedList(pageNumber, pageSize);
                int Allpage = Convert.ToInt16(Math.Ceiling((double)Model.News_ListViewModel.Count / pageSize));
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + langcode + "/News/List?category=" + category + "&", Allpage, pageNumber, Model.News_ListViewModel.Count, "");
            }

            //Other
            ViewBag.LayoutID = "news";
            ViewBag.ViewTitle = Model.NewsCategory_ViewModel.title + " | ";
            return View(Model);
        }

        //內頁
        public ActionResult Index(string guid)
        {
            try
            {
                //Language
                string lang = CultureHelper.GetLanguage();

                //Model
                NewsViewModel Model = new NewsViewModel();
                NewsService News = new NewsService();
                NewsCategoryService NewsCategory = new NewsCategoryService();

                //Content
                Model.News_ViewModel = News.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == guid).SingleOrDefault();
                Model.NewsCategory_ViewModel = NewsCategory.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == Model.News_ViewModel.category).SingleOrDefault();

                var NextPrev = News.GetNextPrev(lang, Model.NewsCategory_ViewModel.guid, Model.News_ViewModel.guid);
                Model.News_NextViewModel = NextPrev["Next"];
                Model.News_PrevViewModel = NextPrev["Prev"];

                //Other
                ViewBag.LayoutID = "news";
                ViewBag.ViewTitle = Model.News_ViewModel.title + " | ";
                return View(Model);
            }
            catch
            {
                return RedirectToAction("List");
            }
        }
    }
}