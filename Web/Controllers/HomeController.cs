﻿using PagedList;
using Pechkin;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Models.Interface;
using Web.Models.Repository;
using Web.Models.ViewModels;
using Web.Service;

namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        //首頁
        public ActionResult Index()
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            //Model
            HomeViewModel Model = new HomeViewModel();
            IndexBannerService IndexBanner = new IndexBannerService();
            NewsService News = new NewsService();
            NewsCategoryService NewsCategory = new NewsCategoryService();
            ProductsSeriesService PoductSeries = new ProductsSeriesService();
            VideoDataService VideoData = new VideoDataService();
            IndexDataService IndexData = new IndexDataService();
            WebDataService WebData = new WebDataService();

            //Content
            Model.IndexBanner_ListViewModel = IndexBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
            Model.IndexNewsCategory_ListViewModel = NewsCategory.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
            Model.IndexNews_ListViewModel = News.GetAllIndexData(lang);
            Model.IndexProductSeries_ListViewModel = PoductSeries.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.index_sticky == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
            Model.IndexVideoData_ListViewModel = VideoData.GetAllIndexVideoData(lang);
            Model.IndexData_ViewModel = IndexData.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").SingleOrDefault();
            Model.WebData_ViewModel = WebData.GetAllDataByPredicate(x => x.lang == lang).SingleOrDefault();

            //Other
            ViewBag.LayoutID = "index";
            
            if(!string.IsNullOrEmpty(Model.IndexData_ViewModel.index_newstitle))
            {
                ViewBag.index_newstitle = Model.IndexData_ViewModel.index_newstitle.Replace("<br />", "\n").ToString();
            }

            if(!string.IsNullOrEmpty(Model.IndexData_ViewModel.index_newscontent))
            {
                ViewBag.index_newscontent = Model.IndexData_ViewModel.index_newscontent.Replace("<br />", "\n").ToString();
            }

            

            if (lang == "en")
            {
                ViewBag.ViewTitle = "Home | ";
            }
            else
            {
                ViewBag.ViewTitle = "首頁 | ";
            }

            return View(Model);
        }

        //隱私權政策
        public ActionResult Privacy()
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            //Model
            HomeViewModel Model = new HomeViewModel();
            PrivacyService Privacy = new PrivacyService();

            //Content
            Model.Privacy_ViewModel = Privacy.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").SingleOrDefault();

            //Other
            ViewBag.LayoutID = "privacy";

            if (lang == "en")
            {
                ViewBag.ViewTitle = "Privacy Policy | ";
            }
            else
            {
                ViewBag.ViewTitle = "隱私權政策 | ";
            }
            return View(Model);
        }

        //語系切換
        public ActionResult SetCulture(string culture, string returnUrl)
        {
            returnUrl = CultureHelper.ReplaceLanguage(culture, returnUrl);

            culture = CultureHelper.GetImplementedCulture(culture);

            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];

            if (cookie != null)
            {
                // update cookie value
                cookie.Value = culture;
            }
            else
            {
                // create cookie value
                cookie = new HttpCookie("_culture");
                cookie.HttpOnly = true;
                cookie.Secure = true;
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }

            Response.Cookies.Add(cookie);
            return Redirect(returnUrl);
        }

        public ActionResult Search(string keyword, int? page)
        {
            //Language
            string lang = CultureHelper.GetLanguage();
            string langcode = CultureHelper.GetUrlLanguage();

            //Model
            HomeViewModel Model = new HomeViewModel();
            ProductDataService Product = new ProductDataService();

            //Content
            Model.SearchProduct_ListViewModel = Product.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && (x.title.Contains(keyword) || x.model.Contains(keyword))).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

            if (Model.SearchProduct_ListViewModel != null && Model.SearchProduct_ListViewModel.Count != 0)
            {
                var pageNumber = page ?? 1;
                var pageSize = 12;
                Model.SearchProduct_PagedListViewModel = Model.SearchProduct_ListViewModel.ToPagedList(pageNumber, pageSize);
                int Allpage = Convert.ToInt16(Math.Ceiling((double)Model.SearchProduct_ListViewModel.Count / pageSize));
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + langcode + "/Home/Search?keyword=" + keyword + "&", Allpage, pageNumber, Model.SearchProduct_ListViewModel.Count, "");
            }

            //Other
            ViewBag.keyword = keyword;
            ViewBag.count = Model.SearchProduct_ListViewModel.Count;
            ViewBag.LayoutID = "search";
            if (lang == "en")
            {
                ViewBag.ViewTitle = "Select For Product | ";
            }
            else
            {
                ViewBag.ViewTitle = "關鍵字搜尋 | ";
            }

            return View(Model);
        }

        public ActionResult Print(string guid)
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            //Model
            ProductViewModel Model = new ProductViewModel();
            NotePadDataService NotepadData = new NotePadDataService();

            //Content
            Model.NotepadData_PDFListViewModel = NotepadData.GetPrintDataByNoteGuid(lang, guid);

            ViewBag.year = DateTime.Now.ToString("yyyy");
            ViewBag.month = DateTime.Now.ToString("MM");
            ViewBag.day = DateTime.Now.ToString("dd");

            return View(Model);
        }

        public ActionResult DownloadPDF(string guid)
        {
            //Language
            string lang = CultureHelper.GetLanguage();
            string langcode = CultureHelper.GetUrlLanguage();

            //var url = "http://localhost:14575/" + langcode + "/Home/Print?guid=" + guid;
            //var url = "http://iis.24241872.tw/projects/public/m000291/test/" + langcode + "/Home/Print?guid=" + guid;
            //var url = "http://www.rr-medical.com/" + langcode + "/Home/Print?guid=" + guid;

            var url = FunctionService.getWebUrl() + "/" + langcode + "/Home/Print?giud=" + guid;

            NotePadService Notepad = new NotePadService();

            var notepadta = Notepad.GetAllDataByPredicate(x => x.guid == guid).SingleOrDefault();

            using (IPechkin pechkin = Factory.Create(new GlobalConfig().SetMargins(0, 0, 0, 0).SetPaperSize(PaperKind.A4)))
            {
                ObjectConfig oc = new ObjectConfig();
                oc.SetPrintBackground(true)
                   .SetLoadImages(true)
                   .SetScreenMediaType(true)
                   .SetPageUri(url);

                byte[] pdf = pechkin.Convert(oc);
                return File(pdf, "Application/pdf", notepadta.title + ".pdf");
            }
        }
    }
}