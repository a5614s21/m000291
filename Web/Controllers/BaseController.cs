﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Web.Models.Repository;

namespace Web.Controllers
{
    public class BaseController : Controller
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            //Language
            var lang = CultureHelper.GetLanguage();
            ViewBag.Lang = lang;

            WebDataService WebData = new WebDataService();
            NewsCategoryService NewsCategory = new NewsCategoryService();
            VideoCategoryService VideoCategory = new VideoCategoryService();
            NotePadService Notepad = new NotePadService();

            ViewBag.WebData = WebData.GetAllDataByPredicate(x => x.lang == lang).SingleOrDefault();
            ViewBag.NewsCategory = NewsCategory.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
            ViewBag.VideoCategory = VideoCategory.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

            HttpCookie cookie = Request.Cookies["Notepads"];
            if (cookie != null)
            {
                var cookieData = cookie.Values["NoteGuid"];
                List<string> guidList = JsonConvert.DeserializeObject<List<string>>(cookieData);
                ViewBag.Notepad = Notepad.GetDataByCookie(lang, guidList);
            }
            else
            {
                ViewBag.Notepad = null;
            }
        }
    }
}