﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models.Repository;
using Web.Models.ViewModels;
using Web.Service;

namespace Web.Controllers
{
    public class VideoController : BaseController
    {
        //列表
        public ActionResult List(string category, int? page)
        {
            //Language
            string lang = CultureHelper.GetLanguage();
            string langcode = CultureHelper.GetUrlLanguage();

            //Model
            VideoViewModel Model = new VideoViewModel();
            PageBannerService PageBanner = new PageBannerService();
            VideoCategoryService VideoCategory = new VideoCategoryService();
            VideoDataService VideoData = new VideoDataService();

            //Content
            Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Video_List").SingleOrDefault();
            Model.VideoCategory_ListViewModel = VideoCategory.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

            if (!string.IsNullOrEmpty(category))
            {
                Model.VideoCategory_ViewModel = VideoCategory.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == category).SingleOrDefault();
            }
            else
            {
                Model.VideoCategory_ViewModel = Model.VideoCategory_ListViewModel.FirstOrDefault();
                category = Model.VideoCategory_ViewModel.guid;
            }

            Model.VideoData_ListViewModel = VideoData.GetAllDataByCategory(lang, category);

            if (Model.VideoData_ListViewModel != null && Model.VideoData_ListViewModel.Count != 0)
            {
                var pageNumber = page ?? 1;
                var pageSize = 8;
                Model.VideoData_PagedListViewModel = Model.VideoData_ListViewModel.ToPagedList(pageNumber, pageSize);
                int Allpage = Convert.ToInt16(Math.Ceiling((double)Model.VideoData_ListViewModel.Count / pageSize));
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + langcode + "/Video/List?category=" + category + "&", Allpage, pageNumber, Model.VideoData_ListViewModel.Count, "");
            }

            //Other
            ViewBag.LayoutID = "video";
            ViewBag.ViewTitle = Model.VideoCategory_ViewModel.title + " | ";
            return View(Model);
        }

        //內頁
        public ActionResult Index(string guid)
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            //Model
            VideoViewModel Model = new VideoViewModel();
            VideoDataService VideoData = new VideoDataService();

            //Content
            Model.VideoData_ViewModel = VideoData.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == guid).SingleOrDefault();

            return PartialView("video_lbox", Model);
        }
    }
}