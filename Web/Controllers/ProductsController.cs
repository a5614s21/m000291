﻿using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models.Repository;
using Web.Models.ViewModels;
using Web.Service;

namespace Web.Controllers
{
    public class ProductsController : BaseController
    {
        //產品分類
        public ActionResult Category()
        {

            //Language
            string lang = CultureHelper.GetLanguage();

            ////Model
            //ProductViewModel Model = new ProductViewModel();
            //PageBannerService PageBanner = new PageBannerService();
            //ProductCategoryService ProductCategory = new ProductCategoryService();

            ////Content
            //Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Product_Category").SingleOrDefault();
            //Model.ProductCategory_ListViewModel = ProductCategory.GetAllData(lang);

            //Model
            ProductViewModel Model = new ProductViewModel();
            PageBannerService PageBanner = new PageBannerService();
            ProductsSeriesService ProductSeries = new ProductsSeriesService();

            //Content
            Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Product_Series").SingleOrDefault();
            Model.ProductSeries_ListViewModel = ProductSeries.GetAllData(lang);

            //Other
            ViewBag.LayoutID = "products";

            if (lang == "en")
            {
                ViewBag.ViewTitle = "Product Categories | ";
            }
            else
            {
                ViewBag.ViewTitle = "產品分類 | ";
            }
            return View(Model);
        }

        //全部系列
        public ActionResult Series()
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            ////Model
            //ProductViewModel Model = new ProductViewModel();
            //PageBannerService PageBanner = new PageBannerService();
            //ProductsSeriesService ProductSeries = new ProductsSeriesService();

            ////Content
            //Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Product_Series").SingleOrDefault();
            //Model.ProductSeries_ListViewModel = ProductSeries.GetAllData(lang);

            //Model
            ProductViewModel Model = new ProductViewModel();
            PageBannerService PageBanner = new PageBannerService();
            ProductCategoryService ProductCategory = new ProductCategoryService();

            //Content
            Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Product_Category").SingleOrDefault();
            Model.ProductSeriesCategory_ListViewModel = ProductCategory.GetAllSeriesData(lang);

            //Other
            ViewBag.LayoutID = "products";
            if (lang == "en")
            {
                ViewBag.ViewTitle = "All | ";
            }
            else
            {
                ViewBag.ViewTitle = "全部系列 | ";
            }
            return View(Model);
        }

        //商品列表(類別)
        public ActionResult ProductsList_Category(string category, string brand, int? page)
        {
            //Language
            string lang = CultureHelper.GetLanguage();
            string langcode = CultureHelper.GetUrlLanguage();

            //Model
            ProductViewModel Model = new ProductViewModel();
            ProductCategoryService ProductCategory = new ProductCategoryService();
            ProductBrandService ProductBrand = new ProductBrandService();
            ProductDataService ProductData = new ProductDataService();

            //Content
            Model.ProductCategory_ViewModel = ProductCategory.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == category).SingleOrDefault();
            Model.ProductCategory_ListViewModel = ProductCategory.GetAllData(lang);
            Model.ProductBrand_ListViewModel = ProductBrand.GetAllDataByCategoryData(lang, Model.ProductCategory_ViewModel);

            if (string.IsNullOrEmpty(brand))
            {
                //brand = Model.ProductBrand_ListViewModel.First().guid;
                Model.ProductData_ListViewModel = ProductData.GetAllDataByCategory(lang, category);
            }
            else
            {
                Model.ProductData_ListViewModel = ProductData.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.product_category == category && x.category_brand == brand).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
            }

            if (Model.ProductData_ListViewModel != null && Model.ProductData_ListViewModel.Count != 0)
            {
                var pageNumber = page ?? 1;
                var pageSize = 12;
                Model.ProductData_PagedListViewModel = Model.ProductData_ListViewModel.ToPagedList(pageNumber, pageSize);
                int Allpage = Convert.ToInt16(Math.Ceiling((double)Model.ProductData_ListViewModel.Count / pageSize));
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + langcode + "/Products/ProductsList_Category?category=" + category + "&brand=" + brand + "&", Allpage, pageNumber, Model.ProductData_ListViewModel.Count, "");
            }

            //Other
            ViewBag.LayoutID = "products";
            ViewBag.ViewTitle = Model.ProductCategory_ViewModel.title + " | ";
            return View(Model);
        }

        //商品列表(系列)
        public ActionResult ProductsList_Series(string series, string brand, int? page)
        {
            //Language
            string lang = CultureHelper.GetLanguage();
            string langcode = CultureHelper.GetUrlLanguage();

            //Model
            ProductViewModel Model = new ProductViewModel();
            ProductsSeriesService ProductSeries = new ProductsSeriesService();
            ProductBrandService ProductBrand = new ProductBrandService();
            ProductDataService ProductData = new ProductDataService();

            var tempcategory = ProductSeries.GetJudgeSeries(series);
            if (tempcategory == null)
            {
                var changeseries = ProductSeries.GetChangeSeries(series);
                if (changeseries != null)
                {
                    series = changeseries;
                }
            }

            //Content
            Model.ProductSeries_ViewModel = ProductSeries.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == series).SingleOrDefault();
            Model.ProductSeries_ListViewModel = ProductSeries.GetAllData(lang);
            //Model.ProductBrand_ListViewModel = ProductBrand.GetAllDataBySeriesData(lang, Model.ProductSeries_ViewModel);

            Model.ProductCateSeries_ListViewModel = ProductSeries.GetAllDataByCate(lang,Model.ProductSeries_ViewModel);
            Model.ProductCategoryAll_ViewModel = ProductSeries.GetAllCateData(lang);

            if (string.IsNullOrEmpty(brand))
            {
                Model.ProductData_ListViewModel = ProductData.GetAllDataBySeries(lang, series);
            }
            else
            {
                Model.ProductData_ListViewModel = ProductData.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.product_series == series && x.series_brand == brand).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
            }

            if (Model.ProductData_ListViewModel != null && Model.ProductData_ListViewModel.Count != 0)
            {
                var pageNumber = page ?? 1;
                var pageSize = 12;
                Model.ProductData_PagedListViewModel = Model.ProductData_ListViewModel.ToPagedList(pageNumber, pageSize);
                int Allpage = Convert.ToInt16(Math.Ceiling((double)Model.ProductData_ListViewModel.Count / pageSize));
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + langcode + "/Products/ProductsList_Series?series=" + series + "&brand=" + brand + "&", Allpage, pageNumber, Model.ProductData_ListViewModel.Count, "");
            }

            //Other
            ViewBag.LayoutID = "products";
            ViewBag.ViewTitle = Model.ProductSeries_ViewModel.title + " | ";
            return View(Model);
        }

        //商品內頁
        public ActionResult Product_Data(string guid, string type)
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            //Model
            ProductViewModel Model = new ProductViewModel();
            ProductDataService ProductData = new ProductDataService();
            ProductCategoryService ProductCategory = new ProductCategoryService();
            ProductsSeriesService ProductSeries = new ProductsSeriesService();
            ProductIconDataService ProductIconData = new ProductIconDataService();
            VideoDataService VideoData = new VideoDataService();

            //Content
            Model.ProductData_ViewModel = ProductData.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == guid).SingleOrDefault();
            Model.ProductCategory_ViewModel = ProductCategory.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == Model.ProductData_ViewModel.product_category).SingleOrDefault();
            Model.ProductSeries_ViewModel = ProductSeries.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.guid == Model.ProductData_ViewModel.product_series).SingleOrDefault();

            var NextPrev = ProductData.GetNextPrevDataByGuid(lang, guid, type);
            Model.ProductData_NextViewModel = NextPrev["Next"];
            Model.ProductData_PrevViewModel = NextPrev["Prev"];
            Model.ProductIconData_ListViewModel = ProductIconData.GetDataByProductData(lang, Model.ProductData_ViewModel);
            Model.ProductVideoData_ListViewModel = VideoData.GetDataByProductData(lang, Model.ProductData_ViewModel);
            Model.ProductRelatedData_ListViewModel = ProductData.GetRelatedDataByProductData(lang, type, Model.ProductData_ViewModel);

            //Other
            ViewBag.type = type;
            ViewBag.LayoutID = "products";
            ViewBag.ViewTitle = Model.ProductData_ViewModel.title + " | ";
            return View(Model);
        }

        //加入詢問表單
        public ActionResult lbox_inquiry(string guid)
        {
            HttpCookie cookie = Request.Cookies["p_guid"];
            if (cookie == null)
            {
                HttpCookie c = new HttpCookie("p_guid");
                c.Value = guid;
                c.Expires = DateTime.Now.AddDays(7);
                Response.Cookies.Add(c);
                ViewBag.success = "add_success";
            }
            else
            {
                //當cookie已有該產品時
                if (cookie.Value.ToString().Contains(guid) == true)
                {
                    var list = cookie.Value.Split(',').ToList();
                    var val = "";
                    foreach (var item in list)
                    {
                        if (item != guid)
                        {
                            val = item + ",";
                        }
                    }

                    cookie.Expires = DateTime.Now.AddDays(-7);
                    Response.Cookies.Add(cookie);

                    if (!string.IsNullOrEmpty(val))
                    {
                        HttpCookie c = new HttpCookie("p_guid");
                        c.Value = val.TrimEnd(',');
                        c.Expires = DateTime.Now.AddDays(7);
                        Response.Cookies.Add(c);
                    }
                    ViewBag.success = "del_success";
                }
                else
                {
                    cookie.Value = cookie.Value.ToString() + "," + guid;
                    cookie.Expires = DateTime.Now.AddDays(7);
                    Response.Cookies.Add(cookie);
                    ViewBag.success = "add_success";
                }
            }

            return PartialView("lbox_inquiry");
        }

        //加入我的型錄
        public ActionResult lbox_catalog(string guid)
        {
            //Model
            ProductViewModel Model = new ProductViewModel();
            NotePadService Notepad = new NotePadService();

            //Content
            HttpCookie cookie = Request.Cookies["Notepads"];
            if (cookie != null)
            {
                var cookieData = cookie.Values["NoteGuid"];
                List<string> guidList = JsonConvert.DeserializeObject<List<string>>(cookieData);
                Model.Notepad_ListViewModel = Notepad.GetDataByGuidList(guidList);
            }

            ViewBag.guid = guid;
            return PartialView("lbox_catalog", Model);
        }

        //新增Notepad
        [HttpPost]
        public ActionResult AddNotepad(string notepadname)
        {
            try
            {
                NotePadService Notepad = new NotePadService();

                HttpCookie cookie = Request.Cookies["Notepads"];
                var success = "";
                if (cookie == null)
                {
                    List<string> guidList = new List<string>();
                    var guid = Guid.NewGuid().ToString();

                    //新增資料到資料庫
                    var result = Notepad.InsertNotepad(guid, notepadname);
                    if (result == true)
                    {
                        guidList.Add(guid);
                        string jsonGuidList = JsonConvert.SerializeObject(guidList);

                        //產生一個Cookie
                        HttpCookie newcookie = new HttpCookie("Notepads");
                        newcookie.Expires = DateTime.Now.AddDays(30);
                        newcookie.Values.Add("NoteGuid", jsonGuidList);
                        //newcookie.Secure = true;
                        Response.AppendCookie(newcookie);
                        success = "success";
                    }
                    else
                    {
                        success = "faild";
                    }
                }
                else
                {
                    var cookiedata = cookie.Values["NoteGuid"];
                    List<string> guidList = JsonConvert.DeserializeObject<List<string>>(cookiedata);
                    var guid = Guid.NewGuid().ToString();

                    //新增資料到資料庫
                    var result = Notepad.InsertNotepad(guid, notepadname);
                    if (result == true)
                    {
                        guidList.Add(guid);
                        string jsonGuidList = JsonConvert.SerializeObject(guidList);

                        cookie.Expires = DateTime.Now.AddDays(30);
                        cookie.Values.Set("NoteGuid", jsonGuidList);
                        Response.AppendCookie(cookie);
                        success = "success";
                    }
                    else
                    {
                        success = "faild";
                    }
                }

                return Json(new { success }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "faild" }, JsonRequestBehavior.AllowGet);
            }
        }

        //新增產品至Notepad
        [HttpPost]
        public ActionResult AddProductToNotepad(string type, string product_guid, string val)
        {
            try
            {
                //Language
                string lang = CultureHelper.GetLanguage();

                //Model
                NotePadService NotePad = new NotePadService();
                NotePadDataService NotePad_Data = new NotePadDataService();

                //Content
                var success = "faild";
                if (type == "create")
                {
                    HttpCookie cookie = Request.Cookies["Notepads"];
                    if (cookie == null)
                    {
                        List<string> guidList = new List<string>();
                        var guid = Guid.NewGuid().ToString();

                        //新增Notepad
                        var result = NotePad.InsertNotepad(guid, val);
                        if (result == true)
                        {
                            guidList.Add(guid);
                            string jsonGuidList = JsonConvert.SerializeObject(guidList);

                            //產生一個Cookie
                            HttpCookie newcookie = new HttpCookie("Notepads");
                            newcookie.Expires = DateTime.Now.AddDays(30);
                            newcookie.Values.Add("NoteGuid", jsonGuidList);
                            Response.AppendCookie(newcookie);

                            var data = NotePad_Data.InsertData(lang, guid, product_guid);
                            if (data == true)
                            {
                                success = "success";
                            }
                            else
                            {
                                success = "repeat";
                            }
                        }
                    }
                    else
                    {
                        var cookiedata = cookie.Values["NoteGuid"];
                        List<string> guidList = JsonConvert.DeserializeObject<List<string>>(cookiedata);
                        var guid = Guid.NewGuid().ToString();

                        //新增Notepad
                        var result = NotePad.InsertNotepad(guid, val);
                        if (result == true)
                        {
                            guidList.Add(guid);
                            string jsonGuidList = JsonConvert.SerializeObject(guidList);

                            cookie.Expires = DateTime.Now.AddDays(30);
                            cookie.Values.Set("NoteGuid", jsonGuidList);
                            Response.AppendCookie(cookie);

                            var data = NotePad_Data.InsertData(lang, guid, product_guid);
                            if (data == true)
                            {
                                success = "success";
                            }
                            else
                            {
                                success = "repeat";
                            }
                        }
                    }
                }
                else if (type == "select")
                {
                    HttpCookie cookie = Request.Cookies["Notepads"];
                    cookie.Expires = DateTime.Now.AddDays(30);
                    Response.AppendCookie(cookie);

                    var data = NotePad_Data.InsertData(lang, val, product_guid);
                    if (data == true)
                    {
                        success = "success";
                    }
                    else
                    {
                        success = "repeat";
                    }
                }

                return Json(new { success = success }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = "faild" }, JsonRequestBehavior.AllowGet);
            }
        }

        //修改Notepad名稱
        [HttpPost]
        public ActionResult ChangeNotePadName(string guid, string name)
        {
            try
            {
                if (!string.IsNullOrEmpty(guid) && !string.IsNullOrEmpty(name))
                {
                    NotePadService Notepad = new NotePadService();
                    var success = Notepad.ChangeNotepadName(guid, name);

                    return Json(new { success = success }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        //刪除Notepad
        [HttpPost]
        public ActionResult DelteNotepad(string guid)
        {
            try
            {
                if (!string.IsNullOrEmpty(guid))
                {
                    NotePadService Notepad = new NotePadService();
                    var success = Notepad.DeleteNotepad(guid);
                    if (success == true)
                    {
                        HttpCookie cookie = Request.Cookies["Notepads"];
                        var cookiedata = cookie.Values["NoteGuid"];
                        List<string> guidList = JsonConvert.DeserializeObject<List<string>>(cookiedata);

                        List<string> result = new List<string>();
                        foreach (var item in guidList)
                        {
                            if (item != guid)
                            {
                                result.Add(item);
                            }
                        }

                        string jsonGuidList = JsonConvert.SerializeObject(guidList);
                        cookie.Expires = DateTime.Now.AddDays(30);
                        cookie.Values.Set("NoteGuid", jsonGuidList);
                        Response.AppendCookie(cookie);
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        //刪除指定Notepad的產品
        [HttpPost]
        public ActionResult DeleteNotepadData(string notepad_guid, string product_guid)
        {
            try
            {
                if (!string.IsNullOrEmpty(notepad_guid) && !string.IsNullOrEmpty(product_guid))
                {
                    NotePadDataService NotepadData = new NotePadDataService();
                    var success = NotepadData.DeleteData(notepad_guid, product_guid);
                    if (success == true)
                    {
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        //產品型錄
        public ActionResult Catalog(int? page)
        {
            //Language
            string lang = CultureHelper.GetLanguage();
            string langcode = CultureHelper.GetUrlLanguage();

            //Model
            KnowledgeViewModel Model = new KnowledgeViewModel();
            PageBannerService PageBanner = new PageBannerService();
            KnowledgeCatalogService KnowledgeCatalog = new KnowledgeCatalogService();

            //Content
            Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "Product_Catalog").SingleOrDefault();
            Model.KnowledgeCatalog_ListViewModel = KnowledgeCatalog.GetAllData_KnowledgeCatalog(lang);

            if (Model.KnowledgeCatalog_ListViewModel != null && Model.KnowledgeCatalog_ListViewModel.Count != 0)
            {
                var pageNumber = page ?? 1;
                var pageSize = 10;
                Model.KnowledgeCatalog_PagedListViewModel = Model.KnowledgeCatalog_ListViewModel.ToPagedList(pageNumber, pageSize);
                int Allpage = Convert.ToInt16(Math.Ceiling((double)Model.KnowledgeCatalog_ListViewModel.Count / pageSize));
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + langcode + "/Products/Catalog?", Allpage, pageNumber, Model.KnowledgeCatalog_ListViewModel.Count, "");
            }

            //Other
            ViewBag.LayoutID = "knowledge";

            if (lang == "en")
            {
                ViewBag.ViewTitle = "Catalogue | ";
            }
            else
            {
                ViewBag.ViewTitle = "產品型錄 | ";
            }
            return View(Model);
        }

        [HttpPost]
        public ActionResult GetPic(string guid)
        {
            try
            {
                //Language
                string lang = CultureHelper.GetLanguage();

                //Model
                KnowledgeCatalogService KnowledgeCatalog = new KnowledgeCatalogService();

                //Content
                var data = KnowledgeCatalog.GetDataPicByGuid(lang, guid);

                return Json(new { pic = data["pic"], picalt = data["pic_alt"] }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { pic = "", picalt = "" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}