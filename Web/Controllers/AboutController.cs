﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models.Repository;
using Web.Models.ViewModels;

namespace Web.Controllers
{
    public class AboutController : BaseController
    {
        //公司簡介
        public ActionResult Index()
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            //Model
            AboutViewModel Model = new AboutViewModel();
            PageBannerService PageBanner = new PageBannerService();
            AboutService About = new AboutService();
            CourseService Curse = new CourseService();

            //Content
            Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "About_Index").SingleOrDefault();
            Model.About_ViewModel = About.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").SingleOrDefault();
            Model.Course_ListViewModel = Curse.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

            //Other
            ViewBag.LayoutID = "about";

            if (lang == "en")
            {
                ViewBag.ViewTitle = "Company Profile | ";
            }
            else
            {
                ViewBag.ViewTitle = "公司簡介 | ";
            }

            return View(Model);
        }

        //專業認證
        public ActionResult Certificate()
        {
            //Language
            string lang = CultureHelper.GetLanguage();

            //Model
            AboutViewModel Model = new AboutViewModel();
            PageBannerService PageBanner = new PageBannerService();
            CertficateService Certificate = new CertficateService();

            //Content
            Model.PageBanner_ViewModel = PageBanner.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y" && x.category == "About_Certificate").SingleOrDefault();
            Model.Certificate_ListViewModel = Certificate.GetAllDataByPredicate(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ToList();

            //Other
            ViewBag.LayoutID = "about";

            if (lang == "en")
            {
                ViewBag.ViewTitle = "Certification | ";
            }
            else
            {
                ViewBag.ViewTitle = "專業認證 | ";
            }

            return View(Model);
        }

        [HttpPost]
        public ActionResult GetPic(string guid)
        {
            try
            {
                //Language
                string lang = CultureHelper.GetLanguage();

                //Model
                CertficateService Certificate = new CertficateService();

                //Content
                var data = Certificate.GetDataPicByGuid(lang, guid);

                return Json(new { pic = data["pic"], picalt = data["pic_alt"] }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { pic = "", picalt = "" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}