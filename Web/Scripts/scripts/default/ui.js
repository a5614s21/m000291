/* ==========================================================================
	  variables
==========================================================================*/

var $window = $(window),
    $header = $("header");


$(function () {
    'use strict';


	/* ==========================================================================
		initialize
	==========================================================================*/

    // plugin

    if ($(window).outerWidth() > 1024) {
        skrollr.init();
    }

    //mCustomScrollbar
    if (!/Android/i.test(window.navigator.userAgent)) {
        $(".scrollbarX").each(function () {
            $(this).mCustomScrollbar({
                axis: "x",
                mouseWheel: { enable: false }
            });
        });

        $('.scrollbarY').mCustomScrollbar({
            axis: "y"
        });

    }
    // imgLiquid
    $('.jqimgFill').imgLiquid();
    $('.jqimgFill, .main').css("opacity", "1");

    //select2
    $('.selectpicker').select2();
    //燈箱
    $(".lightgallery").lightGallery({
        fullScreen: false,
        share: false,
        actualSize: false,
        autoplayControls: false
    });

    //因架構變化，resize trigger reload
    var wW = $(window).width();
    var trigger_size = [576, 768, 992, 1200];
    $(window).resize(function () {
        trigger_size.forEach(function (ele) {
            if (wW > ele) {
                $(window).width() <= ele ? location.reload() : "";
            } else {
                $(window).width() > ele ? location.reload() : "";
            }
        });
    });





	/* ==========================================================================
		元件
	==========================================================================*/

    $(".watermark").each(function(){
        $(window).scroll(function () {
            var $windowTop = $(window).scrollTop();
            var $h=$(window).height();
            var $editPosiiton=$(".edit").offset();
            var $headerHeight=$("header").height();    
            var $editPosiitonTop = $editPosiiton.top;
            var $editHeight = $(".edit").height();
            if ($windowTop + $headerHeight < $editPosiitonTop ){
                $(".watermark").css({ 'top': $h/2 });
            }
            else if ($windowTop + $headerHeight > $editPosiitonTop && $windowTop < $editPosiitonTop + $editHeight-$h){
                $(".watermark").css({ 'top': $windowTop - $editPosiitonTop + $h/2});
            }
            else{
               
            }
            
        });       
    });
    $(".product-block").each(function(){
        $('.product-block .slider-group .slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.product-block .slider-group .slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            asNavFor: '.slider-for',
            dots: false,
            infinite: false,
            focusOnSelect: true,
            prevArrow: "<div class='slick-arrow slick-prev'><i class='icon-left-open'></i></div>",
            nextArrow: "<div class='slick-arrow slick-next'><i class='icon-right-open'></i></div>",
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                }
            ]
        });
        $(".specification").each(function () {
            $(this).find(".options").find(".btn").click(function () {
                $(this).addClass("active").siblings().removeClass("active");
            });
        });
    });
    $(".labels").each(function(){
        $(this).children("span").click(function(){
            $(this).next("ul").slideToggle();
        });
    });
    $(".file-group input").change(function () {
        $(this).parents(".file-group").find(".val").text($(this).val().substring($(this).val().lastIndexOf("\\") + 1));
    });


	/* ==========================================================================
		pages
	==========================================================================*/

    layout();
    numberWrapperEvent();
    productItem();
});













function layout() {
    $('header .slider').slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 500,
        infinite: false,
        prevArrow: "<div class='slick-arrow slick-prev'><i class='icon-left-open'></i></div>",
        nextArrow: "<div class='slick-arrow slick-next'><i class='icon-right-open'></i></div>",
        responsive: [
            {
                breakpoint: 576,
                settings: {
                    dots: false
                }
            },
        ]
    });

    if ($(window).outerWidth() >=1200) {
        $("#header ul.menu>li:nth-child(1)").hover(function () {
            $(this).children(".submenu").addClass("show");

        }, function () {
            $(this).children(".submenu").removeClass("show");
        });
    }



    //漢堡條
    $(".menu-toggle").click(function () {
        $(this).toggleClass('active');
        $(this).parents("header").find(".bg_phone").toggleClass('active');
        $(".gray_block").toggleClass('active');
        $("html").toggleClass("noscroll");
        $("header").find(".submenu").removeClass("active");
    });
    //選單展開的灰色背景
    $(".gray_block").click(function () {
        $(".menu-toggle").removeClass('active');
        $(this).parent().find(".bg_phone").toggleClass('active');
        $(".gray_block").removeClass('active');
        $("html").removeClass("noscroll");
        $("header").find(".submenu").removeClass("active");
    });



    //goTop
    $("#goTop").on("click", function () {
        $('html, body').animate({ scrollTop: 0 }, 800);
    });



    //1199以下 選單
    if ($(window).outerWidth() < 1199) {
        //回上層選單
        $("header ul.menu>li .submenu>.tag").click(function () {
            $(this).parents(".submenu").eq(0).removeClass("active");
        });

        //下層選單 展開
        $("header ul.menu>li .list").click(function () {
            $(this).next(".submenu").addClass("active");
        });
    }

    if ($window.scrollTop() > 100) {
        $header.addClass("scroll");
    }
    $window.scroll(function () {
        if ($window.scrollTop() > 100) {
            $header.addClass("scroll");
        }
        else {
            $header.removeClass("scroll");
        }
    });

 


    $("header .search-toggle").click(function () {
        $("header .search").toggleClass("actived");
        $("header .search  .search-toggle .icon-cancel").toggleClass("hidden");
        $("header .search  .search-toggle .icon-search").toggleClass("hidden");
    }); 



}

function uniformHeight($el) {
    if ($el.length !== 0) {
        var $h_array = [];
        $el.each(function (index) {
            $(this).height("auto");
            $h_array[index] = $(this).height();
        });
        $max = Math.max.apply(null, $h_array);
        $el.each(function (index) {
            $(this).height($max);
        });
    }
}
function productItem() {
    if ($(window).outerWidth() >= 1200) {
        $(".product-item").hover(function () {
            $(this).find(".img-box img").addClass("scale");
        }, function () {
            if ($(this).find(".heart").hasClass("select") || $(this).find(".addcart").hasClass("select")) {
                $(this).find(".box").addClass("fixed");
            }
            else{
                $(this).find(".box").removeClass("fixed");
            }
                $(this).find(".img-box img").removeClass("scale");
        });
    }
    $(".product-item").find(".heart").click(function () {
        $(this).toggleClass("select");
    });

    cartFly();
    function cartFly() {
        $(".addcart").click(function (event) {

            if ($(this).hasClass("select")) {

            }
            else {
                var offset = $("#cart-icon").offset();
                var img = $(this).parents(".product-item").find("a").find('img').attr('src'); //获取当前点击图片链接  
                if ($(this).parents(".product-item").length == 0) {
                    img = $(this).children("img.hidden").attr('src');
                }
                var flyer = $('<img class="flyer-img" src="' + img + '">'); //抛物体对象   
                if ($(window).outerWidth() > 576) {
                    flyer.fly({
                        start: {
                            left: event.pageX,//抛物体起点横坐标   
                            top: event.pageY - $(window).scrollTop(), //抛物体起点纵坐标   

                        },
                        end: {
                            left: offset.left - 30,//抛物体终点横坐标   
                            top: offset.top - $(window).scrollTop() + 10, //抛物体终点纵坐标   

                        },
                        onEnd: function () {
                            flyer.remove(); //销毁抛物体   
                        },

                    });
                }
                else {
                    flyer.fly({
                        start: {
                            left: event.pageX,//抛物体起点横坐标   
                            top: event.pageY - $(window).scrollTop(), //抛物体起点纵坐标   

                        },
                        end: {
                            left: offset.left - 30,//抛物体终点横坐标   
                            top: offset.top - $(window).scrollTop() + 10, //抛物体终点纵坐标   

                        },
                        onEnd: function () {
                            flyer.remove(); //销毁抛物体   
                        },
                        speed: 1.5
                    });
                }

            }
            $(this).toggleClass("select");
        });

    }
    if($(window).outerWidth()>576){
        uniformHeight($(".product-item a > span .text-box p"));
    }
    
}


function menuToggle(){
    $(this).toggleClass('active');
    $(this).parents("header").find(".bg_phone").toggleClass('active');
    $(".gray_block").toggleClass('active');
    $("html").toggleClass("noscroll");
    $("header").find(".submenu").removeClass("active");
}

function numberWrapperEvent() {
    $(".number-wrapper").find(".add-btn").click(function () {
        var $val = parseInt($(this).parents(".number-wrapper").find("input").val());
        $(this).parents(".number-wrapper").find("input[type=number]").val($val + 1);
        $(this).parents(".number-wrapper").find("input[type=number]").blur();
    });
    $(".number-wrapper").find(".minus-btn").click(function () {
        var $val = parseInt($(this).parents(".number-wrapper").find("input").val());
        if ($val > 0) {
            $(this).parents(".number-wrapper").find("input[type=number]").val($val - 1)
        }

    });
}


// (function () {

//     var unit = 100;
//     var canvas, context, canvas2, context2, height, width, xAxis, yAxis, draw;

//     function init() {
//         canvas = document.getElementById("sineCanvas");
//         canvas.width = document.documentElement.clientWidth;
//         canvas.height = 100;
//         context = canvas.getContext("2d");
//         height = canvas.height;
//         width = canvas.width;
//         xAxis = Math.floor(height / 2);
//         yAxis = 0;

//         draw();
//     }

//     function draw() {
//         context.clearRect(0, 0, width, height);

//         drawWave('#EECFDE', 1, 3, 0); //（fillcolor, alpha, zoom, delay）

//         draw.seconds = draw.seconds + .009;
//         draw.t = draw.seconds * Math.PI;
//         setTimeout(draw, 35);
//     };
//     draw.seconds = 0;
//     draw.t = 0;

//     function drawWave(fillcolor, alpha, zoom, delay) {
//         context.fillStyle = fillcolor;
//         context.globalAlpha = alpha;

//         context.beginPath();
//         drawSine(draw.t / 0.8, zoom, delay);
//         context.lineTo(width + 10, height);
//         context.lineTo(0, height);
//         context.closePath()
//         context.fill();
//     }

//     function drawSine(t, zoom, delay) {
//         var x = t;
//         var y = Math.sin(x) / zoom;
//         context.moveTo(yAxis, unit * y + xAxis);


//         for (i = yAxis; i <= width + 10; i += 10) {
//             x = t + (-yAxis + i) / unit / zoom;
//             y = Math.sin(x - delay) / 3;
//             context.lineTo(i, unit * y + xAxis);
//         }
//     }

//     init();

// })();

