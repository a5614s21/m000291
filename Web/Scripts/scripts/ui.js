// Variables ==================================================================
var $window = $(window),
  $header = $('header'),
  $body = $('body'),
  rwd = 992

// Plug-in ====================================================================
// Lazyload
function lazyloadFn() {
  $('[data-src]').lazyload()
  // if(){
  // }
  // $('img').lazyload({
  //     effect: 'fadeIn'
  // });
}
function lightgalleryFn() {
  $('.js-lightgallery').lightGallery()
}

// Textillate
function textillateFn(el) {
  $(el).textillate({
    initialDelay: 0,
    loop: false,
    autoStart: false,
    in: {
      effect: 'flipInY'
      // delay: 50
    }
    // out: {
    //     effect: 'fadeOut',
    // }
  })
}

// Aos
function AosFn() {
  AOS.init({
    duration: 800,
    // easing: 'ease-in-out-quad',
    // default easing for AOS animations
    offset: 100,
    // once: true,
    // whether animation should happen only once - while scrolling down
    mirror: true
    // whether animation should happen only once - while scrolling down
  })
}

// dropkick
function dropkickFn(ele) {
  $(ele).dropkick({
    mobile: true
  })
}

// imgLiquid
$('.js-imgFill').imgLiquid()

//Horizontal Tab
function tabTagFn() {
  $('#parentHorizontalTab').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    tabidentify: 'hor_1', // The tab groups identifier
    activate: function (event) {
      // Callback function if tab is switched
      var $tab = $(this)
      var $info = $('#nested-tabInfo')
      var $name = $('span', $info)
      $name.text($tab.text())
      $info.show()
    }
  })
}

// Swiper
var swiper = {
  index: function () {
    var banner = new Swiper('.m-bn__swp', {
      spaceBetween: 0,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false
      },
      speed: 2000,
      effect: 'fade',
      loop: true,
      fadeEffect: {
        crossFade: true
      },
      navigation: {
        nextEl: '.m-bn__btn .js-next',
        prevEl: '.m-bn__btn .js-prev'
      },
      pagination: {
        el: '.m-bn__dot',
        clickable: true
        // renderBullet: function (index, className) {
        //     return '<div class="' + className + '"><span>0' + (index + 1) + '</span></div>';
        // },
      }
    })
    function nwsJudge() {
      var relatedItem = $('.idx-nws__swp .swiper-slide').length,
        maxItem = 1,
        item = maxItem
      console.log(relatedItem)

      if (relatedItem == 1) {
        console.log('= 1')

        $('.idx-nws__btn').addClass('u-hide')

        var nws = new Swiper('.idx-nws__swp', {
          spaceBetween: 0,
          slidesPerView: 3,
          centeredSlides: true,
          noSwiping: true,
          noSwipingSelector: '.swiper-slide',
          loop: true,
          breakpoints: {
            992: {
              slidesPerView: 1,
              loopAdditionalSlides: 2,
              speed: 2000,
              effect: 'fade',
              fadeEffect: {
                crossFade: true
              }
              // spaceBetween: -40,
            }
          }
        })
      } else {
        console.log('> 1')

        $('.idx-nws__btn').removeClass('u-hide')

        var nws = new Swiper('.idx-nws__swp', {
          spaceBetween: 0,
          slidesPerView: 3,
          centeredSlides: true,
          loop: true,
          breakpoints: {
            992: {
              slidesPerView: 1,
              loopAdditionalSlides: 2,
              speed: 2000,
              effect: 'fade',
              fadeEffect: {
                crossFade: true
              }
              // spaceBetween: -40,
            }
          },
          navigation: {
            nextEl: '.idx-nws__btn .js-next',
            prevEl: '.idx-nws__btn .js-prev'
          }
        })
      }
    }
    nwsJudge()

    var pro = new Swiper('.idx-pro__swp', {
      spaceBetween: 10,
      slidesPerView: 4,
      // slideNextClass: 'my-slide-next',
      // slideDuplicatedNextClass: 'tt',
      // loop: true,
      breakpoints: {
        1440: {
          slidesPerView: 3
        },
        1100: {
          slidesPerView: 2
        },
        575: {
          spaceBetween: 5,
          slidesPerView: 2
        }
      },
      navigation: {
        nextEl: '.idx-pro__swp-btn .js-next',
        prevEl: '.idx-pro__swp-btn .js-prev'
      }
    })

    function videoJudge() {
      var relatedItem = $('.idx-video__swp .swiper-slide').length,
        maxItem = 1,
        item = maxItem
      // console.log(relatedItem);

      if (relatedItem == 1) {
        // console.log('1');
        $('.idx-video__btn').addClass('u-hide')

        var vidoe = new Swiper('.idx-video__swp', {
          spaceBetween: 0,
          slidesPerView: 3,
          // loopedSlides: 3,
          centeredSlides: true,
          loop: true,
          noSwiping: true,
          noSwipingSelector: '.swiper-slide',
          // loopAdditionalSlides: 1,
          breakpoints: {
            992: {
              slidesPerView: 1,
              // loopAdditionalSlides: 2,
              speed: 2000,
              effect: 'fade',
              fadeEffect: {
                crossFade: true
              }
              // spaceBetween: -40,
            }
          }
        })
      } else {
        // console.log('> 1');

        $('.idx-video__btn').removeClass('u-hide')

        var vidoe = new Swiper('.idx-video__swp', {
          spaceBetween: 0,
          slidesPerView: 3,
          centeredSlides: true,
          loop: true,
          loopAdditionalSlides: 2,
          breakpoints: {
            992: {
              slidesPerView: 1,
              loopAdditionalSlides: 2,
              speed: 2000,
              effect: 'fade',
              fadeEffect: {
                crossFade: true
              }
              // spaceBetween: -40,
            }
          },
          navigation: {
            nextEl: '.idx-video__btn .js-next',
            prevEl: '.idx-video__btn .js-prev'
          }
        })
      }
    }
    videoJudge()
  },
  products: function () {
    function tagJudge(i) {
      var tag = new Swiper('.cms-cls__swp', {
        spaceBetween: 35,
        slidesPerView: i,
        // centeredSlides: true,
        // loop: true,
        breakpoints: {
          1440: {
            spaceBetween: 0
          },
          1200: {
            slidesPerView: 5,
            spaceBetween: 35
          },
          992: {
            slidesPerView: 4
          },
          900: {
            slidesPerView: 3
          },
          670: {
            slidesPerView: 2
          },
          320: {
            slidesPerView: 1
          }
        },
        navigation: {
          nextEl: '.cms-cls__btn .js-next',
          prevEl: '.cms-cls__btn .js-prev'
        }
      })
    }
    var tagItem = $('.cms-cls__swp .swiper-slide').length,
      maxItem = 6,
      item = maxItem
    if (tagItem < maxItem) {
      if (tagItem == maxItem) {
        $('.cms-cls__wp').removeClass('l-wp-1194').addClass('l-wp-1430')
      }
      item = tagItem
    }
    tagJudge(item)

    // var logo = new Swiper('.prol-cls__swp', {
    //     spaceBetween: -1,
    //     slidesPerView: 8,
    //     // centeredSlides: true,
    //     // loop: true,
    //     breakpoints: {
    //         1680: {
    //             slidesPerView: 6,
    //         },
    //         1200: {
    //             slidesPerView: 4,
    //         },
    //         575: {
    //             slidesPerView: 2,
    //         },
    //         320: {
    //             slidesPerView: 1,
    //         },
    //     },
    //     navigation: {
    //         nextEl: '.prol-cls__btn .js-next',
    //         prevEl: '.prol-cls__btn .js-prev',
    //     },
    // });

    var opt = new Swiper('.prod-pic__swp.js-opt', {
      spaceBetween: 0,
      slidesPerView: 5,
      slideToClickedSlide: true,
      centeredSlides: true,
      loop: true,
      // loopAdditionalSlides: 2,
      swipeHandler: '.swipe-handler',
      navigation: {
        nextEl: '.prod-pic__opt-btn .js-next',
        prevEl: '.prod-pic__opt-btn .js-prev'
      },
      slidePrevClass: 'my-slide-prev',
      slideNextClass: 'my-slide-next',
      breakpoints: {
        1440: {
          slidesPerView: 3
        },
        1200: {
          slidesPerView: 1,
          swipeHandler: null
        }
      }
      // on: {
      //     click: function () {
      //         var bk = $('.prod-pic__swp'),
      //             item = bk.find('.swiper-slide');

      //         function judge(el) {
      //             var showImg = $('.prod-pic__show img').attr('src');
      //             if (showImg != el) {
      //                 $('.prod-pic__show img').attr('src', el)
      //             }
      //         }

      //         item.click(function () {
      //             var active = $(this).find('img').attr('src');
      //             console.log('this' + active);

      //             judge(active);
      //         });
      //     },
      // }
    })

    var prodOther = new Swiper('.prod-more__swp', {
      spaceBetween: 0,
      slidesPerView: 5,
      breakpoints: {
        1440: {
          slidesPerView: 4
        },
        1366: {
          slidesPerView: 3
        },
        992: {
          slidesPerView: 2
        },
        575: {
          slidesPerView: 1
        }
      },
      navigation: {
        nextEl: '.prod-more__btn .js-next',
        prevEl: '.prod-more__btn .js-prev'
      }
    })
  },
  about: function () {
    var profession = new Swiper('.abt-info__bk--profession .abt-info__swp', {
      slidesPerView: 6,
      spaceBetween: 0,
      navigation: {
        prevEl: '.abt-info__bk--profession .js-prev',
        nextEl: '.abt-info__bk--profession .js-next'
      },
      breakpoints: {
        1100: {
          slidesPerView: 3
        },
        575: {
          slidesPerView: 1
        }
      }
    })
  },
  service: function () {
    var entity = new Swiper('.ser-list--entity .ser-list__swp', {
      spaceBetween: -1,
      slidesPerView: 4,
      breakpoints: {
        1366: {
          slidesPerView: 3
        },
        1200: {
          slidesPerView: 2
        },
        575: {
          slidesPerView: 1,
          spaceBetween: 0
        }
      },
      navigation: {
        prevEl: '.ser-list--entity .js-swp-prev',
        nextEl: '.ser-list--entity .js-swp-next'
      }
      // pagination: {
      //     el: '.swiper-pagination',
      //     clickable: true,
      // }
    })

    var web = new Swiper('.ser-list--web .ser-list__swp', {
      spaceBetween: -1,
      slidesPerView: 4,
      breakpoints: {
        1366: {
          slidesPerView: 3
        },
        1200: {
          slidesPerView: 2
        },
        575: {
          slidesPerView: 1,
          spaceBetween: 0
        }
      },
      navigation: {
        prevEl: '.ser-list--web .js-swp-prev',
        nextEl: '.ser-list--web .js-swp-next'
      }
      // pagination: {
      //     el: '.swiper-pagination',
      //     clickable: true,
      // }
    })
  }
}

var _waypoint = {
  eachWaypoint: function (target, offsetValue) {
    $(target).each(function (index, element) {
      $(element).waypoint({
        handler: function () {
          $(element).addClass('is-animation')
        },
        offset: offsetValue
      })
    })
  },
  index: function () {
    _waypoint.eachWaypoint('.idx-pro_bg-el--pic', '80%')
    _waypoint.eachWaypoint('.idx-application_bg-el--pic', '30%')
  }
}

// img zoom
var imgZoom = {
  prod: function (el) {
    $(el).imagezoomsl({
      innerzoommagnifier: true,
      classmagnifier: window.external ? 'round-loope' : '',
      zoomrange: [1.2, 3],
      zoomstart: 1.2,
      magnifiersize: [300, 300]
    })
  }
}

// tablesorter
function tabFn() {
  $(function () {
    $('table').tablesorter({
      widgets: ['zebra', 'columns'],
      usNumberFormat: false,
      sortReset: true,
      sortRestart: true
    })
  })
}
// Element ====================================================================

// Common ---------------------------------------------------------------------
function browserIE() {
  var browser = navigator.userAgent
  var browserVerify = browser.toLowerCase().match(/rv:([\d.]+)\) like gecko/)
  var bie = browserVerify
  // console.log('ans: ' + bie);

  if (bie != null) {
    $('body').addClass('is-ie11')
  }

  return bie
}

function reBrowser() {
  //因架構變化，resize trigger reload
  var wW = $(window).width()
  var trigger_size = [
    576, 768, 800, 885, 890, 944, 952, 950, 992, 1200, 1366, 1400, 1440, 1680
  ]
  $(window).resize(function () {
    trigger_size.forEach(function (ele) {
      if (wW > ele) {
        $(window).width() <= ele ? location.reload() : ''
      } else {
        $(window).width() > ele ? location.reload() : ''
      }
    })
  })
}

// dataFun
var dataFun = {
  href: function () {
    $('[data-href]').click(function () {
      var href = $(this).data('href')
      location.href = href
    })
  },
  open: function () {
    $('[data-open]').click(function () {
      var href = $(this).data('open')
      window.open(href)
    })
  },
  bg: function () {
    $('[data-imgBg]').each(function () {
      var src = $(this).data('imgBg')
      $(this).css('background-image', 'url("' + src + '")')
    })
  },
  changeImg: function () {
    $('[data-img]').each(function () {
      var $this = $(this),
        judge = $this.data('img'),
        lazyImg = $this.find('img').data('src'),
        img = $this.find('img'),
        src = img.attr('src')
      // console.log(judge);

      function imgBg(name) {
        if (lazyImg == undefined) {
          $this
            .css('background-image', 'url("' + src + '")')
            .addClass('js-img--' + name + ' js-img--config')
          img.hide()
        }
      }
      imgBg(judge)
    })
  },
  imgfix: function () {
    $('[data-imgfix]').each(function () {
      var $this = $(this),
        $img = $this.find('img'),
        _imgSrcLoading = $img.attr('src'),
        _imgSrcMain = $img.data('src')

      // console.log('loading:' + _imgSrcLoading + ' Main:' + _imgSrcMain);

      $img.wrap('<div class="is-imgfix"></div>')
    })
  },
  all: function () {
    this.href()
    this.open()
    this.bg()
    this.changeImg()
    this.imgfix()
  }
}

// change svg
function svg() {
  $('img.js-svg').each(function () {
    var $img = $(this)
    var imgID = $img.attr('id')
    var imgClass = $img.attr('class')
    var imgURL = $img.attr('src')

    $.get(
      imgURL,
      function (data) {
        // Get the SVG tag, ignore the rest
        var $svg = $(data).find('svg')

        // Add replaced image's ID to the new SVG
        if (typeof imgID !== 'undefined') {
          $svg = $svg.attr('id', imgID)
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
          $svg = $svg.attr('class', imgClass + ' replaced-svg')
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a')

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if (
          !$svg.attr('viewBox') &&
          $svg.attr('height') &&
          $svg.attr('width')
        ) {
          $svg.attr(
            'viewBox',
            '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width')
          )
        }

        // Replace image with new SVG
        $img.replaceWith($svg)
      },
      'xml'
    )
  })
}

// Click
var clickBase = {
  bnScroll: function (ele, tag) {
    $(ele).click(function () {
      var thisID = tag
      $('body,html').animate({ scrollTop: $(thisID).offset().top }, 600)
    })
  },
  gotop: function (bk, btn) {
    $(window).scroll(function () {
      var threshold =
        $(document).height() - $(window).height() - $('footer').height()

      if ($(window).scrollTop() > 0) {
        $(bk).addClass('is-show')

        if ($(window).scrollTop() >= threshold) {
          $(bk).removeClass('is-move')
        } else {
          $(bk).addClass('is-move')
        }
      } else {
        $(bk).removeClass('is-show')
      }
    })

    $(bk)
      .find(btn)
      .click(function () {
        $('html, body').animate(
          {
            scrollTop: 0
          },
          500,
          'swing'
        )
      })
  },
  back: function (ele) {
    $(ele).click(function () {
      history.back()
    })
  }
}

function lockScroll() {
  $body.addClass('u-scroll-no fancybox-active compensate-for-scrollbar')
}
function unlockScroll() {
  $body.removeClass('u-scroll-no fancybox-active compensate-for-scrollbar')
}

// Ajax Lightbox
function active_lbox() {
  var lbox_switch = $('[data-lbox]')
  lbox_switch.on('click', function () {
    var a = $(this).attr('data-lbox')
    var b = $(this).attr('data-id')
    var c = $(this).attr('data-videoId')
    lbox(a, b, c)
  })
}
function lbox(lbox_page, lbox_id, video_id) {
  $.ajax({
    url: lbox_page
  }).done(function (data) {
    $('body')
      .append(
        "<article class='m-lbox " +
          lbox_id +
          "' data-id=" +
          lbox_id +
          '></article>'
      )
      .addClass('is-lbox-open')

    lockScroll()

    var injectTarget = '.m-lbox.' + lbox_id
    $(injectTarget).html(data)
    lbox_function(lbox_id, video_id)
    /*給燈箱一個 open 讓動畫作動*/
    if ($(injectTarget).length > 0) {
      setTimeout(function () {
        $(injectTarget).addClass('is-open')
      }, 500)
    }
  })
}
function lbox_function(id, video_id) {
  // dataFun.all();
  switch (id) {
    case 'lbox-video':
      // $('.lbox-video iframe').attr({
      //     'src': 'https://www.youtube.com/embed/' + video_id + '?rel=0&autoplay=1'
      // })
      lbox_close(id, 500)
      break

    case 'csr-lb':
      //csr-lb 燈箱
      // console.log('13579');
      setTimeout(function () {
        $('.map-list').addClass('in')
      }, 800)
      // features.csr_page()
      lbox_close(id, 1000)
      break
    case 'lbox-catalog':
      $('.js-ctg-open').click(function () {
        $('.hd-catalog__ctr--list').addClass('is-open')
        $('.hd').addClass('is-search-open')
        $body.addClass('u-scroll-no')
        $('.hd-nav').addClass('u-scroll-no')
      })
    default:
      lbox_close(id, 500)
      break
  }
}
function lbox_close(id, time) {
  var closeBtn = '.m-lbox.' + id
  var ajaxCloseBtn = $(closeBtn).find('.js-ajax-close')
  ajaxCloseBtn.on('click', function () {
    var _this = $(this),
      targetPage = _this.closest('.m-lbox')
    targetPage.removeClass('is-open').addClass('is-close')
    $('body').removeClass('is-lbox-open')
    unlockScroll()

    setTimeout(function () {
      targetPage.remove()
    }, time)
  })
}
// load
function myload() {
  setTimeout(function () {
    $('.loading').addClass('is-hide')
  }, 2500)
}

// Menu
function menu() {
  var menu = {
    click: function () {
      var btn = $('.hd-toggle'),
        ctr = $('.hd-nav')
      btn.click(function () {
        ctr.toggleClass('is-show')
        $(this).toggleClass('is-active')
        $('.hd').toggleClass('is-open')
        $('body').toggleClass('u-scroll-no')
      })
    },
    move: function () {
      var init = $(window).scrollTop()
      if (init != 0) {
        $('.hd').removeClass('is-top')
      }

      $(window).scroll(function () {
        if ($(window).scrollTop() > 0) {
          $('.hd').removeClass('is-top')
          // $('.hd').removeClass('is-top').addClass('is-move');
        } else {
          $('.hd').addClass('is-top')
          $('.hd').addClass('is-top').removeClass('is-move')
        }
      })
    },
    active: function () {
      var _page = $('.l-wp-html').data('id')
      $('.menu__item[data-active="' + _page + '"]')
        .find('.menu__link')
        .addClass('active')
    },
    setting: function () {
      this.active()
      this.click()
      this.move()
    }
  }
  menu.setting()

  // Search
  function search() {
    var btn = $('.hd-search__btn'),
      ctr = $('.hd-search__ctr'),
      inpt = ctr.find('.m-fm__input')

    btn.click(function () {
      ctr.toggleClass('is-show')
      $('.hd').toggleClass('is-search-open');
      $('.hd-logo').toggleClass('vis');

    })
    if ($('.hd-toggle').is(':visible')) {
      inpt.attr('placeholder', '')
    }
  }
  search()

  function catalogFn() {
    var _btn = $('.hd-catalog__btn'),
      _ctr = $('.hd-catalog__ctr--list'),
      _editBtn = $('.hd-catalog__item-btn .m-btn--edit'),
      _saveBtn = $('.hd-catalog__item-btn .m-btn--save'),
      toggleBtn = $('.hd-catalog__item-name .el-name'),
      _addCtr = $('.hd-catalog__ctr--add'),
      _hd = $('.hd-nav')

    function ctgClose(el) {
      $(el).click(function () {
        _ctr.removeClass('is-open')
        $('.hd').removeClass('is-search-open')
        $body.removeClass('u-scroll-no')
      })
    }

    ctgClose('.hd-catalog__ctr--list .hd-catalog__head-btn--close')
    ctgClose('.hd-catalog__ctr--list .hd-catalog__mask')

    var ctgAdd = {
      open: function (el) {
        $(el).click(function () {
          _addCtr.addClass('is-open')
          $('.hd').addClass('is-search-open')
          $body.addClass('u-scroll-no')
          // _hd.addClass('u-scroll-no');
        })
      },
      close: function (el) {
        $(el).click(function () {
          _addCtr.removeClass('is-open')
          // _hd.removeClass('u-scroll-no');
        })
      }
    }
    ctgAdd.open('.hd-catalog__head-btn--add')
    ctgAdd.close('.hd-catalog__ctr--add .hd-catalog__head-btn--close')
    ctgAdd.close('.hd-catalog__ctr--add .hd-catalog__mask')
    ctgAdd.close('.hd-catalog__save .m-btn')

    var ctgList = {
      open: function (el) {
        $(el).click(function () {
          _ctr.addClass('is-open')
          $('.hd').addClass('is-search-open')
          $body.addClass('u-scroll-no')
          _hd.addClass('u-scroll-no')
        })
      }
    }
    ctgList.open('.js-ctg-open')

    _btn.click(function () {
      _ctr.toggleClass('is-open')
      $('.hd').toggleClass('is-search-open')
      $body.toggleClass('u-scroll-no')
      _hd.toggleClass('u-scroll-no')
    })

    _editBtn.click(function () {
      $(this).parents('.hd-catalog__item-head').addClass('is-edit')
    })

    _saveBtn.click(function () {
      $(this).parents('.hd-catalog__item-head').removeClass('is-edit')
    })

    toggleBtn.click(function () {
      $(this)
        .parents('.hd-catalog__item')
        .find('.hd-catalog__item-body')
        .stop()
        .slideToggle()
    })
  }
  catalogFn()
}

function bookmarkFn() {
  var mask = $('<div class="cms-tages__mask"></div>'),
    _btn = $('.cms-tages__toggle'),
    _ctr = $('.cms-tages__ctr')

  mask.appendTo('.cms-tages__ctr')
  var _maskBtn = $('.cms-tages__mask')
  _maskBtn.click(function () {
    _ctr.removeClass('is-show')
    $body.removeClass('u-scroll-no')
  })

  _btn.click(function () {
    $(this).next(_ctr).toggleClass('is-show')
    $body.addClass('u-scroll-no')
  })
}

var insideLightbox = {
  prod: function () {
    function inquiry() {
      var btn = $('.js-inquiry'),
        close = $('.js-inquiry-close'),
        lb = $('.prod-inquiry.lb')
      btn.click(function () {
        lb.addClass('is-show')
        $body.addClass('u-scroll-no fancybox-active compensate-for-scrollbar')
      })
      close.click(function () {
        lb.removeClass('is-show')
        $body.removeClass(
          'u-scroll-no fancybox-active compensate-for-scrollbar'
        )
      })
    }
    inquiry()

    function compare() {
      var _checkboxBk = $('.el-compare'),
        showBk = $('.prod-compare'),
        inpt = $('input[type="checkbox"]')

      var checkArray = new Array()

      _checkboxBk.find(inpt).change(function () {
        checkArray = []
        _checkboxBk.each(function () {
          var judge = $(this).find('input').prop('checked')
          // console.log(judge);

          checkArray.push(judge)
        })
        // console.log(checkArray);
        for (var i = 0; i < checkArray.length; i++) {
          // console.log('a. ' + checkArray[i]);

          if (checkArray[i] == true) {
            showBk.addClass('is-show')
            $('.gotop').addClass('is-compare-show')

            // console.log('b. ' + checkArray[i]);
            return
          } else {
            showBk.removeClass('is-show is-slide-down')
            $('.gotop').removeClass('is-compare-show')
            $('.prod-compare .m-btn--switch').remove('is-slide-down')
            // console.log('c. ' + checkArray[i]);
          }
        }
      })

      var toolBtn = $('.prod-compare .m-btn--switch')
      toolBtn.click(function () {
        if ($('.prod-compare').is(':visible')) {
          showBk.toggleClass('is-slide-down')
          $(this).toggleClass('is-slide-down')
        }
      })
    }
    compare()
  },
  app: function () {
    function lbOpen() {
      var btn = $('.js-lb-open'),
        close = $('.js-lb-close'),
        lb = $('.login-lb.lb')
      btn.click(function () {
        lb.addClass('is-show')
        $body.addClass('u-scroll-no fancybox-active compensate-for-scrollbar')
      })
      close.click(function () {
        lb.removeClass('is-show')
        $body.removeClass(
          'u-scroll-no fancybox-active compensate-for-scrollbar'
        )
      })
    }
    lbOpen()
  },
  inquiry: function () {
    function lbOpen() {
      var btn = $('.js-lb-open'),
        close = $('.js-lb-close'),
        lb = $('.inquiry-lb.lb')
      btn.click(function () {
        lb.addClass('is-show')
        $body.addClass('u-scroll-no fancybox-active compensate-for-scrollbar')
      })
      close.click(function () {
        lb.removeClass('is-show')
        $body.removeClass(
          'u-scroll-no fancybox-active compensate-for-scrollbar'
        )
      })
    }
    lbOpen()
  },
  contact: function () {
    var btn = $('.js-lb-open'),
      close = $('.js-lb-close'),
      lb = $('.contact-submit.lb')
    btn.click(function () {
      lb.addClass('is-show')
      $body.addClass('u-scroll-no fancybox-active compensate-for-scrollbar')
    })
    close.click(function () {
      lb.removeClass('is-show')
      $body.removeClass('u-scroll-no fancybox-active compensate-for-scrollbar')
    })
  }
}

// product
function productFn() {
  function cls() {
    var _btn = $('.cms-cls__toggle'),
      _bk = $('.cms-cls')
    _btn.click(function () {
      _bk.toggleClass('is-open')
      $('.m-bn__headline').toggleClass('is-close')
    })
  }
  cls()

  function proList() {
    var item = $('.cms-cls__link'),
      bnImg = $('.m-bn--pro-list .m-bn__pic')

    item.each(function () {
      var hoverImg = $(this).data('change')
      $(
        '<figure data-imgfix="full" class="is-changeImg"><img src="' +
          hoverImg +
          '"></figure>'
      ).appendTo(bnImg)
    })

    item.hover(
      function () {
        var hoverImg = $(this).data('change')
        $('.is-changeImg').each(function () {
          var judeg = $(this).find('img').attr('src')
          if (hoverImg == judeg) {
            $(this).find('img').addClass('is-show')
          }
        })
      },
      function () {
        $('.is-changeImg').find('img').removeClass('is-show')
      }
    )
  }
  proList()

  function detail() {
    if ($('.prod-stn').is(':visible')) {
      $('.l-wp-html').addClass('prod-html')
    }
  }
  detail()

  function detailImg() {
    var bk = $('.prod-pic__swp'),
      item = bk.find('.swiper-slide'),
      pv = $('.prod-pic__opt-btn .js-prev'),
      nt = $('.prod-pic__opt-btn .js-next')

    function judge(el) {
      var showImg = $('.prod-pic__show .js-show').attr('src')
      // console.log(showImg);

      if (showImg != el) {
        $('.prod-pic__show .js-show')
          .stop(false, true)
          .fadeOut(400)
          .attr('src', el)
          .stop(false, true)
          .fadeIn(400)
      }
    }

    pv.click(function () {
      var active = bk.find('.my-slide-prev img').attr('src')
      // console.log('pv' + active);

      judge(active)
    })
    nt.click(function () {
      var active = bk.find('.my-slide-next img').attr('src')
      // console.log('nt' + active);

      judge(active)
    })
  }
  detailImg()
}

// knowledge
function knowledgeFn() {
  function faq() {
    var bk = $('.faq-card'),
      btn = $('.faq-card__question')

    btn.click(function () {
      var _bk = $(this).parent('.faq-card'),
        _ctr = $(this).next('.faq-card__answer')

      if (!_ctr.is(':visible')) {
        bk.removeClass('is-open')
        $('.faq-card__answer:visible').slideUp()
      }

      _bk.toggleClass('is-open')
      _ctr.slideToggle()
    })
  }
  faq()

  function catalogFn() {
    $('.js-lbox').click(function () {
      $(this)
        .parents('.catalog-list__item')
        .children('.js-lightgallery')
        .children('a')
        .eq(0)
        .click()
    })
  }
  catalogFn()
}

// contact from
function fromFn() {
  $('.fm-gp--file input').change(function () {
    $(this)
      .parents('.fm-gp')
      .find('label')
      .removeClass('placeholder')
      .text(
        $(this)
          .val()
          .substring($(this).val().lastIndexOf('\\') + 1)
      )
  })
}

// Page =======================================================================
var readyFunction = {
  checkFunction: function checkFunction() {
    //擷取body id
    var functionName = $('body > div').data('id')

    // console.log(functionName);
    if (functionName !== undefined) {
      // $('body > div').addClass(functionName);
      $('body > div').attr('id', functionName)
      //呼叫函數( 如果 id = home 輸出的結果為 readyFunction.home(); )
      eval('readyFunction.' + functionName + '();')
    }

    this
    //共用函數呼叫
    readyFunction.common()
  },
  common: function () {
    browserIE()
    active_lbox()
    lazyloadFn()
    menu()
    clickBase.gotop('.gotop', '.btn')
    dataFun.all()
    svg()
    dropkickFn('.dkSel')
    reBrowser()
    lightgalleryFn()
    clickBase.gotop('.gotop', '.m-btn--gotop')
    myload()
    AosFn()
    // textillateFn('.tlt');
  },
  index: function () {
    swiper.index()
  },
  about: function () {
    swiper.about()
    bookmarkFn()
  },
  news: function () {
    bookmarkFn()
  },
  products: function () {
    bookmarkFn()
    productFn()
    swiper.products()
    tabTagFn()
  },
  video: function () {
    bookmarkFn()
  },
  service: function () {
    bookmarkFn()
    swiper.service()
  },
  privacy: function () {},
  knowledge: function () {
    knowledgeFn()
    bookmarkFn()
  },
  search: function () {},
  inquiry: function () {}
}
$(function () {
  readyFunction.checkFunction()
})
