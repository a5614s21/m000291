// JavaScript Document
$(document).ready(function () {
    $('.jqimgFill').imgLiquid();

    $('.report .print-Box').each(function () {
        printPage = 1451;
        printInner = printPage - 100;
        printInner2 = printPage * 2 - 100;
        printheight = $(this).height();
        printInnerPage = Math.floor(printheight / printPage) + 1;
        printName = $(this).attr("title");
        pageNum = 1;

        //設定每區塊高
        $(this).css({
            // height: printPage * printInnerPage
            height: printPage
        });
        //自動分頁
        $(this).find('.pageb').each(function () {
            var top = $(this).position().top,
                height = $(this).height();
            /*第二頁*/
            if (top + height > printPage * pageNum - 100) {
                pagebottomPosition = printPage * pageNum - top - 2;

                $(this).before('<div class="footerH" page=""><div class="footer"><span class="left">@(!string.IsNullOrEmpty(Model.PDFSetting_ViewModel.footer_content) ? Model.PDFSetting_ViewModel.footer_content : "")</span></div></div><div class="head"></div>');
                $(this).prevAll(".footerH").css("height", pagebottomPosition);

                pageNum++;
            }
        });

        /*頁首 - 標題*/
        $(this).find('.head').each(function () {
            var dateObj = new Date();
            var year = dateObj.getFullYear();
            var month = dateObj.getMonth() + 1;
            var day = dateObj.getDate();


            $(this).append('<div class="stitle">本型錄資訊擷取日為' + year + '年' + month + '月' + day + '日</div>');
        });
        /*頁首 - 標題靠左靠右*/
        // $('.report .stitle:even').each(function() {
        // 	$(this).addClass('even');
        // });

        /*頁尾 - 頁碼*/
        $(this).find('.footer').each(function () {
            var index = $('.report .footer').index(this) + 1;
            $(this).append('<span class="copyright">COPYRIGHT © TAIWAN STANCH CO., LTD.</span>' + '<span class="pageNum fw-b">PAGE ' + index + '</span>');
            $(this).attr("page", index);
            $(this).parent().attr("page", index);
        });
    });

    //針對商品加上所屬頁碼
    var page = $(".footerH:first").attr("page") || $(".print-Box.page.product .footer").attr("page")
    $('.pro-list .pageb ,.pro-list .footerH').each(function () {
        $(this).attr('page', page);
        if ($(this).hasClass('footerH')) {
            page++;
        }
    })

    //商品目錄加上商品所屬頁碼
    $("span.contents").each(function () {
        var pguid = $(this).attr("pguid");
        var cguid = $(this).attr("cguid");
        var page = $("#" + pguid + "-" + cguid).attr("page");
        $(this).text(page);
    });
});