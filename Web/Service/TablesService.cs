﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Web.Models.EntityModel;
using Web.Models.Repository;
using System.Web.UI.WebControls;

namespace Web.Service
{
    public class TablesService : Controller
    {
        /// <summary>
        /// 取得列表資料提供給DataTable
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> getListData(string tables, NameValueCollection requests, string urlRoot)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> list = new Dictionary<String, Object>();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            int skip = int.Parse(requests["start"].ToString());
            int take = int.Parse(requests["length"].ToString());
            if (take == -1)
            {
                take = 9999;
            }
            int iTotalDisplayRecords = 0;
            dynamic listData = null;
            dynamic sSearch = null;

            if (!string.IsNullOrEmpty(requests["search[value]"]))
            {
                sSearch = Newtonsoft.Json.Linq.JArray.Parse(requests["search[value]"].ToString());
                sSearch = sSearch[0];
            }

            string columns = requests["order[0][column]"];

            string orderByKey = requests["columns[" + columns + "][data]"].ToString();
            string orderByType = requests["order[0][dir]"].ToString();
            string orderBy = formatOrderByKey(tables, orderByKey) + " " + orderByType.ToUpper();

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //首頁BANNER
                case "index_banner":
                    if (model != null)
                    {
                        var data = model.Repository<index_banner>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //分頁BANNER
                case "page_banner":
                    if (model != null)
                    {
                        var data = model.Repository<page_banner>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords) || x.categoryname.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                #endregion

                #region -- 公司簡介 --

                //發展歷程
                case "course":
                    if (model != null)
                    {
                        var data = model.Repository<course>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //專業認證
                case "certificate":
                    if (model != null)
                    {
                        var data = model.Repository<certificate>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                #endregion

                #region -- 最新消息 --

                //最新消息類別
                case "news_category":
                    if (model != null)
                    {
                        var data = model.Repository<news_category>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //最新消息
                case "news":
                    if (model != null)
                    {
                        var data = model.Repository<news>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(x => x.category == category);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                #endregion

                #region -- 產品專區 --

                //產品品牌
                case "product_brand":
                    if (model != null)
                    {
                        var data = model.Repository<product_brand>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //產品分類
                case "product_category":
                    if (model != null)
                    {
                        var data = model.Repository<product_category>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //產品系列
                case "product_series":
                    if (model != null)
                    {
                        var data = model.Repository<product_series>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //產品icon
                case "product_icondata":
                    if (model != null)
                    {
                        var data = model.Repository<product_icondata>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //產品資料
                case "product_data":
                    if (model != null)
                    {
                        var data = model.Repository<product_data>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["product_category"].ToString() != "")
                            {
                                string category = sSearch["product_category"].ToString();
                                models = models.Where(m => m.product_category == category);
                            }
                            if (sSearch["product_categorybrand"].ToString() != "")
                            {
                                string categorybrand = sSearch["product_categorybrand"].ToString();
                                models = models.Where(m => m.category_brand == categorybrand);
                            }
                            if (sSearch["product_series"].ToString() != "")
                            {
                                string series = sSearch["product_series"].ToString();
                                models = models.Where(m => m.product_series == series);
                            }
                            if (sSearch["product_seriesbrand"].ToString() != "")
                            {
                                string seriesbrand = sSearch["product_seriesbrand"].ToString();
                                models = models.Where(m => m.series_brand == seriesbrand);
                            }

                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                #endregion

                #region -- 資訊小教室 --

                //靜態頁面
                case "knowledge_page":
                    if (model != null)
                    {
                        var data = model.Repository<knowledge_page>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //常見問題
                case "knowledge_data":
                    if (model != null)
                    {
                        var data = model.Repository<knowledge_data>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //產品型錄
                case "knowledge_catalog":
                    if (model != null)
                    {
                        var data = model.Repository<knowledge_catalog>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //生活趣
                case "knowledge_lifestyle":
                    if (model != null)
                    {
                        var data = model.Repository<knowledge_lifestyle>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                #endregion

                #region -- 影音專區 --

                //影音分類
                case "video_category":
                    if (model != null)
                    {
                        var data = model.Repository<video_category>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //影音內容
                case "video_data":
                    if (model != null)
                    {
                        var data = model.Repository<video_data>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(x => x.category == category);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                #endregion

                #region -- 客戶服務 --

                //銷售據點
                case "business_location":
                    if (model != null)
                    {
                        var data = model.Repository<business_location>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(x => x.category == category);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //表單轉寄設定
                case "mail_send":
                    if (model != null)
                    {
                        var data = model.Repository<mail_send>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //諮詢表單
                case "contact":
                    if (model != null)
                    {
                        var data = model.Repository<contact>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.company.Contains(keywords) || x.name.Contains(keywords) || x.phone.Contains(keywords) || x.email.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                #endregion

                //群組管理
                case "roles":
                    if (model != null)
                    {
                        var data = model.Repository<roles>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //使用者
                case "user":
                    if (model != null)
                    {
                        var data = model.Repository<user>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.username != "sysadmin");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.username.Contains(keywords) || m.name.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.role_guid.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //防火牆
                case "firewalls":
                    if (model != null)
                    {
                        var data = model.Repository<firewalls>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //系統日誌
                case "system_log":
                    if (model != null)
                    {
                        var data = model.Repository<system_log>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.ip.Contains(keywords) || m.notes.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //資源回收桶
                case "ashcan":
                    if (model != null)
                    {
                        var data = model.Repository<ashcan>();

                        var models = data.ReadsWhere(m => m.from_guid != "");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
            }

            list.Add("data", dataTableListData(listData, tables, urlRoot, requests));
            list.Add("iTotalDisplayRecords", iTotalDisplayRecords);

            return list;
        }

        /// <summary>
        /// 調整DataTable資料輸出
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static List<Object> dataTableListData(dynamic list, string tables, string urlRoot, NameValueCollection requests)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            Model DB = new Model();

            List<Object> re = new List<Object>();

            Dictionary<String, Object> dataTableRow = dataTableTitle(tables);//資料欄位

            string json = JsonConvert.SerializeObject(list, Formatting.Indented);//轉型(model to Json String)

            var Json1 = Newtonsoft.Json.Linq.JArray.Parse(json);//第一層列表轉json陣列
            string picurlRoot = urlRoot;
            if (urlRoot == "/")
            {
                urlRoot = "";
            }

            //取得選單一些基本資訊

            IQueryable<system_menu> system_menu_search = DB.system_menu.Where(m => m.tables == tables);
            if (requests["category"] != null && requests["category"].ToString() != "")
            {
                string category = requests["category"].ToString();
                string act_path = "list/" + category;
                system_menu_search = system_menu_search.Where(m => m.act_path == act_path);
            }

            var system_menu = system_menu_search.FirstOrDefault();
            if (system_menu == null)
            {
                system_menu = DB.system_menu.Where(m => m.tables == tables).FirstOrDefault();
            }

            int i = 0;
            foreach (var dataList in Json1)
            {
                Dictionary<String, Object> rowData = new Dictionary<string, object>();

                string thisGuid = dataList["guid"].ToString();

                rowData.Add("DT_RowId", "row_" + thisGuid);//欄位ID

                foreach (KeyValuePair<string, object> dataItem in dataTableRow)
                {
                    switch (dataItem.Key.ToString())
                    {
                        #region 鍵值

                        case "guid":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/checkbox.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$title}", tableTitleKey(tables, dataList));
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "indexpic":
                        case "pic":
                        case "listpic":
                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                var key = dataList[dataItem.Key.ToString()].ToString();
                                var u = HttpUtility.UrlDecode(key);

                                if (u.Contains("//"))
                                {
                                    u = u.Replace("//", "/");
                                }

                                if (picurlRoot == "/")
                                {
                                    readText = readText.Replace("{$value}", u);
                                }
                                else
                                {
                                    readText = readText.Replace("{$value}", picurlRoot + u);
                                }

                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "icon":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}", "style=\"background-color:#333;\"");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "list_pic":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 內容摘要

                        case "info_onlytitle":
                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/info_title.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", dataList["title"].ToString());

                                //置頂
                                string sticky = "";
                                if (dataList["sticky"] != null && dataList["sticky"].ToString() != "")
                                {
                                    if (dataList["sticky"].ToString() == "Y")
                                    {
                                        sticky = "<span class=\"badge badge-pill badge-warning ml-2\">置頂</span>";
                                    }
                                }
                                readText = readText.Replace("{$sticky}", sticky);

                                //首頁
                                string index = "";
                                if (dataList["index_sticky"] != null && dataList["index_sticky"].ToString() != "")
                                {
                                    if (dataList["index_sticky"].ToString() == "Y")
                                    {
                                        index = "<span class=\"badge badge-pill badge-danger ml-2\">首頁</span>";
                                    }
                                }
                                readText = readText.Replace("{$index}", index);

                                //今日異動
                                string modifydate = "";
                                if (dataList["modifydate"] != null && dataList["modifydate"].ToString() != "")
                                {
                                    string date = DateTime.Parse(dataList["modifydate"].ToString()).ToString("yyyy-MM-dd");
                                    if (date == DateTime.Now.ToString("yyyy-MM-dd"))
                                    {
                                        modifydate = "<span class=\"badge badge-pill badge-info ml-2\">今日異動</span>";
                                    }
                                }
                                readText = readText.Replace("{$modifydate}", modifydate);

                                //簡述
                                string content = "";
                                readText = readText.Replace("{$content}", content);

                                string notes = "";
                                readText = readText.Replace("{$notes}", notes);

                                //連結
                                string addId = "";
                                //dataTableCategory
                                if (context.Session["dataTableCategory"] != null)
                                {
                                    addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                }
                                readText = readText.Replace("{$url}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        case "info":

                            if (thisGuid != null && thisGuid != "")
                            {
                                if (tables != "shareholders")
                                {
                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/info.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);

                                    readText = readText.Replace("{$value}", dataList["title"].ToString());

                                    //置頂
                                    string sticky = "";
                                    if (dataList["sticky"] != null && dataList["sticky"].ToString() != "")
                                    {
                                        if (dataList["sticky"].ToString() == "Y")
                                        {
                                            sticky = "<span class=\"badge badge-pill badge-warning ml-2\">置頂</span>";
                                        }
                                    }
                                    readText = readText.Replace("{$sticky}", sticky);

                                    //今日異動
                                    string modifydate = "";
                                    if (dataList["modifydate"] != null && dataList["modifydate"].ToString() != "")
                                    {
                                        string date = DateTime.Parse(dataList["modifydate"].ToString()).ToString("yyyy-MM-dd");
                                        if (date == DateTime.Now.ToString("yyyy-MM-dd"))
                                        {
                                            modifydate = "<span class=\"badge badge-pill badge-info ml-2\">今日異動</span>";
                                        }
                                    }
                                    readText = readText.Replace("{$modifydate}", modifydate);

                                    //簡述
                                    string content = "";
                                    if (tables == "product_data")
                                    {
                                        if (dataList["model"] != null && dataList["model"].ToString() != "")
                                        {
                                            content = Regex.Replace(dataList["model"].ToString(), "<.*?>", String.Empty);

                                            if (content.Length > 50)
                                            {
                                                content = content.Substring(0, 50) + "...";
                                            }
                                            content = "<p class=\"m-0 p-0 text-pre-line\"><small>" + content + "</small></p>";
                                        }
                                    }
                                    else
                                    {
                                        if (dataList["content"] != null && dataList["content"].ToString() != "")
                                        {
                                            content = Regex.Replace(dataList["content"].ToString(), "<.*?>", String.Empty);

                                            if (content.Length > 50)
                                            {
                                                content = content.Substring(0, 50) + "...";
                                            }
                                            content = "<p class=\"m-0 p-0 text-pre-line\"><small>" + content + "</small></p>";
                                        }
                                    }
                                    readText = readText.Replace("{$content}", content);

                                    string notes = "";//

                                    readText = readText.Replace("{$notes}", notes);

                                    //連結
                                    string addId = "";
                                    //dataTableCategory
                                    if (context.Session["dataTableCategory"] != null)
                                    {
                                        addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                    }
                                    readText = readText.Replace("{$url}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                                else
                                {
                                    string readText = "<div>" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }
                            break;

                        #endregion

                        #region 分類或其他上層對應

                        case "category":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                switch (tables)
                                {
                                    case "news":
                                        string news_categoryID = dataList["category"].ToString();
                                        var news_category = DB.news_category.Where(x => x.guid == news_categoryID).FirstOrDefault();
                                        readText = news_category.title;
                                        break;

                                    case "video_data":
                                        string video_categoryID = dataList["category"].ToString();
                                        var video_category = DB.video_category.Where(x => x.guid == video_categoryID).FirstOrDefault();
                                        readText = video_category.title;
                                        break;

                                    case "product_subcategory":
                                        string product_categoryID = dataList["category"].ToString();
                                        var product_category = DB.product_category.Where(x => x.guid == product_categoryID).FirstOrDefault();
                                        readText = product_category.title;
                                        break;

                                    case "business_location":
                                        string business_categoryID = dataList["category"].ToString();
                                        if (business_categoryID == "store")
                                        {
                                            readText = "實體銷售據點";
                                        }
                                        else if (business_categoryID == "web")
                                        {
                                            readText = "網路平台銷售";
                                        }
                                        break;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        //類別
                        case "product_category":
                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                string readText = "";
                                string product_categoryID = dataList["product_category"].ToString();
                                var product_category = DB.product_category.Where(x => x.guid == product_categoryID && x.lang == "tw").FirstOrDefault();
                                if (product_category != null)
                                {
                                    readText = product_category.title;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        //系列
                        case "product_series":
                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                string readText = "";
                                string product_seriesID = dataList["product_series"].ToString();
                                var product_series = DB.product_series.Where(x => x.guid == product_seriesID && x.lang == "tw").FirstOrDefault();
                                if (product_series != null)
                                {
                                    readText = product_series.title;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        //品牌
                        case "category_brand":
                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                string readText = "";
                                string product_brandID = dataList["category_brand"].ToString();
                                var product_brand = DB.product_brand.Where(x => x.guid == product_brandID && x.lang == "tw").FirstOrDefault();
                                if (product_brand != null)
                                {
                                    readText = product_brand.title;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        case "series_brand":
                            if (!string.IsNullOrEmpty(thisGuid))
                            {
                                string readText = "";
                                string product_brandID = dataList["series_brand"].ToString();
                                var product_brand = DB.product_brand.Where(x => x.guid == product_brandID && x.lang == "tw").FirstOrDefault();
                                if (product_brand != null)
                                {
                                    readText = product_brand.title;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 複選分類

                        case "pluralCategory":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                if (tables == "user")
                                {
                                    string[] categoryArr = dataList["role_guid"].ToString().Split(',');
                                    for (int s = 0; s < categoryArr.Length; s++)
                                    {
                                        string categoryID = categoryArr[s].ToString();
                                        var category = DB.roles.Where(m => m.guid == categoryID).FirstOrDefault();
                                        if (category != null)
                                        {
                                            readText += category.title + "<br>";
                                        }
                                    }
                                }

                                readText = "<div>" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 審核者

                        case "user":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";

                                if (dataList["user_guid"] != null && dataList["user_guid"].ToString() != "")
                                {
                                    string categoryID = dataList["user_guid"].ToString();
                                    var category = DB.user.Where(m => m.guid == categoryID).FirstOrDefault();
                                    if (category != null)
                                    {
                                        readText = category.name;
                                    }
                                    else
                                    {
                                        readText = "查無審核者!";
                                    }
                                }
                                else
                                {
                                    readText = "未指定";
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 瀏覽次數

                        case "view":

                            /*  if (thisGuid != null && thisGuid != "")
                              {
                                  int record_log_qty = DB.record_log.Where(m => m.from == tables).Where(m => m.from_guid == thisGuid).Where(m => m.types == "view").Count();
                                  string readText = "<div class=\"text-center\">"+ record_log_qty.ToString() + "</div>";
                                  rowData.Add(dataItem.Key.ToString(), readText);
                              }*/
                            break;

                        #endregion

                        #region 日期

                        case "date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 發布日期

                        case "startdate":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 發布日期2

                        case "create_date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 狀態

                        case "status":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/status.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                if (tables == "contact")
                                {
                                    string style = "danger";
                                    string statusSubject = "已回覆";
                                    string status = dataList[dataItem.Key.ToString()].ToString();

                                    if (status == "N")
                                    {
                                        style = "secondary";
                                        statusSubject = "未回覆";
                                    }

                                    readText = readText.Replace("{$style}", style);
                                    readText = readText.Replace("{$statusSubject}", statusSubject);
                                    readText = readText.Replace("{$status}", status);
                                    readText = readText.Replace("{$guid}", thisGuid);
                                    if (tables == "user" || tables == "project_users")
                                    {
                                        readText = readText.Replace("{$title}", dataList["name"].ToString());
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$title}", dataList["title"].ToString());
                                    }

                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                                else
                                {
                                    string style = "danger";
                                    string statusSubject = "啟用";
                                    string status = dataList[dataItem.Key.ToString()].ToString();

                                    if (status == "N")
                                    {
                                        style = "secondary";
                                        statusSubject = "停用";
                                    }

                                    readText = readText.Replace("{$style}", style);
                                    readText = readText.Replace("{$statusSubject}", statusSubject);
                                    readText = readText.Replace("{$status}", status);
                                    readText = readText.Replace("{$guid}", thisGuid);
                                    if (tables == "user" || tables == "project_users")
                                    {
                                        readText = readText.Replace("{$title}", dataList["name"].ToString());
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$title}", dataList["title"].ToString());
                                    }

                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }

                            break;

                        #endregion

                        #region 討論區狀態

                        case "forum_status":

                            if (dataList["status"] != null && dataList["status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "待分派";
                                string status = dataList["status"].ToString();

                                if (status == "disable")
                                {
                                    style = "secondary";
                                    statusSubject = "停用";
                                }
                                if (status == "stagnate")
                                {
                                    style = "dark";
                                    statusSubject = "後續評估";
                                }
                                if (status == "success")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }
                                if (status == "warning")
                                {
                                    style = "warning";
                                    statusSubject = "處理中";
                                }
                                if (status == "end")
                                {
                                    style = "dark";
                                    statusSubject = "已解決";
                                }
                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 回覆狀態

                        case "re_status":

                            if (dataList["re_status"] != null && dataList["re_status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "尚未回覆";
                                string status = dataList["re_status"].ToString();

                                if (status == "Y")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "");
                            }
                            break;

                        #endregion

                        #region 角色

                        case "role":

                            if (dataList["role"] != null && dataList["role"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "primary";
                                string statusSubject = "回覆者";
                                string status = dataList["role"].ToString();

                                if (status == "Tongren")
                                {
                                    style = "secondary";
                                    statusSubject = "同仁";
                                }
                                if (status == "Asker")
                                {
                                    style = "danger";
                                    statusSubject = "提問者";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 屬性

                        case "type":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string readText = "";
                                if (tables == "holiday")
                                {
                                    string type = "休假";
                                    if (dataList[dataItem.Key.ToString()].ToString() == "duty")
                                    {
                                        type = "補上班";
                                    }
                                    readText = type;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 排序

                        case "sortIndex":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/sortIndex.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$guid}", thisGuid);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 成員數

                        case "user_qty":
                            if (dataList["guid"] != null)
                            {
                                string role_guid = dataList["guid"].ToString();
                                var data = DB.user.Where(m => m.role_guid.Contains(role_guid)).Where(m => m.username != "sysadmin").Count();
                                string readText = "<div>" + data + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 來源資料表名稱

                        case "tables":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string fromTable = dataList[dataItem.Key.ToString()].ToString();
                                var data = DB.system_menu.Where(m => m.tables == fromTable).Select(a => new { title = a.title }).FirstOrDefault();
                                rowData.Add(dataItem.Key.ToString(), data.title);
                            }
                            break;

                        #endregion

                        #region 動作

                        case "action":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/action.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$guid}", thisGuid);

                                //是否修改
                                if (system_menu.can_edit == "Y")
                                {
                                    readText = readText.Replace("{$editHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$editHide}", "style=\"display:none;\"");
                                }
                                //是否可刪除
                                dynamic permissions = context.Session["permissions"];//取得權限紀錄
                                if (system_menu.can_del == "Y" && permissions[system_menu.guid] == "F")
                                {
                                    readText = readText.Replace("{$delHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$delHide}", "style=\"display:none;\"");
                                }

                                string addId = "";
                                //dataTableCategory
                                if (context.Session["dataTableCategory"] != null)
                                {
                                    addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                }

                                //連結
                                readText = readText.Replace("{$editUrl}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);

                                if (system_menu.index_view_url != null && system_menu.index_view_url.ToString() != "")
                                {
                                    readText = readText.Replace("{$index_view_url}", "<a href=\"" + urlRoot + system_menu.index_view_url.ToString() + thisGuid + "\" target=\"_blank\" class=\"btn btn-outline-default btn-sm\" role=\"button\" aria-pressed=\"true\" title=\"瀏覽前端\"><i class=\"icon-eye3\"></i><span class=\"text-hide\">瀏覽</span></a>");
                                }
                                else
                                {
                                    readText = readText.Replace("{$index_view_url}", "");
                                }

                                readText = readText.Replace("{$title}", tableTitleKey(tables, dataList));

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 按鈕

                        case "button":
                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/button.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                switch (tables)
                                {
                                    case "cases_city":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/cases_city_history/list/" + thisGuid);
                                        break;

                                    case "progress_cases":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/progress_info/list/" + thisGuid);
                                        break;
                                }

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        case "company_name":
                            string readtext_comapnyname = "";
                            readtext_comapnyname += "<div >" + dataList["company"].ToString() + "</div>";
                            readtext_comapnyname += "<div >" + dataList["name"].ToString() + "</div>";
                            rowData.Add(dataItem.Key.ToString(), readtext_comapnyname);
                            break;

                        case "phone_email":
                            string readtext_phoneemail = "";
                            readtext_phoneemail += "<div >" + dataList["phone"].ToString() + "</div>";
                            readtext_phoneemail += "<div >" + dataList["email"].ToString() + "</div>";
                            rowData.Add(dataItem.Key.ToString(), readtext_phoneemail);
                            break;

                        default:
                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string readText = "<div >" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                    }
                }

                re.Add(rowData);
            }

            return re;
        }

        /// <summary>
        /// 是否使用語系
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string useLang(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //首頁BANNER
                case "index_banner":
                    re = IndexBannerRepository.useLang();
                    break;

                //各頁BANNER
                case "page_banner":
                    re = pageBannerRepository.useLang();
                    break;

                //首頁內容
                case "index_data":
                    re = indexDataRepository.useLang();
                    break;

                //隱私權政策
                case "privacy":
                    re = privacyRepository.useLang();
                    break;

                #endregion

                #region -- 公司簡介 --

                //公司簡介
                case "about":
                    re = aboutRepository.useLang();
                    break;

                //發展里程
                case "course":
                    re = courseRepository.useLang();
                    break;

                //專業認證
                case "certificate":
                    re = certificateRepository.useLang();
                    break;

                #endregion

                #region -- 最新消息 --

                //最新消息類別
                case "news_category":
                    re = newsCategoryRepository.useLang();
                    break;

                //最新消息
                case "news":
                    re = newsRepository.useLang();
                    break;

                #endregion

                #region -- 產品專區 --

                //產品品牌
                case "product_brand":
                    re = productBrandRepository.useLang();
                    break;

                //產品分類
                case "product_category":
                    re = productCategoryRepository.useLang();
                    break;

                //產品系列
                case "product_series":
                    re = productSeriesRepository.useLang();
                    break;

                //產品icon
                case "product_icondata":
                    re = productIconDataRepository.useLang();
                    break;

                //產品資料
                case "product_data":
                    re = productDataRepository.useLang();
                    break;

                #endregion

                #region -- 資訊小教室 --

                //靜態頁面
                case "knowledge_page":
                    re = knowledgePageRepository.useLang();
                    break;

                //常見問題
                case "knowledge_data":
                    re = knowledgeDataRepository.useLang();
                    break;

                //產品型錄
                case "knowledge_catalog":
                    re = knowledgeCatalogRepository.useLang();
                    break;

                //生活趣
                case "knowledge_lifestyle":
                    re = knowledgeLifeStyleRepository.useLang();
                    break;

                #endregion

                #region -- 影音專區 --

                //影音分類
                case "video_category":
                    re = videoCategoryRepository.useLang();
                    break;

                //影音內容
                case "video_data":
                    re = videoDataRepository.useLang();
                    break;

                #endregion

                #region -- 客戶服務 --

                //銷售據點
                case "business_location":
                    re = businessLocationRepository.useLang();
                    break;

                //公司資訊
                case "company_info":
                    re = companyinfoRepository.useLang();
                    break;

                //表單轉寄設定
                case "mail_send":
                    re = mailsendRepository.useLang();
                    break;

                //諮詢表單
                case "contact":
                    re = contactRepository.useLang();
                    break;

                #endregion

                //系統日誌
                case "system_log":
                    re = systemLogRepository.useLang();
                    break;

                //防火牆
                case "firewalls":
                    re = firewallsRepository.useLang();
                    break;

                //群組管理
                case "roles":
                    re = rolesRepository.useLang();
                    break;
                //使用者
                case "user":
                    re = userRepository.useLang();
                    break;

                //網站基本資料
                case "web_data":
                    re = webDataRepository.useLang();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = smtpDataRepository.useLang();
                    break;

                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.useLang();
                    break;
            }

            return re;
        }

        /// <summary>
        /// 取得DataTable欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic dataTableTitle(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //首頁BANNER
                case "index_banner":
                    re = IndexBannerRepository.dataTableTitle();
                    break;

                //各頁BANNER
                case "page_banner":
                    re = pageBannerRepository.dataTableTitle();
                    break;

                #endregion

                #region -- 公司簡介 --

                //發展里程
                case "course":
                    re = courseRepository.dataTableTitle();
                    break;

                //專業認證
                case "certificate":
                    re = certificateRepository.dataTableTitle();
                    break;

                #endregion

                #region -- 最新消息 --

                //最新消息類別
                case "news_category":
                    re = newsCategoryRepository.dataTableTitle();
                    break;

                //最新消息
                case "news":
                    re = newsRepository.dataTableTitle();
                    break;

                #endregion

                #region -- 產品專區 --

                //產品品牌
                case "product_brand":
                    re = productBrandRepository.dataTableTitle();
                    break;

                //產品分類
                case "product_category":
                    re = productCategoryRepository.dataTableTitle();
                    break;

                //產品系列
                case "product_series":
                    re = productSeriesRepository.dataTableTitle();
                    break;

                //產品icon
                case "product_icondata":
                    re = productIconDataRepository.dataTableTitle();
                    break;

                //產品資料
                case "product_data":
                    re = productDataRepository.dataTableTitle();
                    break;

                #endregion

                #region -- 資訊小教室 --

                //常見問題
                case "knowledge_data":
                    re = knowledgeDataRepository.dataTableTitle();
                    break;

                //產品型錄
                case "knowledge_catalog":
                    re = knowledgeCatalogRepository.dataTableTitle();
                    break;

                //生活趣
                case "knowledge_lifestyle":
                    re = knowledgeLifeStyleRepository.dataTableTitle();
                    break;

                #endregion

                #region -- 影音專區 --

                //影音分類
                case "video_category":
                    re = videoCategoryRepository.dataTableTitle();
                    break;

                //影音內容
                case "video_data":
                    re = videoDataRepository.dataTableTitle();
                    break;

                #endregion

                #region -- 客戶服務 --

                //銷售據點
                case "business_location":
                    re = businessLocationRepository.dataTableTitle();
                    break;

                //表單轉寄設定
                case "mail_send":
                    re = mailsendRepository.dataTableTitle();
                    break;

                //諮詢表單
                case "contact":
                    re = contactRepository.dataTableTitle();
                    break;

                #endregion

                //系統日誌
                case "system_log":
                    re = systemLogRepository.dataTableTitle();
                    break;
                //防火牆
                case "firewalls":
                    re = firewallsRepository.dataTableTitle();
                    break;

                //群組管理
                case "roles":
                    re = rolesRepository.dataTableTitle();
                    break;

                //使用者
                case "user":
                    re = userRepository.dataTableTitle();
                    break;

                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.dataTableTitle();
                    break;
            }

            return re;
        }

        /// <summary>
        /// 取得列表說明
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic listMessage(string tables)
        {
            dynamic re = null;
            switch (tables)
            {
                //防火牆
                case "firewalls":
                    re = firewallsRepository.listMessage();
                    break;

                default:
                    re = "";
                    break;
            }

            return re;
        }

        /// <summary>
        /// 欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getColData(string tables, dynamic data)
        {
            dynamic re = null;
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //首頁BANNER
                case "index_banner":
                    re = IndexBannerRepository.colFrom();
                    break;

                //各頁BANNER
                case "page_banner":
                    re = pageBannerRepository.colFrom();
                    break;

                //首頁內容
                case "index_data":
                    re = indexDataRepository.colFrom();
                    break;

                //隱私權政策
                case "privacy":
                    re = privacyRepository.colFrom();
                    break;

                #endregion

                #region -- 公司簡介 --

                //公司簡介
                case "about":
                    re = aboutRepository.colFrom();
                    break;

                //發展里程
                case "course":
                    re = courseRepository.colFrom();
                    break;

                //專業認證
                case "certificate":
                    re = certificateRepository.colFrom();
                    break;

                #endregion

                #region -- 最新消息 --

                //最新消息類別
                case "news_category":
                    re = newsCategoryRepository.colFrom();
                    break;

                //最新消息
                case "news":
                    re = newsRepository.colFrom();
                    break;

                #endregion

                #region -- 產品專區 --

                //產品品牌
                case "product_brand":
                    re = productBrandRepository.colFrom();
                    break;

                //產品分類
                case "product_category":
                    re = productCategoryRepository.colFrom();
                    break;

                //產品系列
                case "product_series":
                    re = productSeriesRepository.colFrom();
                    break;

                //產品icon
                case "product_icondata":
                    re = productIconDataRepository.colFrom();
                    break;

                //產品資料
                case "product_data":
                    re = productDataRepository.colFrom();
                    break;

                #endregion

                #region -- 資訊小教室 --

                //靜態頁面
                case "knowledge_page":
                    re = knowledgePageRepository.colFrom();
                    break;

                //常見問題
                case "knowledge_data":
                    re = knowledgeDataRepository.colFrom();
                    break;

                //產品型錄
                case "knowledge_catalog":
                    re = knowledgeCatalogRepository.colFrom();
                    break;

                //生活趣
                case "knowledge_lifestyle":
                    re = knowledgeLifeStyleRepository.colFrom();
                    break;

                #endregion

                #region -- 影音專區 --

                //影音分類
                case "video_category":
                    re = videoCategoryRepository.colFrom();
                    break;

                //影音內容
                case "video_data":
                    re = videoDataRepository.colFrom();
                    break;

                #endregion

                #region -- 客戶服務 --

                //銷售據點
                case "business_location":
                    re = businessLocationRepository.colFrom();
                    break;

                //公司資訊
                case "company_info":
                    re = companyinfoRepository.colFrom();
                    break;

                //表單轉寄設定
                case "mail_send":
                    re = mailsendRepository.colFrom();
                    break;

                //諮詢表單
                case "contact":
                    re = contactRepository.colFrom();
                    break;

                #endregion

                //系統日誌
                case "system_log":
                    re = systemLogRepository.colFrom();
                    break;
                //防火牆
                case "firewalls":
                    re = firewallsRepository.colFrom();
                    break;

                //群組管理
                case "roles":
                    re = rolesRepository.colFrom();
                    break;
                //使用者
                case "user":
                    re = userRepository.colFrom();
                    break;

                case "edituser":
                    re = userRepository.EditcolFrom();
                    break;

                //網站基本資料
                case "web_data":
                    re = webDataRepository.colFrom();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = smtpDataRepository.colFrom();
                    break;

                //系統參數
                case "system_data":
                    re = systemDataRepository.colFrom();
                    break;

                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.colFrom();
                    break;
            }

            return re;
        }

        /// <summary>
        /// 取得資料庫資料
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getPresetData(string tables, string guid)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            string re = "";

            Dictionary<String, Object> reValData = new Dictionary<String, Object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //首頁BANNER
                case "index_banner":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<index_banner>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //各頁BANNER
                case "page_banner":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<page_banner>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //首頁內容
                case "index_data":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<index_data>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //隱私權政策
                case "privacy":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<privacy>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #endregion

                #region -- 公司簡介 --

                //公司簡介
                case "about":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<about>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //發展里程
                case "course":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<course>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //專業認證
                case "certificate":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<certificate>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #endregion

                #region -- 最新消息 --

                //最新消息類別
                case "news_category":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<news_category>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //最新消息
                case "news":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<news>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #endregion

                #region -- 產品專區 --

                //產品品牌
                case "product_brand":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<product_brand>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //產品分類
                case "product_category":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<product_category>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //產品系列
                case "product_series":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<product_series>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //產品icon
                case "product_icondata":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<product_icondata>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //產品資料
                case "product_data":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<product_data>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #endregion

                #region -- 資訊小教室 --

                //靜態頁面
                case "knowledge_page":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<knowledge_page>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //常見問題
                case "knowledge_data":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<knowledge_data>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //產品型錄
                case "knowledge_catalog":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<knowledge_catalog>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //生活趣
                case "knowledge_lifestyle":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<knowledge_lifestyle>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #endregion

                #region -- 影音專區 --

                //影音分類
                case "video_category":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<video_category>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //影音內容
                case "video_data":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<video_data>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #endregion

                #region -- 客戶服務 --

                //銷售據點
                case "business_location":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<business_location>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //公司資訊
                case "company_info":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<company_info>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //表單轉寄設定
                case "mail_send":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<mail_send>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //諮詢表單
                case "contact":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<contact>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #endregion

                //系統日誌
                case "system_log":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_log>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //防火牆
                case "firewalls":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<firewalls>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //群組管理
                case "roles":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<roles>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //使用者
                case "user":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<user>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //網站基本資料
                case "web_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<web_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //系統參數
                case "system_data":

                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_data>();
                        Guid newGuid = Guid.Parse(guid);
                        var tempData = data.ReadsWhere(m => m.guid == newGuid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<smtp_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
            }

            return re;
        }

        /// <summary>
        /// 取得分類
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getCategory(string tables, dynamic data = null)
        {
            Model DB = new Model();
            dynamic re = null;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            switch (tables)
            {
                //銷售據點
                case "business_location":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        Dictionary<string, string> result = new Dictionary<string, string>();
                        result.Add("實體銷售據點", "store");
                        result.Add("網路平台銷售", "web");
                        re = result;
                    }
                    break;

                //最新消息類別
                case "news_category":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        re = DB.news_category.Where(x => x.status == "Y" && x.lang == "tw").ToList();
                    }
                    break;

                //產品品牌
                case "products_brand":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        Dictionary<string, List<product_brand>> result = new Dictionary<string, List<product_brand>>();
                        var brand = DB.product_brand.Where(x => x.status == "Y" && x.lang == defLang).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                        result.Add("品牌", brand);
                        re = result;
                    }
                    break;

                //產品ICON
                case "products_icondata":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        Dictionary<string, List<product_icondata>> result = new Dictionary<string, List<product_icondata>>();
                        var icondata = DB.product_icondata.Where(x => x.status == "Y" && x.lang == defLang).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                        result.Add("icon", icondata);
                        re = result;
                    }
                    break;

                //產品分類
                case "product_category":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        re = DB.product_category.Where(x => x.status == "Y" && x.lang == "tw").OrderBy(x => x.sortIndex).ToList();
                    }
                    break;

                case "category_brand":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        if (data != null)
                        {
                            ProductBrandService ProductBrand = new ProductBrandService();
                            re = ProductBrand.GetCategoryBrandDataByProductGuid(data.Value.ToString());
                        }
                        else
                        {
                            re = null;
                        }
                    }
                    break;

                //產品系列
                case "product_series":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        //re = DB.product_series.Where(x => x.status == "Y" && x.lang == "tw").OrderBy(x => x.sortIndex).ToList();

                        Dictionary<string, List<product_series>> result = new Dictionary<string, List<product_series>>();
                        var series = DB.product_series.Where(x => x.status == "Y" && x.lang == "tw").OrderBy(x => x.sortIndex).ToList();
                        result.Add("系列", series);
                        re = result;
                    }
                    break;

                //產品系列
                case "product_series_lang":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        re = DB.product_series.Where(x => x.status == "Y" && x.lang == "tw").OrderBy(x => x.sortIndex).ToList();
                    }
                    break;

                //產品系列
                case "product_serieslist":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        re = DB.product_series.Where(x => x.status == "Y" && x.lang == "tw").OrderBy(x => x.sortIndex).ToList();
                    }
                    break;

                case "series_brand":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        if (data != null)
                        {
                            ProductBrandService productBrand = new ProductBrandService();
                            re = productBrand.GetSeriesBrandDataByProductGuid(data.Value.ToString());
                        }
                        else
                        {
                            re = null;
                        }
                    }
                    break;

                //影音分類
                case "video_category":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        re = DB.video_category.Where(x => x.status == "Y" && x.lang == "tw").ToList();
                    }
                    break;

                //影音專區
                case "products_video":
                    if (!string.IsNullOrEmpty(tables))
                    {
                        Dictionary<string, List<video_data>> result = new Dictionary<string, List<video_data>>();
                        VideoDataService Video = new VideoDataService();
                        result = Video.GetSelectMultipleData();
                        re = result;
                    }
                    break;

                //群組
                case "roles":
                    if (tables != null && tables != "")
                    {
                        re = DB.roles.Where(m => m.status == "Y").ToList();
                    }
                    break;

                //使用者
                case "user":
                    if (tables != null && tables != "")
                    {
                        re = DB.user.Where(m => m.status == "Y").ToList();
                    }
                    break;
            }

            return re;
        }

        /// <summary>
        /// 回傳標題
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getDataTitle(string tables, string guid)
        {
            Model DB = new Model();
            string re = "";
            /* switch (tables)
            {
            }*/

            return re;
        }

        /// <summary>
        /// 複選選單選取判斷用
        /// </summary>
        /// <param name="tabels"></param>
        /// <param name="user_guid"></param>
        /// <param name="forum_guid"></param>
        /// <returns></returns>
        public static string selectMultipleSelected(string tabels, string user_guid, string forum_guid)
        {
            Model DB = new Model();

            string selected = "";
            /*
            switch(tabels)
            {
                case "forum_message":

                    int data = DB.forum_message.Where(m=>m.user_guid == user_guid).Where(m=>m.forum_guid == forum_guid).Select(a => new { guid = a.guid }).Count();
                    if(data > 0)
                    {
                        selected = " selected";
                    }
                    break;
            }
            */

            return selected;
        }

        /// <summary>
        /// 新增 修改
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="form"></param>
        /// <param name="Field"></param>
        /// <returns></returns>
        public static string saveData(string tables, string form, string Field, string guid, string actType, Dictionary<String, Object> FormObj = null)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            dynamic Model = null;//目前Model
            dynamic FromData = null;//表單資訊
            string lang = "";

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //首頁BANNER
                case "index_banner":
                    FromData = JsonConvert.DeserializeObject<index_banner>(form);
                    Model = model.Repository<index_banner>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.index_banner.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //各頁BANNER
                case "page_banner":
                    FromData = JsonConvert.DeserializeObject<page_banner>(form);
                    Model = model.Repository<page_banner>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.page_banner.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //首頁內容
                case "index_data":
                    FromData = JsonConvert.DeserializeObject<index_data>(form);
                    Model = model.Repository<index_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.index_data.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //隱私權政策
                case "privacy":
                    FromData = JsonConvert.DeserializeObject<privacy>(form);
                    Model = model.Repository<privacy>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.privacy.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                #endregion

                #region -- 公司簡介 --

                //公司簡介
                case "about":
                    FromData = JsonConvert.DeserializeObject<about>(form);
                    Model = model.Repository<about>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.about.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //發展里程
                case "course":
                    FromData = JsonConvert.DeserializeObject<course>(form);
                    Model = model.Repository<course>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.course.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //專業認證
                case "certificate":
                    FromData = JsonConvert.DeserializeObject<certificate>(form);
                    Model = model.Repository<certificate>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.certificate.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                #endregion

                #region -- 最新消息 --

                //最新消息類別
                case "news_category":
                    FromData = JsonConvert.DeserializeObject<news_category>(form);
                    Model = model.Repository<news_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news_category.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //最新消息
                case "news":
                    FromData = JsonConvert.DeserializeObject<news>(form);
                    Model = model.Repository<news>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                #endregion

                #region -- 產品專區 --

                //產品品牌
                case "product_brand":
                    FromData = JsonConvert.DeserializeObject<product_brand>(form);
                    Model = model.Repository<product_brand>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.product_brand.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //產品分類
                case "product_category":
                    FromData = JsonConvert.DeserializeObject<product_category>(form);
                    Model = model.Repository<product_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.product_category.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //產品系列
                case "product_series":
                    FromData = JsonConvert.DeserializeObject<product_series>(form);
                    Model = model.Repository<product_series>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.product_series.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //產品icon
                case "product_icondata":
                    FromData = JsonConvert.DeserializeObject<product_icondata>(form);
                    Model = model.Repository<product_icondata>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.product_icondata.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //產品資料
                case "product_data":
                    FromData = JsonConvert.DeserializeObject<product_data>(form);
                    Model = model.Repository<product_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.product_data.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                #endregion

                #region -- 資訊小教室 --

                //靜態頁面
                case "knowledge_page":
                    FromData = JsonConvert.DeserializeObject<knowledge_page>(form);
                    Model = model.Repository<knowledge_page>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.knowledge_page.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //常見問題
                case "knowledge_data":
                    FromData = JsonConvert.DeserializeObject<knowledge_data>(form);
                    Model = model.Repository<knowledge_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.knowledge_data.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //產品型錄
                case "knowledge_catalog":
                    FromData = JsonConvert.DeserializeObject<knowledge_catalog>(form);
                    Model = model.Repository<knowledge_catalog>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.knowledge_catalog.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //生活趣
                case "knowledge_lifestyle":
                    FromData = JsonConvert.DeserializeObject<knowledge_lifestyle>(form);
                    Model = model.Repository<knowledge_lifestyle>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.knowledge_lifestyle.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                #endregion

                #region -- 影音專區 --

                //影音分類
                case "video_category":
                    FromData = JsonConvert.DeserializeObject<video_category>(form);
                    Model = model.Repository<video_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.video_category.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //影音內容
                case "video_data":
                    FromData = JsonConvert.DeserializeObject<video_data>(form);
                    Model = model.Repository<video_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.video_data.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                #endregion

                #region -- 客戶服務 --

                //銷售據點
                case "business_location":
                    FromData = JsonConvert.DeserializeObject<business_location>(form);
                    Model = model.Repository<business_location>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.business_location.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //公司資訊
                case "company_info":
                    FromData = JsonConvert.DeserializeObject<company_info>(form);
                    Model = model.Repository<company_info>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.company_info.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //表單轉寄設定
                case "mail_send":
                    FromData = JsonConvert.DeserializeObject<mail_send>(form);
                    Model = model.Repository<mail_send>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.mail_send.Where(x => x.guid == guid && x.lang == lang).FirstOrDefault();
                    }
                    break;

                //諮詢表單
                case "contact":
                    FromData = JsonConvert.DeserializeObject<contact>(form);
                    Model = model.Repository<contact>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.contact.Where(x => x.guid == guid && x.lang == "tw").FirstOrDefault();
                    }
                    break;

                #endregion

                //系統日誌
                case "system_log":

                    FromData = JsonConvert.DeserializeObject<system_log>(form);
                    Model = model.Repository<system_log>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.system_log.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //防火牆
                case "firewalls":

                    FromData = JsonConvert.DeserializeObject<firewalls>(form);
                    Model = model.Repository<firewalls>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.firewalls.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //群組管理
                case "roles":

                    FromData = JsonConvert.DeserializeObject<roles>(form);
                    Model = model.Repository<roles>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.roles.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;

                //網站基本資料
                case "web_data":
                    FromData = JsonConvert.DeserializeObject<web_data>(form);
                    Model = model.Repository<web_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.web_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    FromData = JsonConvert.DeserializeObject<smtp_data>(form);
                    Model = model.Repository<smtp_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.smtp_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //資源回收桶
                case "ashcan":

                    FromData = JsonConvert.DeserializeObject<ashcan>(form);
                    Model = model.Repository<ashcan>();
                    if (Field != "")
                    {
                        Guid NewGuid = Guid.Parse(FromData.guid.ToString());
                        Model = DB.ashcan.Where(m => m.guid == NewGuid).FirstOrDefault();
                    }

                    break;
                //使用者
                case "user":

                    FromData = JsonConvert.DeserializeObject<user>(form);
                    Model = model.Repository<user>();

                    if (FromData.password != null && FromData.password != "")
                    {
                        FromData.password = FunctionService.md5(FromData.password);
                    }
                    //無輸入密碼進入修改情況下
                    if (FromData.password == "" && FromData.guid != "")
                    {
                        FromData.password = FormObj["defPassword"].ToString();
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.user.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;

                //系統參數
                case "system_data":

                    FromData = JsonConvert.DeserializeObject<system_data>(form);
                    Model = model.Repository<system_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Guid newGuid = Guid.Parse(guid);
                        Model = DB.system_data.Where(m => m.guid == newGuid).FirstOrDefault();
                    }

                    break;
            }

            FromData.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            if (actType == "add")
            {
                FromData.guid = guid;
                FromData.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                Model.Create(FromData);
                Model.SaveChanges();
            }
            else
            {
                if (Field == "")
                {
                    Model.Update(FromData);
                    Model.SaveChanges();

                    //回覆信件
                    if (tables == "contact")
                    {
                        if (FromData.replystatus == "Y" && FromData.status == "Y" && string.IsNullOrEmpty(FromData.replied))
                        {
                            Web.Controllers.ContactController Contact = new Controllers.ContactController();
                            Contact.SendConatctReply(guid);
                        }
                    }
                }
                else
                {
                    //單一欄位修改
                    switch (Field)
                    {
                        case "status":
                            Model.status = FromData.status;

                            if (tables == "product_brand" && FromData.status == "D")
                            {
                                ProductBrandService ProductBrand = new ProductBrandService();
                                ProductBrand.DeleteBrand_Category(FromData.guid);
                                ProductBrand.DeleteBrand_Series(FromData.guid);
                            }
                            break;

                        case "sortIndex":
                            Model.sortIndex = FromData.sortIndex;
                            break;

                        case "logindate":
                            Model.logindate = FromData.logindate;
                            break;
                    }

                    DB.SaveChanges();
                }
            }

            if (FormObj != null)
            {
                #region 權限

                if (FormObj.ContainsKey("permissions") && FormObj["permissions"] != null && FormObj["permissions"].ToString() != "")
                {
                    NameValueCollection permissions = FunctionService.reSubmitFormDataJson(FormObj["permissions"].ToString());

                    role_permissions role_permissions_add = new role_permissions();

                    var delData = DB.role_permissions.Where(m => m.role_guid == guid).ToList();

                    foreach (var item in delData)
                    {
                        DB.role_permissions.Remove((role_permissions)item);
                    }

                    foreach (string key in permissions)
                    {
                        //Guid newGuid = Guid.NewGuid();
                        role_permissions_add = new role_permissions();
                        role_permissions_add.guid = Guid.NewGuid();
                        role_permissions_add.role_guid = guid;
                        role_permissions_add.permissions_guid = key.ToString();
                        role_permissions_add.permissions_status = permissions[key].ToString();
                        DB.role_permissions.Add(role_permissions_add);
                        DB.SaveChanges();
                    }
                }

                #endregion
            }

            return guid;
        }

        /// <summary>
        /// 取得預設排序
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy(string tables)
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //首頁BANNER
                case "index_banner":
                    re.Clear();
                    re = IndexBannerRepository.defaultOrderBy();
                    break;

                //各頁BANNER
                case "page_banner":
                    re.Clear();
                    re = pageBannerRepository.defaultOrderBy();
                    break;

                #endregion

                #region -- 公司簡介 --

                //發展歷程
                case "course":
                    re.Clear();
                    re = courseRepository.defaultOrderBy();
                    break;

                //專業認證
                case "certificate":
                    re = certificateRepository.defaultOrderBy();
                    break;

                #endregion

                #region -- 最新消息 --

                //最新消息類別
                case "news_category":
                    re.Clear();
                    re = newsCategoryRepository.defaultOrderBy();
                    break;

                //最新消息
                case "news":
                    re.Clear();
                    re = newsRepository.defaultOrderBy();
                    break;

                #endregion

                #region -- 產品專區 --

                //產品品牌
                case "product_brand":
                    re.Clear();
                    re = productBrandRepository.defaultOrderBy();
                    break;

                //產品分類
                case "product_category":
                    re.Clear();
                    re = productCategoryRepository.defaultOrderBy();
                    break;

                //產品系列
                case "product_series":
                    re.Clear();
                    re = productSeriesRepository.defaultOrderBy();
                    break;

                //產品icon
                case "product_icondata":
                    re.Clear();
                    re = productIconDataRepository.defaultOrderBy();
                    break;

                //產品資料
                case "product_data":
                    re.Clear();
                    re = productDataRepository.defaultOrderBy();
                    break;

                #endregion

                #region -- 資訊小教室 --

                //常見問題
                case "knowledge_data":
                    re.Clear();
                    re = knowledgeDataRepository.defaultOrderBy();
                    break;

                //產品型錄
                case "knowledge_catalog":
                    re.Clear();
                    re = knowledgeCatalogRepository.defaultOrderBy();
                    break;

                //生活趣
                case "knowledge_lifestyle":
                    re.Clear();
                    re = knowledgeLifeStyleRepository.defaultOrderBy();
                    break;

                #endregion

                #region -- 影音專區 --

                //影音分類
                case "video_category":
                    re.Clear();
                    re = videoCategoryRepository.defaultOrderBy();
                    break;

                //影音內容
                case "video_data":
                    re.Clear();
                    re = videoDataRepository.defaultOrderBy();
                    break;

                #endregion

                #region -- 客戶服務 --

                //銷售據點
                case "business_location":
                    re.Clear();
                    re = businessLocationRepository.defaultOrderBy();
                    break;

                //表單轉寄設定
                case "mail_send":
                    re.Clear();
                    re = mailsendRepository.defaultOrderBy();
                    break;

                //諮詢表單
                case "contact":
                    re.Clear();
                    re = contactRepository.defaultOrderBy();
                    break;

                #endregion

                //系統日誌
                case "system_log":
                    re.Clear();
                    re = systemLogRepository.defaultOrderBy();
                    break;
                //防火牆
                case "firewalls":
                    re.Clear();
                    re = firewallsRepository.defaultOrderBy();
                    break;
            }
            return re;
        }

        /// <summary>
        /// 格式化排序欄位
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="orderByKey"></param>
        /// <returns></returns>
        public static string formatOrderByKey(string tables, string orderByKey)
        {
            string re = "";
            switch (orderByKey)
            {
                case "info":
                    re = "title";
                    break;

                case "view_info":
                    re = "nums";
                    break;

                case "forum_status":
                    re = "status";
                    break;

                case "action":
                    re = "guid";
                    break;

                case "user":
                    re = "user_guid";
                    break;

                case "category":
                    re = "category";
                    if (tables == "package_content")
                    {
                        re = "package_guid";
                    }
                    break;

                default:
                    re = orderByKey;
                    break;
            }
            return re;
        }

        /// <summary>
        /// 回傳資料表的標題或姓名
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string tableTitleKey(string tables, dynamic dataList)
        {
            Model DB = new Model();
            string re = "";
            switch (tables)
            {
                case "user":
                    re = dataList["name"];
                    break;

                default:
                    re = dataList["title"];
                    break;
            }
            return re;
        }
    }
}