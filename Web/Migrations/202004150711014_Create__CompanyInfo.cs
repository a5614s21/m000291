namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__CompanyInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.company_info",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        contetne = c.String(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            AddColumn("dbo.business_location", "modifydate", c => c.DateTime());
            AddColumn("dbo.business_location", "create_date", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.business_location", "create_date");
            DropColumn("dbo.business_location", "modifydate");
            DropTable("dbo.company_info");
        }
    }
}
