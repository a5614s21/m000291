namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__NotepadData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.notepad_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        notepad_guid = c.String(),
                        product_guid = c.String(),
                        product_pic = c.String(),
                        product_title = c.String(),
                        product_model = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.notepad_data");
        }
    }
}
