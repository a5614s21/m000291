namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__ProductData1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_data", "product_category", c => c.String());
            AddColumn("dbo.product_data", "product_series", c => c.String());
            DropColumn("dbo.product_data", "category");
            DropColumn("dbo.product_data", "series");
        }
        
        public override void Down()
        {
            AddColumn("dbo.product_data", "series", c => c.String());
            AddColumn("dbo.product_data", "category", c => c.String());
            DropColumn("dbo.product_data", "product_series");
            DropColumn("dbo.product_data", "product_category");
        }
    }
}
