namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_prev_table_to_system_menu_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.system_menu", "prev_table", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'�W�h��ƪ�' ,'SCHEMA', N'dbo','TABLE', N'system_menu', 'COLUMN', N'prev_table'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.system_menu", "prev_table");
        }
    }
}
