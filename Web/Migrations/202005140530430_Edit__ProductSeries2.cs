namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__ProductSeries2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_series", "index_sticky", c => c.String(maxLength: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_series", "index_sticky");
        }
    }
}
