namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addProductCategoryIconAlt : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_category", "icon_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_category", "icon_alt");
        }
    }
}
