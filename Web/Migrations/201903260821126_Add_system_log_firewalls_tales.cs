namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_system_log_firewalls_tales : DbMigration
    {
        public override void Up()
        {

            Sql("execute sp_addextendedproperty 'MS_Description', N'來源' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'IP位置' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'ip'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'屬性' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'types'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'來源資料表' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'tables'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'來源資料表鍵值' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'tables_guid'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'備註' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'modifydate'");


            Sql("execute sp_addextendedproperty 'MS_Description', N'名稱' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'IP位置' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'ip'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'其他敘述' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'firewalls', 'COLUMN', N'modifydate'");
        }
        
        public override void Down()
        {
        }
    }
}
