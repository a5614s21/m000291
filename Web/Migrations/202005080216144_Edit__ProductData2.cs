namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__ProductData2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_data", "url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_data", "url");
        }
    }
}
