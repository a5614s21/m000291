namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Clear : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.news", new[] { "news_categoryid", "news_categoryguid" }, "dbo.news_category");
            DropIndex("dbo.news", new[] { "news_categoryid", "news_categoryguid" });
            DropColumn("dbo.news", "news_categoryid");
            DropColumn("dbo.news", "news_categoryguid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.news", "news_categoryguid", c => c.String(maxLength: 64));
            AddColumn("dbo.news", "news_categoryid", c => c.Int(nullable: false));
            CreateIndex("dbo.news", new[] { "news_categoryid", "news_categoryguid" });
            AddForeignKey("dbo.news", new[] { "news_categoryid", "news_categoryguid" }, "dbo.news_category", new[] { "id", "guid" });
        }
    }
}
