namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__ProductSeries1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_series", "banner", c => c.String());
            AddColumn("dbo.product_series", "banner_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_series", "banner_alt");
            DropColumn("dbo.product_series", "banner");
        }
    }
}
