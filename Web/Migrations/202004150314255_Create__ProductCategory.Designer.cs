// <auto-generated />
namespace Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Create__ProductCategory : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Create__ProductCategory));
        
        string IMigrationMetadata.Id
        {
            get { return "202004150314255_Create__ProductCategory"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
