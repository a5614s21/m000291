namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__Test : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.news");
            DropPrimaryKey("dbo.news_category");
            AddColumn("dbo.news", "NewsCategory_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.news", "guid", c => c.String(maxLength: 64));
            AlterColumn("dbo.news_category", "guid", c => c.String(maxLength: 64));
            AddPrimaryKey("dbo.news", "id");
            AddPrimaryKey("dbo.news_category", "id");
            CreateIndex("dbo.news", "NewsCategory_Id");
            AddForeignKey("dbo.news", "NewsCategory_Id", "dbo.news_category", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.news", "NewsCategory_Id", "dbo.news_category");
            DropIndex("dbo.news", new[] { "NewsCategory_Id" });
            DropPrimaryKey("dbo.news_category");
            DropPrimaryKey("dbo.news");
            AlterColumn("dbo.news_category", "guid", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.news", "guid", c => c.String(nullable: false, maxLength: 64));
            DropColumn("dbo.news", "NewsCategory_Id");
            AddPrimaryKey("dbo.news_category", new[] { "id", "guid" });
            AddPrimaryKey("dbo.news", new[] { "id", "guid" });
        }
    }
}
