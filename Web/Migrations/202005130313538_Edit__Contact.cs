namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__Contact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.contact", "email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.contact", "email");
        }
    }
}
