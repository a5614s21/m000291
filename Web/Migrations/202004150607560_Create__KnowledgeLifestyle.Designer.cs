// <auto-generated />
namespace Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Create__KnowledgeLifestyle : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Create__KnowledgeLifestyle));
        
        string IMigrationMetadata.Id
        {
            get { return "202004150607560_Create__KnowledgeLifestyle"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
