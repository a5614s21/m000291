namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__News3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "sticky", c => c.String(maxLength: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "sticky");
        }
    }
}
