namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__News2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "news_categoryid", c => c.Int(nullable: false));
            AddColumn("dbo.news", "news_categoryguid", c => c.String(maxLength: 64));
            CreateIndex("dbo.news", new[] { "news_categoryid", "news_categoryguid" });
            AddForeignKey("dbo.news", new[] { "news_categoryid", "news_categoryguid" }, "dbo.news_category", new[] { "id", "guid" });
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.news", new[] { "news_categoryid", "news_categoryguid" }, "dbo.news_category");
            DropIndex("dbo.news", new[] { "news_categoryid", "news_categoryguid" });
            DropColumn("dbo.news", "news_categoryguid");
            DropColumn("dbo.news", "news_categoryid");
        }
    }
}
