namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__News4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "pic", c => c.String());
            AddColumn("dbo.news", "pic_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "pic_alt");
            DropColumn("dbo.news", "pic");
        }
    }
}
