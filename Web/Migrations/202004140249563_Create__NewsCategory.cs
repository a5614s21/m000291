namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__NewsCategory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.news_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.news_category");
        }
    }
}
