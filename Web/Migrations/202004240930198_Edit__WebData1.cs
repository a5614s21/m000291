namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__WebData1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "code");
        }
    }
}
