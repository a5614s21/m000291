namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__VideoData2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.video_data", "listpic", c => c.String());
            AddColumn("dbo.video_data", "listpic_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.video_data", "listpic_alt");
            DropColumn("dbo.video_data", "listpic");
        }
    }
}
