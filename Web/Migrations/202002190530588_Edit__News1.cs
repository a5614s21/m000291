namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__News1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.news", new[] { "NewsCategory_id", "NewsCategory_guid" }, "dbo.news_category");
            DropIndex("dbo.news", new[] { "NewsCategory_id", "NewsCategory_guid" });
            DropColumn("dbo.news", "NewsCategory_id");
            DropColumn("dbo.news", "NewsCategory_guid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.news", "NewsCategory_guid", c => c.String(maxLength: 64));
            AddColumn("dbo.news", "NewsCategory_id", c => c.Int());
            CreateIndex("dbo.news", new[] { "NewsCategory_id", "NewsCategory_guid" });
            AddForeignKey("dbo.news", new[] { "NewsCategory_id", "NewsCategory_guid" }, "dbo.news_category", new[] { "id", "guid" });
        }
    }
}
