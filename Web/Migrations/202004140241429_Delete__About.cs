namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Delete__About : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.abouts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.abouts",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content_1 = c.String(storeType: "ntext"),
                        content_2 = c.String(storeType: "ntext"),
                        content_3 = c.String(storeType: "ntext"),
                        content_4 = c.String(storeType: "ntext"),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
    }
}
