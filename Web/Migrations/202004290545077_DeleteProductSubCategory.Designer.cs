// <auto-generated />
namespace Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class DeleteProductSubCategory : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DeleteProductSubCategory));
        
        string IMigrationMetadata.Id
        {
            get { return "202004290545077_DeleteProductSubCategory"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
