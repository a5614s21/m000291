namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__Certificate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.certificate",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.certificate");
        }
    }
}
