namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__Test1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.news", "NewsCategory_Id", "dbo.news_category");
            DropIndex("dbo.news", new[] { "NewsCategory_Id" });
            DropPrimaryKey("dbo.news");
            DropPrimaryKey("dbo.news_category");
            AlterColumn("dbo.news", "guid", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.news_category", "guid", c => c.String(nullable: false, maxLength: 64));
            AddPrimaryKey("dbo.news", new[] { "id", "guid" });
            AddPrimaryKey("dbo.news_category", new[] { "id", "guid" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.news_category");
            DropPrimaryKey("dbo.news");
            AlterColumn("dbo.news_category", "guid", c => c.String(maxLength: 64));
            AlterColumn("dbo.news", "guid", c => c.String(maxLength: 64));
            AddPrimaryKey("dbo.news_category", "id");
            AddPrimaryKey("dbo.news", "id");
            CreateIndex("dbo.news", "NewsCategory_Id");
            AddForeignKey("dbo.news", "NewsCategory_Id", "dbo.news_category", "id", cascadeDelete: true);
        }
    }
}
