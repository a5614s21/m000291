namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__News5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "banner", c => c.String());
            AddColumn("dbo.news", "banner_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "banner_alt");
            DropColumn("dbo.news", "banner");
        }
    }
}
