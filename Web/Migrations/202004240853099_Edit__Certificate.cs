namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__Certificate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.certificate", "bpic", c => c.String());
            AddColumn("dbo.certificate", "bpic_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.certificate", "bpic_alt");
            DropColumn("dbo.certificate", "bpic");
        }
    }
}
