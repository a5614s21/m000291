namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__CompanyInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.company_info", "content", c => c.String());
            DropColumn("dbo.company_info", "contetne");
        }
        
        public override void Down()
        {
            AddColumn("dbo.company_info", "contetne", c => c.String());
            DropColumn("dbo.company_info", "content");
        }
    }
}
