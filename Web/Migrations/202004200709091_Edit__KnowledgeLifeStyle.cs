namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__KnowledgeLifeStyle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.knowledge_lifestyle", "pic", c => c.String());
            AddColumn("dbo.knowledge_lifestyle", "pic_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.knowledge_lifestyle", "pic_alt");
            DropColumn("dbo.knowledge_lifestyle", "pic");
        }
    }
}
