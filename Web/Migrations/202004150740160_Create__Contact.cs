namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__Contact : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.contact",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        company = c.String(),
                        name = c.String(),
                        phone = c.String(),
                        fax = c.String(),
                        address = c.String(),
                        product = c.String(),
                        message = c.String(storeType: "ntext"),
                        response = c.String(storeType: "ntext"),
                        remarks = c.String(storeType: "ntext"),
                        replystatus = c.String(maxLength: 1),
                        replied = c.String(maxLength: 1),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        replydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        form_lang = c.String(maxLength: 30),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.contact");
        }
    }
}
