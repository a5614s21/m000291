namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__WebData2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "headquarters", c => c.String(maxLength: 255));
            DropColumn("dbo.web_data", "ext_num");
        }
        
        public override void Down()
        {
            AddColumn("dbo.web_data", "ext_num", c => c.String(maxLength: 255));
            DropColumn("dbo.web_data", "headquarters");
        }
    }
}
