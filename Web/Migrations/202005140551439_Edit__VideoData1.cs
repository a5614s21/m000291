namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__VideoData1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.video_data", "indexpic", c => c.String());
            AddColumn("dbo.video_data", "indexpic_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.video_data", "indexpic_alt");
            DropColumn("dbo.video_data", "indexpic");
        }
    }
}
