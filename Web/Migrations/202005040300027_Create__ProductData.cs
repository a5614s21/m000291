namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__ProductData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.product_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        model = c.String(),
                        indexpic = c.String(),
                        indexpic_alt = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        icondata = c.String(),
                        spec = c.String(),
                        feature = c.String(),
                        method = c.String(),
                        video = c.String(),
                        sortIndex = c.Int(),
                        startdate = c.DateTime(),
                        enddate = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.product_data");
        }
    }
}
