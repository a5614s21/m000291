namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__News7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "url");
        }
    }
}
