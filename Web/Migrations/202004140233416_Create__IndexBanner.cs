namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__IndexBanner : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.index_banner",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        url = c.String(),
                        content = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        phonepic = c.String(),
                        phonepic_alt = c.String(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.index_banner");
        }
    }
}
