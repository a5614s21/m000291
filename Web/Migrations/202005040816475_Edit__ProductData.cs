namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__ProductData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_data", "category", c => c.String());
            AddColumn("dbo.product_data", "category_brand", c => c.String());
            AddColumn("dbo.product_data", "series", c => c.String());
            AddColumn("dbo.product_data", "series_brand", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_data", "series_brand");
            DropColumn("dbo.product_data", "series");
            DropColumn("dbo.product_data", "category_brand");
            DropColumn("dbo.product_data", "category");
        }
    }
}
