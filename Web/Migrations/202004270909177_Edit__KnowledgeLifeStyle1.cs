namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__KnowledgeLifeStyle1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.knowledge_lifestyle", "banner", c => c.String());
            AddColumn("dbo.knowledge_lifestyle", "banner_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.knowledge_lifestyle", "banner_alt");
            DropColumn("dbo.knowledge_lifestyle", "banner");
        }
    }
}
