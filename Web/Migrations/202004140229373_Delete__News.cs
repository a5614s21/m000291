namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Delete__News : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.news");
            DropTable("dbo.news_category");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.news_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        en_title = c.String(maxLength: 255),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.news",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        video = c.String(),
                        startdate = c.DateTime(),
                        enddate = c.DateTime(),
                        modifydate = c.DateTime(),
                        sticky = c.String(maxLength: 1),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                        infinity = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
    }
}
