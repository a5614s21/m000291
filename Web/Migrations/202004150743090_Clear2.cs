namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Clear2 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.contacts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.contacts",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        en_title = c.String(maxLength: 255),
                        content = c.String(storeType: "ntext"),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
    }
}
