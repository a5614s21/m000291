namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__News6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "index_sticky", c => c.String(maxLength: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "index_sticky");
        }
    }
}
