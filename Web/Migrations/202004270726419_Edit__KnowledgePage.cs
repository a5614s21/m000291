namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__KnowledgePage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.knowledge_lifestyle", "star", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.knowledge_lifestyle", "star");
        }
    }
}
