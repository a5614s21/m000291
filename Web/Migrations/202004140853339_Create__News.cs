namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__News : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.news",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        subtitle = c.String(),
                        content = c.String(),
                        startdate = c.DateTime(),
                        enddate = c.DateTime(),
                        sortIndex = c.Int(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        seo_keyword = c.String(),
                        seo_description = c.String(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.news");
        }
    }
}
