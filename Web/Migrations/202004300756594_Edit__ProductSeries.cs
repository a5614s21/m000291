namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__ProductSeries : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_series", "brand", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_series", "brand");
        }
    }
}
