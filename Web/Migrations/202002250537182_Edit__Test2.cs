namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__Test2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.news", "NewsCategory_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.news", "NewsCategory_Id", c => c.Int(nullable: false));
        }
    }
}
