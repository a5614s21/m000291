namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteProductSubCategory : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.product_subcategory");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.product_subcategory",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        category = c.String(),
                        sortIndex = c.Int(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
    }
}
