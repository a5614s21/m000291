namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__WebData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "facebook", c => c.String(maxLength: 255));
            AddColumn("dbo.web_data", "instagram", c => c.String(maxLength: 255));
            AddColumn("dbo.web_data", "line", c => c.String(maxLength: 255));
            AddColumn("dbo.web_data", "shop", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "shop");
            DropColumn("dbo.web_data", "line");
            DropColumn("dbo.web_data", "instagram");
            DropColumn("dbo.web_data", "facebook");
        }
    }
}
