namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__VideoData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.video_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        category = c.String(),
                        subtitle = c.String(),
                        content = c.String(),
                        brandpic = c.String(),
                        brandpic_alt = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        url = c.String(),
                        startdate = c.DateTime(),
                        enddate = c.DateTime(),
                        sortIndex = c.Int(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        seo_keyword = c.String(),
                        seo_description = c.String(),
                        sticky = c.String(maxLength: 1),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            AddColumn("dbo.news", "category", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "category");
            DropTable("dbo.video_data");
        }
    }
}
