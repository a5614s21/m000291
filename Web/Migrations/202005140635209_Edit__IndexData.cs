namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__IndexData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.index_data", "etitle", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.index_data", "etitle");
        }
    }
}
