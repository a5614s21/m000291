namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__ProductSeries3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.product_series", "indexpic", c => c.String());
            AddColumn("dbo.product_series", "indexpic_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.product_series", "indexpic_alt");
            DropColumn("dbo.product_series", "indexpic");
        }
    }
}
