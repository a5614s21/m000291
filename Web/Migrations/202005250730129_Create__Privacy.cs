namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__Privacy : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.privacy",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(storeType: "ntext"),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.privacy");
        }
    }
}
