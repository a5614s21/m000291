namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__VideoData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.video_data", "index_sticky", c => c.String(maxLength: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.video_data", "index_sticky");
        }
    }
}
