namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__KnowledgeCatalog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.knowledge_catalog",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        indexpic = c.String(),
                        indexpic_alt = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        file = c.String(),
                        file_alt = c.String(),
                        startdate = c.DateTime(),
                        enddate = c.DateTime(),
                        sortIndex = c.Int(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.knowledge_catalog");
        }
    }
}
