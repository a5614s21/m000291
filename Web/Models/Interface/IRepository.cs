﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Web.Models.Interface
{
    public interface IRepository<TEntity> : IDisposable
        where TEntity : class
    {
        TEntity GetData(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> GetAllData();

        IQueryable<TEntity> GetAllDataByPredicate(Expression<Func<TEntity, bool>> predicate);
    }
}