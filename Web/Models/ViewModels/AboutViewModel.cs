﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public about About_ViewModel { get; set; }

        public List<course> Course_ListViewModel { get; set; }

        public List<certificate> Certificate_ListViewModel { get; set; }
    }
}