﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.ViewModels
{
    public class ContactViewModel : BaseViewModel
    {
        public List<business_location> BusinessLocation_ListViewModel { get; set; }

        public company_info CompanyInfo_ViewModel { get; set; }

        public Contact_PostViewModel Contact_PostViewModel { get; set; }

        public List<ContactProduct_ListViewModel> ContactProduct_ListViewModel { get; set; }
    }

    public class Contact_PostViewModel
    {
        public string company { get; set; }

        [Required]
        public string name { get; set; }

        [Required]
        public string phone { get; set; }

        public string fax { get; set; }

        [Required]
        public string email { get; set; }

        public string address { get; set; }

        public List<string> product { get; set; }

        public string message { get; set; }
    }

    public class ContactProduct_ListViewModel
    {
        public string guid { get; set; }

        public string title { get; set; }

        public string model { get; set; }

        public string pic { get; set; }
    }
}