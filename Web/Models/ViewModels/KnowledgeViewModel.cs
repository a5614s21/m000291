﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.ViewModels
{
    public class KnowledgeViewModel : BaseViewModel
    {
        public knowledge_page KnowledgePage_ViewModel { get; set; }

        public List<knowledge_data> KnowledgeData_ListViewModel { get; set; }

        public IPagedList<knowledge_data> KnowledgeData_PagedListViewModel { get; set; }

        public List<knowledge_catalog> KnowledgeCatalog_ListViewModel { get; set; }

        public IPagedList<knowledge_catalog> KnowledgeCatalog_PagedListViewModel { get; set; }

        public List<knowledge_lifestyle> KnowledgeLifestyle_ListViewModel { get; set; }

        public IPagedList<knowledge_lifestyle> KnowledgeLifestyle_PagedListViewModel { get; set; }

        public knowledge_lifestyle KnowledgeLifestyle_ViewModel { get; set; }

        public knowledge_lifestyle KnowledgeLifestyle_NextViewModel { get; set; }

        public knowledge_lifestyle KnowledgeLifestyle_PrevViewModel { get; set; }
    }
}