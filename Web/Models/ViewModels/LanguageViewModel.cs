﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Web.Models.ViewModels
{
    public enum LanguageEnum
    {
        [Description("zh-TW")]
        Chinese = 1,

        [Description("en-US")]
        English = 2,
    }
}