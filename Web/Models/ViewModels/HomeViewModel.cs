﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        public index_data IndexData_ViewModel { get; set; }
        public List<index_banner> IndexBanner_ListViewModel { get; set; }
        public List<product_data> SearchProduct_ListViewModel { get; set; }

        public List<news> IndexNews_ListViewModel { get; set; }

        public List<news_category> IndexNewsCategory_ListViewModel { get; set; }

        public List<video_data> IndexVideoData_ListViewModel { get; set; }

        public IPagedList<product_data> SearchProduct_PagedListViewModel { get; set; }

        public List<product_series> IndexProductSeries_ListViewModel { get; set; }

        public privacy Privacy_ViewModel { get; set; }

        public web_data WebData_ViewModel { get; set; }
    }
}