﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.ViewModels
{
    public class ProductViewModel : BaseViewModel
    {
        public Dictionary<product_category, List<product_brand>> ProductCategory_ListViewModel { get; set; }
        public Dictionary<product_category, List<product_series>> ProductSeriesCategory_ListViewModel { get; set; }
        public Dictionary<product_series, List<product_brand>> ProductSeries_ListViewModel { get; set; }

        public List<product_series> ProductCateSeries_ListViewModel { get; set; }

        public product_category ProductCategory_ViewModel { get; set; }
        public List<product_category> ProductCategoryAll_ViewModel { get; set; }

        public List<product_brand> ProductBrand_ListViewModel { get; set; }

        public List<product_data> ProductData_ListViewModel { get; set; }

        public IPagedList<product_data> ProductData_PagedListViewModel { get; set; }

        public product_series ProductSeries_ViewModel { get; set; }

        public product_data ProductData_ViewModel { get; set; }

        public product_data ProductData_NextViewModel { get; set; }

        public product_data ProductData_PrevViewModel { get; set; }

        public List<product_icondata> ProductIconData_ListViewModel { get; set; }

        public List<video_data> ProductVideoData_ListViewModel { get; set; }

        public List<product_data> ProductRelatedData_ListViewModel { get; set; }

        public List<notepad> Notepad_ListViewModel { get; set; }

        //public List<notepad_data> NotepadData_PDFListViewModel { get; set; }

        public Dictionary<notepad_data, product_data> NotepadData_PDFListViewModel { get;set; }
    }
}