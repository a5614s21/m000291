﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.ViewModels
{
    public class NewsViewModel : BaseViewModel
    {
        public List<news_category> NewsCategory_ListViewModel { get; set; }

        public news_category NewsCategory_ViewModel { get; set; }

        public List<news> News_ListViewModel { get; set; }

        public IPagedList<news> News_PagedListViewModel { get; set; }

        public news News_ViewModel { get; set; }

        public news News_NextViewModel { get; set; }

        public news News_PrevViewModel { get; set; }
    }
}