﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.ViewModels
{
    public class VideoViewModel : BaseViewModel
    {
        public List<video_category> VideoCategory_ListViewModel { get; set; }

        public video_category VideoCategory_ViewModel { get; set; }

        public List<video_data> VideoData_ListViewModel { get; set; }

        public IPagedList<video_data> VideoData_PagedListViewModel { get; set; }

        public video_data VideoData_ViewModel { get; set; }
    }
}