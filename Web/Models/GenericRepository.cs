﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Web.Models.EntityModel;
using Web.Models.Interface;

namespace Web.Models
{
    public class GenericRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        #region -- 預設 --

        public GenericRepository()
           : this(new Model())
        {
        }

        public GenericRepository(DbContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            this._context = context;
        }

        public GenericRepository(ObjectContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            this._context = new DbContext(context, true);
        }

        private DbContext _context
        {
            get;
            set;
        }

        #endregion

        #region -- Dispose 釋放記憶體 --

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._context != null)
                {
                    this._context.Dispose();
                    this._context = null;
                }
            }
        }

        #endregion

        public IQueryable<TEntity> GetAllData()
        {
            return this._context.Set<TEntity>().AsQueryable();
        }

        public IQueryable<TEntity> GetAllDataByPredicate(Expression<Func<TEntity, bool>> predicate)
        {
            return this._context.Set<TEntity>().Where(predicate).AsQueryable();
        }

        //public IEnumerable<TEntity> GetAllDataByGuid(string guid)
        //{
        //    return this.GetAllData().Where(x => x.guid == guid).AsEnumerable();
        //}

        public TEntity GetData(Expression<Func<TEntity, bool>> predicate)
        {
            return this._context.Set<TEntity>().FirstOrDefault(predicate);
        }
    }
}