﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.Repository
{
    public class knowledgeCatalogRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("indexpic", "[{'subject': '列表圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 360 x 500 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("pic", "[{'subject': '型錄圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 690 x 960 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");
            media.Add("file", "[{'subject': '檔案','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">限PDF檔</small>','filetype': 'application/pdf','multiple': 'n','useLang':'Y'}]");
            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("startdate", "[{'subject': '發佈日期','type': 'dates','defaultVal': '" + DateTime.Now.ToString("yyyy/MM/dd") + "','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
            other.Add("enddate", "[{'subject': '下架日期','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");
            other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("indexpic", "列表圖片");
            re.Add("info", "內容摘要");
            re.Add("startdate", "發佈日期");
            re.Add("enddate", "下架日期");
            re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "desc");

            return re;
        }
    }

    public class KnowledgeCatalogService : GenericRepository<knowledge_catalog>
    {
        private Model _context = new Model();

        public List<knowledge_catalog> GetAllData_KnowledgeCatalog(string lang)
        {
            try
            {
                var now = DateTime.Now;
                var result = _context.knowledge_catalog.Where(x => x.lang == lang && x.status == "Y" && x.startdate <= now && ((x.enddate != null && x.enddate >= now) || x.enddate == null))
                                                       .OrderBy(x => x.sortIndex).ThenByDescending(x => x.startdate).ThenByDescending(x => x.create_date).ToList();

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Dictionary<string, List<string>> GetDataPicByGuid(string lang, string guid)
        {
            try
            {
                Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
                var r = _context.knowledge_catalog.Where(x => x.lang == lang && x.status == "Y" && x.guid == guid).SingleOrDefault();

                List<string> pic = new List<string>();
                List<string> pic_alt = new List<string>();

                if (!string.IsNullOrEmpty(r.pic))
                {
                    pic = r.pic.Split(',').ToList();
                }

                if (!string.IsNullOrEmpty(r.pic_alt))
                {
                    pic_alt = r.pic_alt.Split(',').ToList();
                }

                result.Add("pic", pic);
                result.Add("pic_alt", pic_alt);

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}