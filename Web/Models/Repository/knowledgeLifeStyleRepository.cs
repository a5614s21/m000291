﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.Repository
{
    public class knowledgeLifeStyleRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("subtitle", "[{'subject': '描述','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("content", "[{'subject': '內容介紹','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("pic", "[{'subject': '列表圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 870 x 655 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("banner", "[{'subject': 'BANNER','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 1920 x 600 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("startdate", "[{'subject': '發佈日期','type': 'dates','defaultVal': '" + DateTime.Now.ToString("yyyy/MM/dd") + "','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
            other.Add("enddate", "[{'subject': '下架日期','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
            other.Add("sticky", "[{'subject': '置頂顯示','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">啟用此選項文章將被優先排序到最前面</small>','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("star", "[{'subject': '熱門推薦數','type': 'text','defaultVal': '1','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如有啟用置頂，請輸入熱門推薦星星數(預設為一顆星)</small>','useLang':'Y'}]");
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");
            other.Add("seo_keyword", "[{'subject': 'SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            other.Add("seo_description", "[{'subject': 'SEO敘述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("pic", "列表圖片");
            re.Add("info", "內容摘要");
            re.Add("startdate", "發佈日期");
            re.Add("enddate", "下架日期");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "startdate");
            re.Add("orderByType", "desc");

            return re;
        }
    }

    public class KnowledgeLifeStyleService : GenericRepository<knowledge_lifestyle>
    {
        private Model _context = new Model();

        public List<knowledge_lifestyle> GetAllData_KnowledgeLifestyle(string lang)
        {
            try
            {
                var now = DateTime.Now;
                var result = _context.knowledge_lifestyle.Where(x => x.lang == lang && x.status == "Y"
                                                             && x.startdate <= now && ((x.enddate != null && x.enddate >= now) || x.enddate == null))
                                                                 .OrderByDescending(x => x.sticky).ThenByDescending(x => x.startdate).ThenByDescending(x => x.create_date).ToList();

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Dictionary<string, knowledge_lifestyle> GetNextPrev(string lang, string guid)
        {
            try
            {
                var now = DateTime.Now;
                var result = _context.knowledge_lifestyle.Where(x => x.lang == lang && x.status == "Y"
                                                             && x.startdate <= now && ((x.enddate != null && x.enddate >= now) || x.enddate == null))
                                                             .OrderByDescending(x => x.sticky).ThenByDescending(x => x.startdate).ThenByDescending(x => x.create_date).ToList();

                Dictionary<int, knowledge_lifestyle> D = new Dictionary<int, knowledge_lifestyle>();
                var num = 1;
                foreach (var item in result)
                {
                    D.Add(num, item);
                    num++;
                }

                var key = D.Where(x => x.Value.guid == guid).Select(x => x.Key).SingleOrDefault();
                var prev = D.Where(x => x.Key == (key - 1)).Select(x => x.Value).SingleOrDefault();
                if (prev == null)
                {
                    prev = D.OrderByDescending(x => x.Key).Select(x => x.Value).FirstOrDefault();
                }

                var next = D.Where(x => x.Key == (key + 1)).Select(x => x.Value).SingleOrDefault();
                if (next == null)
                {
                    next = D.OrderBy(x => x.Key).Select(x => x.Value).FirstOrDefault();
                }

                Dictionary<string, knowledge_lifestyle> PrevNext = new Dictionary<string, knowledge_lifestyle>();
                PrevNext.Add("Prev", prev);
                PrevNext.Add("Next", next);
                return PrevNext;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}