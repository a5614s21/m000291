﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.Repository
{
    public class aboutRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("content_1", "[{'subject': '關於輝綉集團','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '', 'useLang': 'Y'}]");
            main.Add("content_2", "[{'subject': '專業化管理','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '', 'useLang': 'Y'}]");
            main.Add("content_3", "[{'subject': '產品系列','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '', 'useLang': 'Y'}]");
            main.Add("content_4", "[{'subject': '企業責任','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '', 'useLang': 'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");
            other.Add("title", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }
    }

    public class AboutService : GenericRepository<about>
    {
    }
}