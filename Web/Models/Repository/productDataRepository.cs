﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;
using Web.Models.ViewModels;

namespace Web.Models.Repository
{
    public class productDataRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '品名','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("model", "[{'subject': '型號','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("product_category", "[{'subject': '所屬類別','type': 'select','default': '','class': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>','inherit':'product_category'}]");
            //main.Add("category_brand", "[{'subject': '所屬品牌(類別)','type': 'select','default': '','class': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如沒有選擇，前台該類別不會顯示</small>','inherit':'category_brand'}]");
            main.Add("product_series", "[{'subject': '所屬系列','type': 'select','default': '','class': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>','inherit':'product_series_lang'}]");
            //main.Add("series_brand", "[{'subject': '所屬品牌(系列)','type': 'select','default': '','class': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如沒有選擇，前台該系列不會顯示</small>','inherit':'series_brand'}]");
            main.Add("url", "[{'subject': '購物連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("icondata", "[{ 'subject': 'icon資料','type': 'selectMultiple','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'','Val':'', 'inherit':'products_icondata'}]");
            main.Add("spec", "[{'subject': '產品規格(頁面)','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("spec_pdf", "[{'subject': '產品規格(PDF)','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("feature", "[{'subject': '產品特點','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("method", "[{'subject': '使用方法','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("video", "[{ 'subject': '影音專區','type': 'selectMultiple','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'','Val':'', 'inherit':'products_video'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)
            media.Add("indexpic", "[{'subject': '列表圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 320 x 300 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("pic", "[{'subject': '圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 600 x 450 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");
            other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("indexpic", "Icon");
            re.Add("info", "內容摘要");
            re.Add("product_category", "類別");
            //re.Add("category_brand", "品牌(類別)");
            re.Add("product_series", "系列");
            //re.Add("series_brand", "品牌(系列)");
            re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }
    }

    public class ProductDataService : GenericRepository<product_data>
    {
        private Model _context = new Model();

        public Dictionary<string, product_data> GetNextPrevDataByGuid(string lang, string guid, string type)
        {
            try
            {
                List<product_data> dataList = new List<product_data>();
                var data = _context.product_data.Where(x => x.lang == lang && x.guid == guid).SingleOrDefault();
                if (type == "category")
                {
                    //dataList = _context.product_data.Where(x => x.lang == lang && x.status == "Y" && x.product_category == data.product_category && x.category_brand == data.category_brand).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                    dataList = _context.product_data.Where(x => x.lang == lang && x.status == "Y" && x.product_category == data.product_category).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                }
                else if (type == "series")
                {
                    //dataList = _context.product_data.Where(x => x.lang == lang && x.status == "Y" && x.product_series == data.product_series && x.series_brand == data.series_brand).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                    dataList = _context.product_data.Where(x => x.lang == lang && x.status == "Y" && x.product_series == data.product_series).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                }

                Dictionary<int, product_data> D = new Dictionary<int, product_data>();
                var num = 1;
                foreach (var item in dataList)
                {
                    D.Add(num, item);
                    num++;
                }

                var key = D.Where(x => x.Value.guid == data.guid).Select(x => x.Key).SingleOrDefault();
                var prev = D.Where(x => x.Key == (key - 1)).Select(x => x.Value).SingleOrDefault();
                if (prev == null)
                {
                    prev = D.OrderByDescending(x => x.Key).Select(x => x.Value).FirstOrDefault();
                }

                var next = D.Where(x => x.Key == (key + 1)).Select(x => x.Value).SingleOrDefault();
                if (next == null)
                {
                    next = D.OrderBy(x => x.Key).Select(x => x.Value).FirstOrDefault();
                }

                Dictionary<string, product_data> PrevNext = new Dictionary<string, product_data>();
                PrevNext.Add("Prev", prev);
                PrevNext.Add("Next", next);

                return PrevNext;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_data> GetRelatedDataByProductData(string lang, string type, product_data Model)
        {
            try
            {
                List<product_data> result = new List<product_data>();
                if (type == "category")
                {
                    result = _context.product_data.Where(x => x.lang == lang && x.status == "Y" && x.product_category == Model.product_category && x.category_brand == Model.category_brand).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                    return result;
                }
                else if (type == "series")
                {
                    result = _context.product_data.Where(x => x.lang == lang && x.status == "Y" && x.product_series == Model.product_series && x.series_brand == Model.series_brand).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ContactProduct_ListViewModel> GetContactProduct(string lang, List<string> guid)
        {
            try
            {
                List<ContactProduct_ListViewModel> result = new List<ContactProduct_ListViewModel>();
                if (guid != null && guid.Count != 0)
                {
                    foreach (var item in guid)
                    {
                        ContactProduct_ListViewModel model = new ContactProduct_ListViewModel();
                        var productdata = _context.product_data.Where(x => x.guid == item && x.lang == lang).SingleOrDefault();
                        model.guid = productdata.guid;
                        model.title = productdata.title;
                        model.model = productdata.model;
                        model.pic = productdata.indexpic;

                        result.Add(model);
                    }

                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_data> GetDataByContactProduct(string productguid)
        {
            try
            {
                List<product_data> result = new List<product_data>();
                if (!string.IsNullOrEmpty(productguid))
                {
                    var guidlist = productguid.Split(',');
                    foreach (var item in guidlist)
                    {
                        var data = _context.product_data.Where(x => x.lang == "tw" && x.guid == item).SingleOrDefault();
                        result.Add(data);
                    }

                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_data> GetAllDataBySeries(string lang, string series)
        {
            try
            {
                List<product_data> result = new List<product_data>();
                var product_series = _context.product_series.Where(x => x.lang == lang && x.status == "Y" && x.guid == series).SingleOrDefault();
                //if (!string.IsNullOrEmpty(product_series.brand))
                //{
                //    var brand = product_series.brand.Split(',');
                //    foreach (var item in brand)
                //    {
                //        if (!string.IsNullOrEmpty(item))
                //        {
                //            var data = _context.product_data.Where(x => x.lang == lang && x.status == "Y" && x.product_series == series && x.series_brand == item).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                //            result.AddRange(data);
                //        }
                //    }

                //    return result;
                //}

                //return null;
                var data = _context.product_data.Where(x => x.lang == lang && x.status == "Y" && x.product_series == series).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                result.AddRange(data);

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_data> GetAllDataByCategory(string lang, string category)
        {
            try
            {
                List<product_data> result = new List<product_data>();
                var product_category = _context.product_category.Where(x => x.lang == lang && x.status == "Y" && x.guid == category).SingleOrDefault();
                if (!string.IsNullOrEmpty(product_category.brand))
                {
                    var brand = product_category.brand.Split(',');
                    foreach (var item in brand)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            var data = _context.product_data.Where(x => x.lang == lang && x.status == "Y" && x.product_category == category && x.category_brand == item).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                            result.AddRange(data);
                        }
                    }

                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }

    public class NotePadService : GenericRepository<notepad>
    {
        private Model _context = new Model();

        public bool InsertNotepad(string guid, string notepadname)
        {
            try
            {
                notepad n = new notepad();
                n.guid = guid;
                n.title = notepadname;
                n.create_date = DateTime.Now;
                n.modifydate = DateTime.Now;
                n.lang = "tw";

                _context.Entry(n).State = System.Data.Entity.EntityState.Added;
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Dictionary<notepad, List<notepad_data>> GetDataByCookie(string lang, List<string> guidList)
        {
            try
            {
                Dictionary<notepad, List<notepad_data>> result = new Dictionary<notepad, List<notepad_data>>();

                foreach (var item in guidList)
                {
                    var notepad = _context.notepad.Where(x => x.guid == item).SingleOrDefault();
                    if (notepad != null)
                    {
                        var notepad_data = _context.notepad_data.Where(x => x.notepad_guid == notepad.guid).OrderBy(x => x.create_date).ToList();

                        result.Add(notepad, notepad_data);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<notepad> GetDataByGuidList(List<string> guidList)
        {
            try
            {
                List<notepad> result = new List<notepad>();
                if (guidList != null && guidList.Count != 0)
                {
                    foreach (var item in guidList)
                    {
                        var data = _context.notepad.Where(x => x.guid == item).SingleOrDefault();
                        result.Add(data);
                    }

                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool ChangeNotepadName(string guid, string name)
        {
            try
            {
                var data = _context.notepad.Where(x => x.guid == guid).SingleOrDefault();
                if (data != null)
                {
                    data.title = name.ToString();
                    data.modifydate = DateTime.Now;
                    _context.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteNotepad(string guid)
        {
            try
            {
                var data = _context.notepad.Where(x => x.guid == guid).SingleOrDefault();
                if (data != null)
                {
                    var notepad_data = _context.notepad_data.Where(x => x.notepad_guid == guid).ToList();
                    foreach (var item in notepad_data)
                    {
                        _context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                    }

                    _context.Entry(data).State = System.Data.Entity.EntityState.Deleted;
                    _context.SaveChanges();

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

    public class NotePadDataService : GenericRepository<notepad_data>
    {
        private Model _context = new Model();

        public bool InsertData(string lang, string notepad_guid, string product_guid)
        {
            try
            {
                var check = _context.notepad_data.Where(x => x.notepad_guid == notepad_guid && x.product_guid == product_guid).Any();

                if (check == true)
                {
                    return false;
                }
                else
                {
                    var productdata = _context.product_data.Where(x => x.lang == lang && x.guid == product_guid && x.status == "Y").SingleOrDefault();
                    notepad_data n = new notepad_data();
                    n.guid = Guid.NewGuid().ToString();
                    n.notepad_guid = notepad_guid;
                    n.product_guid = productdata.guid;
                    n.product_pic = productdata.indexpic;
                    n.product_title = productdata.title;
                    n.product_model = productdata.model;
                    n.create_date = DateTime.Now;
                    n.modifydate = DateTime.Now;
                    n.lang = "tw";

                    _context.Entry(n).State = System.Data.Entity.EntityState.Added;
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteData(string notepad_guid, string product_guid)
        {
            try
            {
                var data = _context.notepad_data.Where(x => x.notepad_guid == notepad_guid && x.guid == product_guid).SingleOrDefault();
                if (data != null)
                {
                    _context.Entry(data).State = System.Data.Entity.EntityState.Deleted;
                    _context.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Dictionary<notepad_data, product_data> GetPrintDataByNoteGuid(string lang, string guid)
        {
            try
            {
                Dictionary<notepad_data, product_data> result = new Dictionary<notepad_data, product_data>();
                var n_data = _context.notepad_data.Where(x => x.lang == lang && x.notepad_guid == guid).OrderBy(x => x.create_date).ToList();
                foreach (var item in n_data)
                {
                    var p_data = _context.product_data.Where(x => x.lang == lang && x.guid == item.product_guid).SingleOrDefault();
                    result.Add(item, p_data);
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}