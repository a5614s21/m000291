﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.Repository
{
    public class productSeriesRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("subtitle", "[{'subject': '描述','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("brand", "[{ 'subject': '品牌','type': 'selectMultiple','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'','Val':'', 'inherit':'products_brand'}]");
            main.Add("brand", "[{ 'subject': '所屬分類','type': 'select','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'','Val':'', 'inherit':'product_category'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            media.Add("pic", "[{'subject': '圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 360 x 320  (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("indexpic", "[{'subject': '首頁圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 510 x 700 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("banner", "[{'subject': 'BANNER','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 1920 x 600 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("index_sticky", "[{'subject': '首頁顯示','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">啟用此選項將在首頁顯示</small>','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");
            other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("pic", "圖片");
            re.Add("info_onlytitle", "內容摘要");
            re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }
    }

    public class ProductsSeriesService : GenericRepository<product_series>
    {
        private Model _context = new Model();

        public string GetJudgeSeries(string series)
        {
            var temp = (from a in _context.product_series
                        where a.guid == series
                        select a).FirstOrDefault();
            if (temp!= null)
            {
                return temp.guid;
            }
            else
            {
                return null;
            }
        }

        public string GetChangeSeries(string series)
        {
            var temp = (from a in _context.product_series
                        where a.brand.Contains(series)
                        select a).FirstOrDefault();
            if (temp != null)
            {
                return temp.guid;
            }
            else
            {
                return null;
            }
        }
        public Dictionary<product_series, List<product_brand>> GetAllData(string lang)
        {
            try
            {
                Dictionary<product_series, List<product_brand>> result = new Dictionary<product_series, List<product_brand>>();
                var Series = _context.product_series.Where(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

                foreach (var item in Series)
                {
                    if (!string.IsNullOrEmpty(item.brand))
                    {
                        string[] brand_array = item.brand.Split(',');
                        List<product_brand> brand_list = new List<product_brand>();

                        foreach (var brand_data in brand_array)
                        {
                            if (!string.IsNullOrEmpty(brand_data))
                            {
                                var brand = _context.product_brand.Where(x => x.lang == lang && x.status == "Y" && x.guid == brand_data).SingleOrDefault();
                                brand_list.Add(brand);
                            }
                        }
                        result.Add(item, brand_list);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_series> GetAllDataByCate(string lang, product_series Model)
        {
            try
            {
                List<product_series> result = new List<product_series>();
                var Series = _context.product_series.Where(x => x.lang == lang && x.status == "Y" && x.brand == Model.brand).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

                foreach (var item in Series)
                {
                    result.Add(item);
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_category> GetAllCateData(string lang)
        {
            try
            {
                List<product_category> result = new List<product_category>();
                var Category = _context.product_category.Where(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

                foreach (var item in Category)
                {
                    result.Add(item);
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}