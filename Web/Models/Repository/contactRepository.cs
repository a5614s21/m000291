﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using Web.Models.EntityModel;
using Web.Models.ViewModels;

namespace Web.Models.Repository
{
    public class contactRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("company", "[{'subject': '公司名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("name", "[{'subject': '姓名','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("phone", "[{'subject': '聯絡電話','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("fax", "[{'subject': '傳真號碼','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("email", "[{'subject': 'Email','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("address", "[{'subject': '通訊地址','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("message", "[{'subject': '詢問內容','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            media.Add("product", "[{'subject': '感興趣的產品','type': 'contact_product','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'N'}]");
            //media.Add("pic", "[{'subject': '列表圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 400 x 300 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("banner", "[{'subject': 'BANNER','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 1920 x 600 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("response", "[{'subject': '回覆內容','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'N'}]");
            other.Add("remarks", "[{'subject': '備註','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'N'}]");
            other.Add("replystatus", "[{'subject': '發送回覆信件','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'是/否','Val':'Y/N','useLang':'N'}]");
            other.Add("status", "[{'subject': '回覆狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'已回覆/未回覆','Val':'Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("title", "[{'subject': 'title','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            hidden.Add("form_lang", "[{'subject': 'lang','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            hidden.Add("lang", "[{'subject': 'lang','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            //hidden.Add("id", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            hidden.Add("replied", "[{'subject': 'id','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("company_name", "公司名稱 / 姓名");
            //re.Add("company", "公司名稱");
            //re.Add("name", "姓名");
            re.Add("phone_email", "聯絡電話 / Email");
            //re.Add("phone", "聯絡電話");
            //re.Add("email", "Email");
            re.Add("modifydate", "異動時間");
            re.Add("create_date", "建立時間");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "create_date");
            re.Add("orderByType", "desc");

            return re;
        }
    }

    public class ContactService : GenericRepository<contact>
    {
        private Model _context = new Model();

        public bool InserData(string lang, Contact_PostViewModel Model)
        {
            try
            {
                contact c = new contact();
                c.guid = Guid.NewGuid().ToString();
                if (string.IsNullOrEmpty(Model.company))
                {
                    c.title = Model.name;
                }
                else
                {
                    c.title = Model.company + " _ " + Model.name;
                }

                c.company = Model.company;
                c.name = Model.name;
                c.phone = Model.phone;
                c.fax = Model.fax;
                c.address = Model.address;
                c.email = Model.email;
                if (Model.product != null && Model.product.Count != 0)
                {
                    var productguid = "";
                    foreach (var item in Model.product)
                    {
                        productguid += item + ",";
                    }
                    productguid = productguid.TrimEnd(',');
                    c.product = productguid;
                }
                c.message = Model.message;
                c.replystatus = "N";
                c.replied = "N";
                c.modifydate = DateTime.Now;
                c.create_date = DateTime.Now;
                c.status = "N";
                c.form_lang = lang;
                c.lang = "tw";

                _context.Entry(c).State = System.Data.Entity.EntityState.Added;
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void SendMailToAdmin(string lang, Contact_PostViewModel Model, string sMessage)
        {
            try
            {
                var smtpdata = _context.smtp_data.SingleOrDefault();
                var admin = _context.mail_send.Where(x => x.lang == "tw" && x.status == "Y").ToList();
                var adminlist = "";
                if (admin != null && admin.Count != 0)
                {
                    foreach (var item in admin)
                    {
                        adminlist += item.mail + ",";
                    }
                }
                adminlist = adminlist.TrimEnd(',');

                //設定smtp主機
                string smtpAddress = smtpdata.host;
                //設定Port
                int portNumber = Int32.Parse(smtpdata.port);
                bool enableSSL = true;
                //SSL是否啟用
                enableSSL = (smtpdata.smtp_auth == "Y" ? true : false);
                //填入寄送方email和password
                string emailFrom = smtpdata.username;
                string passowrd = smtpdata.password;
                //收信芳email 可以用逗號區分多個收件人
                string emailTo = adminlist;
                string subject = "";
                if (lang == "tw")
                {
                    //主旨
                    subject = "【萊禮生醫科技股份有限公司】線上諮詢表單";
                }
                else if (lang == "en")
                {
                    //主旨
                    subject = "【R&R】Contact Form";
                }

                sMessage = sMessage.Replace("{$company}", Model.company);
                sMessage = sMessage.Replace("{$name}", Model.name);
                sMessage = sMessage.Replace("{$phone}", Model.phone);
                sMessage = sMessage.Replace("{$fax}", Model.fax);
                sMessage = sMessage.Replace("{$email}", Model.email);
                sMessage = sMessage.Replace("{$address}", Model.address);
                sMessage = sMessage.Replace("{$content}", Model.message);

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    mail.Body = sMessage;
                    // 若你的內容是HTML格式，則為True
                    mail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(emailFrom, passowrd);
                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }

        public void SendMailToUser(string lang, Contact_PostViewModel Model, string sMessage)
        {
            try
            {
                var smtpdata = _context.smtp_data.SingleOrDefault();
                //設定smtp主機
                string smtpAddress = smtpdata.host;
                //設定Port
                int portNumber = Int32.Parse(smtpdata.port);
                bool enableSSL = true;
                //SSL是否啟用
                enableSSL = (smtpdata.smtp_auth == "Y" ? true : false);
                //填入寄送方email和password
                string emailFrom = smtpdata.username;
                string passowrd = smtpdata.password;
                //收信芳email 可以用逗號區分多個收件人
                string emailTo = Model.email;
                string subject = "";
                if (lang == "tw")
                {
                    //主旨
                    subject = "【萊禮生醫科技股份有限公司】線上諮詢確認函";
                }
                else if (lang == "en")
                {
                    //主旨
                    subject = "【R&R】Contact Form";
                }

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    mail.Body = sMessage;
                    // 若你的內容是HTML格式，則為True
                    mail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(emailFrom, passowrd);
                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }

        public void SendContactReplyToUser(string lang, contact Model, string sMessage)
        {
            try
            {
                var user = Model.email;

                //SMTP
                var smtpdata = _context.smtp_data.SingleOrDefault();
                //設定smtp主機
                string smtpAddress = smtpdata.host;
                //設定Port
                int portNumber = Int32.Parse(smtpdata.port);
                bool enableSSL = true;
                //SSL是否啟用
                enableSSL = (smtpdata.smtp_auth == "Y" ? true : false);
                //填入寄送方email和password
                string emailFrom = smtpdata.username;
                string password = smtpdata.password;
                //收信方email 可以用豆後區分多個收件人
                string emailTo = user;
                string subject = "";

                if(lang == "tw")
                {
                    subject = "【萊禮生醫科技股份有限公司】線上諮詢回覆表單";
                }
                else if(lang == "en")
                {
                    subject = "【R&R】Contact Form";
                }

                sMessage = sMessage.Replace("{$response}", Model.response);
                sMessage = sMessage.Replace("{$company}", Model.company);
                sMessage = sMessage.Replace("{$name}", Model.name);
                sMessage = sMessage.Replace("{$phone}", Model.phone);
                sMessage = sMessage.Replace("{$fax}", Model.fax);
                sMessage = sMessage.Replace("{$email}", Model.email);
                sMessage = sMessage.Replace("{$address}", Model.address);
                sMessage = sMessage.Replace("{$content}", Model.message);

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    mail.Body = sMessage;
                    // 若你的內容是HTML格式，則為True
                    mail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(emailFrom, password);
                        smtp.EnableSsl = enableSSL;
                        //smtp.UseDefaultCredentials = true;
                        smtp.Send(mail);
                    }
                }

                Model.replied = "Y";
                Model.replydate = DateTime.Now;
                _context.Entry(Model).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }
    }
}