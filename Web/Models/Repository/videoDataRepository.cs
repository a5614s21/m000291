﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.Repository
{
    public class videoDataRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("category", "[{'subject': '所屬分類','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'video_category'}]");
            main.Add("subtitle", "[{'subject': '描述','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("content", "[{'subject': '內容','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("url", "[{'subject': '影片連結','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("indexpic", "[{'subject': '首頁圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 720 x 480 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("listpic", "[{'subject': '列表圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 360 x 270 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("pic", "[{'subject': '圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 360 x 270 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("brandpic", "[{'subject': '品牌圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 200 x 50 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("startdate", "[{'subject': '發佈日期','type': 'dates','defaultVal': '" + DateTime.Now.ToString("yyyy/MM/dd") + "','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
            other.Add("enddate", "[{'subject': '下架日期','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
            other.Add("sticky", "[{'subject': '列表置頂','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">啟用此選項文章將被優先排序到最前面</small>','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("index_sticky", "[{'subject': '首頁顯示','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">啟用此選項文章將在首頁顯示</small>','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");
            other.Add("seo_keyword", "[{'subject': 'SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            other.Add("seo_description", "[{'subject': 'SEO敘述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("listpic", "圖片");
            re.Add("info_onlytitle", "內容摘要");
            re.Add("category", "分類");
            re.Add("startdate", "發佈日期");
            re.Add("enddate", "下架日期");
            re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "startdate");
            re.Add("orderByType", "desc");

            return re;
        }
    }

    public class VideoDataService : GenericRepository<video_data>
    {
        private Model _context = new Model();

        public List<video_data> GetAllDataByCategory(string lang, string category)
        {
            try
            {
                var now = DateTime.Now;
                var result = _context.video_data.Where(x => x.lang == lang && x.status == "Y" && x.category == category
                                                   && x.startdate <= now && ((x.enddate != null && x.enddate >= now) || x.enddate == null))
                                                   .OrderByDescending(x => x.sticky).ThenBy(x => x.sortIndex).ThenByDescending(x => x.startdate).ThenByDescending(x => x.create_date).ToList();

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Dictionary<string, List<video_data>> GetSelectMultipleData()
        {
            try
            {
                Dictionary<string, List<video_data>> result = new Dictionary<string, List<video_data>>();
                var category = _context.video_category.Where(x => x.lang == "tw" && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                foreach (var item in category)
                {
                    var data = _context.video_data.Where(x => x.lang == "tw" && x.status == "Y" && x.category == item.guid).OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                    result.Add(item.title, data);
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<video_data> GetDataByProductData(string lang, product_data Model)
        {
            try
            {
                if (!string.IsNullOrEmpty(Model.video))
                {
                    List<video_data> result = new List<video_data>();
                    var videodata = Model.video.Split(',');

                    foreach (var item in videodata)
                    {
                        var data = _context.video_data.Where(x => x.lang == lang && x.status == "Y" && x.guid == item).SingleOrDefault();
                        if (data != null)
                        {
                            result.Add(data);
                        }
                    }

                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<video_data> GetAllIndexVideoData(string lang)
        {
            try
            {
                List<video_data> result = new List<video_data>();
                var video_category = _context.video_category.Where(x => x.lang == lang && x.status == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                foreach (var item in video_category)
                {
                    var video_data = _context.video_data.Where(x => x.lang == lang && x.status == "Y" && x.category == item.guid && x.index_sticky == "Y").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                    result.AddRange(video_data);
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}