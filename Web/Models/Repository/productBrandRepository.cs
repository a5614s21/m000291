﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.Repository
{
    public class productBrandRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            media.Add("pic", "[{'subject': '圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高 120 x 120 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");
            other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("pic", "圖片");
            re.Add("info", "內容摘要");
            re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }
    }

    public class ProductBrandService : GenericRepository<product_brand>
    {
        private Model _context = new Model();

        public void DeleteBrand_Category(string guid)
        {
            try
            {
                var result = _context.product_category.Where(x => x.brand.Contains(guid)).ToList();
                foreach (var item in result)
                {
                    item.brand = item.brand.Replace(guid, "");
                    _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                }

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
            }
        }

        public void DeleteBrand_Series(string guid)
        {
            try
            {
                var result = _context.product_series.Where(x => x.brand.Contains(guid)).ToList();
                foreach (var item in result)
                {
                    item.brand = item.brand.Replace(guid, "");
                    _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                }

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
            }
        }

        public List<product_brand> GetAllDataByCategoryData(string lang, product_category Model)
        {
            try
            {
                if (!string.IsNullOrEmpty(Model.brand))
                {
                    string[] brand_array = Model.brand.Split(',');
                    List<product_brand> result = new List<product_brand>();

                    foreach (var item in brand_array)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            var brand = _context.product_brand.Where(x => x.lang == lang && x.status == "Y" && x.guid == item).SingleOrDefault();
                            result.Add(brand);
                        }
                    }

                    result = result.OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_brand> GetAllDataBySeriesData(string lang, product_series Model)
        {
            try
            {
                if (!string.IsNullOrEmpty(Model.brand))
                {
                    string[] brand_array = Model.brand.Split(',');
                    List<product_brand> result = new List<product_brand>();

                    foreach (var item in brand_array)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            var brand = _context.product_brand.Where(x => x.lang == lang && x.status == "Y" && x.guid == item).SingleOrDefault();
                            result.Add(brand);
                        }
                    }

                    result = result.OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();
                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_brand> GetCategoryBrandByGuid(string lang, string guid)
        {
            try
            {
                List<product_brand> result = new List<product_brand>();

                var brand = _context.product_category.Where(x => x.lang == lang && x.status == "Y" && x.guid == guid).SingleOrDefault();
                if (brand != null)
                {
                    var brand_array = brand.brand.Split(',');
                    foreach (var item in brand_array)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            var b = _context.product_brand.Where(x => x.lang == lang && x.status == "Y" && x.guid == item).SingleOrDefault();
                            result.Add(b);
                        }
                    }
                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_brand> GetSeriesBrandByGuid(string lang, string guid)
        {
            try
            {
                List<product_brand> result = new List<product_brand>();

                var brand = _context.product_series.Where(x => x.lang == lang && x.status == "Y" && x.guid == guid).SingleOrDefault();

                if (brand != null)
                {
                    var brand_array = brand.brand.Split(',');
                    foreach (var item in brand_array)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            var b = _context.product_brand.Where(x => x.lang == lang && x.status == "Y" && x.guid == item).SingleOrDefault();
                            result.Add(b);
                        }
                    }

                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_brand> GetCategoryBrandDataByProductGuid(string guid)
        {
            try
            {
                List<product_brand> result = new List<product_brand>();

                var productdata = _context.product_data.Where(x => x.lang == "tw" && x.guid == guid).SingleOrDefault();
                var productcategory = _context.product_category.Where(x => x.lang == "tw" && x.status == "Y" && x.guid == productdata.product_category).SingleOrDefault();

                if (productcategory != null)
                {
                    var brand_array = productcategory.brand.Split(',');
                    foreach (var item in brand_array)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            var b = _context.product_brand.Where(x => x.lang == "tw" && x.status == "Y" && x.guid == item).SingleOrDefault();
                            result.Add(b);
                        }
                    }

                    result = result.OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<product_brand> GetSeriesBrandDataByProductGuid(string guid)
        {
            try
            {
                List<product_brand> result = new List<product_brand>();
                var productdata = _context.product_data.Where(x => x.lang == "tw" && x.status == "Y" && x.guid == guid).SingleOrDefault();
                var productcategory = _context.product_series.Where(x => x.lang == "tw" && x.status == "Y" && x.guid == productdata.product_series).SingleOrDefault();

                if (productcategory != null)
                {
                    var brand_array = productcategory.brand.Split(',');
                    foreach (var item in brand_array)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            var b = _context.product_brand.Where(x => x.lang == "tw" && x.status == "Y" && x.guid == item).SingleOrDefault();
                            result.Add(b);
                        }
                    }

                    result = result.OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).ToList();

                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}