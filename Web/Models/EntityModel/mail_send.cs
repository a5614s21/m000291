﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Web.Models.EntityModel
{
    public class mail_send
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string mail { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
    }
}