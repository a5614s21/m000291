﻿namespace Web.Models.EntityModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;

    public class Model : DbContext
    {
        // 您的內容已設定為使用應用程式組態檔 (App.config 或 Web.config)
        // 中的 'Model' 連接字串。根據預設，這個連接字串的目標是
        // 您的 LocalDb 執行個體上的 'Web.Models.Model' 資料庫。
        //
        // 如果您的目標是其他資料庫和 (或) 提供者，請修改
        // 應用程式組態檔中的 'Model' 連接字串。
        public Model()
            : base("name=Model")
        {
        }

        public virtual DbSet<ashcan> ashcan { get; set; }
        public virtual DbSet<google_analytics> google_analytics { get; set; }
        public virtual DbSet<language> language { get; set; }
        public virtual DbSet<role_permissions> role_permissions { get; set; }
        public virtual DbSet<roles> roles { get; set; }
        public virtual DbSet<smtp_data> smtp_data { get; set; }
        public virtual DbSet<system_data> system_data { get; set; }
        public virtual DbSet<system_menu> system_menu { get; set; }
        public virtual DbSet<user> user { get; set; }
        public virtual DbSet<user_role> user_role { get; set; }
        public virtual DbSet<web_data> web_data { get; set; }
        public virtual DbSet<taiwan_city> taiwan_city { get; set; }
        public virtual DbSet<mail_contents> mail_contents { get; set; }
        public virtual DbSet<firewalls> firewalls { get; set; }
        public virtual DbSet<system_log> system_log { get; set; }

        public virtual DbSet<notepad> notepad { get; set; }
        public virtual DbSet<notepad_data> notepad_data { get; set; }

        #region -- 內容管理 --

        public virtual DbSet<index_banner> index_banner { get; set; }

        public virtual DbSet<page_banner> page_banner { get; set; }

        public virtual DbSet<index_data> index_data { get; set; }

        public virtual DbSet<privacy> privacy { get; set; }

        #endregion

        #region -- 公司簡介 --

        //公司簡介
        public virtual DbSet<about> about { get; set; }

        //發展歷程
        public virtual DbSet<course> course { get; set; }

        //專業證書
        public virtual DbSet<certificate> certificate { get; set; }

        #endregion

        #region -- 最新消息 --

        //最新消息類別
        public virtual DbSet<news_category> news_category { get; set; }

        //最新消息內容
        public virtual DbSet<news> news { get; set; }

        #endregion

        #region -- 產品專區 --

        //產品品牌
        public virtual DbSet<product_brand> product_brand { get; set; }

        //產品系列
        public virtual DbSet<product_series> product_series { get; set; }

        //產品分類
        public virtual DbSet<product_category> product_category { get; set; }

        //產品內容
        public virtual DbSet<product_data> product_data { get; set; }

        //產品ICON
        public virtual DbSet<product_icondata> product_icondata { get; set; }

        #endregion

        #region --  資訊小教室 --

        //常見問題靜態頁面
        public virtual DbSet<knowledge_page> knowledge_page { get; set; }

        //常見問題
        public virtual DbSet<knowledge_data> knowledge_data { get; set; }

        //產品型錄
        public virtual DbSet<knowledge_catalog> knowledge_catalog { get; set; }

        //生活趣
        public virtual DbSet<knowledge_lifestyle> knowledge_lifestyle { get; set; }

        #endregion

        #region -- 影音專區 --

        //影音分類
        public virtual DbSet<video_category> video_category { get; set; }

        //影音內容
        public virtual DbSet<video_data> video_data { get; set; }

        #endregion

        #region -- 客戶服務 --

        //銷售據點
        public virtual DbSet<business_location> business_location { get; set; }

        //公司資訊
        public virtual DbSet<company_info> company_info { get; set; }

        //表單轉寄設定
        public virtual DbSet<mail_send> mail_send { get; set; }

        //諮詢表單
        public virtual DbSet<contact> contact { get; set; }

        #endregion

        // Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        // 針對您要包含在模型中的每種實體類型新增 DbSet。如需有關設定和使用
        // Code First 模型的詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=390109。
    }
}