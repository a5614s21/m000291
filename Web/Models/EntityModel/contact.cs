﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Web.Models.EntityModel
{
    public class contact
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string company { get; set; }

        public string name { get; set; }

        public string phone { get; set; }

        public string fax { get; set; }

        public string address { get; set; }

        public string email { get; set; }

        public string product { get; set; }

        [Column(TypeName = "ntext")]
        public string message { get; set; }

        [Column(TypeName = "ntext")]
        public string response { get; set; }

        [Column(TypeName = "ntext")]
        public string remarks { get; set; }

        [StringLength(1)]
        public string replystatus { get; set; }

        [StringLength(1)]
        public string replied { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? replydate { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(30)]
        public string form_lang { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
    }
}