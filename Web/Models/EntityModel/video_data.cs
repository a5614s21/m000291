﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Web.Models.EntityModel
{
    public class video_data
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string category { get; set; }

        public string subtitle { get; set; }

        public string content { get; set; }

        public string brandpic { get; set; }

        public string brandpic_alt { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }

        public string indexpic { get; set; }

        public string indexpic_alt { get; set; }

        public string url { get; set; }

        public DateTime? startdate { get; set; }

        public DateTime? enddate { get; set; }

        public int? sortIndex { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }

        public string seo_keyword { get; set; }

        public string seo_description { get; set; }

        [StringLength(1)]
        public string sticky { get; set; }

        [StringLength(1)]
        public string index_sticky { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string listpic { get; set; }

        public string listpic_alt { get; set; }
    }
}