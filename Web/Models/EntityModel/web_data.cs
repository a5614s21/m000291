namespace Web.Models.EntityModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class web_data
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        [StringLength(200)]
        public string title { get; set; }

        [Column(TypeName = "text")]
        public string url { get; set; }

        [StringLength(255)]
        public string phone { get; set; }

        [StringLength(255)]
        public string fax { get; set; }

        [StringLength(255)]
        public string servicemail { get; set; }

        [StringLength(255)]
        public string headquarters { get; set; }

        [StringLength(255)]
        public string address { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string seo_keywords { get; set; }

        public string seo_description { get; set; }

        [StringLength(255)]
        public string facebook { get; set; }

        [StringLength(255)]
        public string instagram { get; set; }

        [StringLength(255)]
        public string line { get; set; }

        [StringLength(255)]
        public string shop { get; set; }

        public string code { get; set; }

        public string index_announcement { get; set; }
    }
}