﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Web.Models.EntityModel
{
    public class notepad_data
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string notepad_guid { get; set; }

        public string product_guid { get; set; }

        public string product_pic { get; set; }

        public string product_title { get; set; }

        public string product_model { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
    }
}