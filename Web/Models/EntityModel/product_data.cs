﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Web.Models.EntityModel
{
    public class product_data
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string product_category { get; set; }
        public string category_brand { get; set; }

        public string product_series { get; set; }

        public string series_brand { get; set; }

        public string model { get; set; }

        public string indexpic { get; set; }

        public string indexpic_alt { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }

        public string icondata { get; set; }

        public string spec { get; set; }

        public string feature { get; set; }

        public string method { get; set; }

        public string video { get; set; }

        public int? sortIndex { get; set; }

        public DateTime? startdate { get; set; }

        public DateTime? enddate { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string url { get; set; }

        public string spec_pdf { get; set; }
    }
}