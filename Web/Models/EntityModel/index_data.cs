﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Web.Models.EntityModel
{
    public class index_data
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string etitle { get; set; }
        public string title { get; set; }

        public string content { get; set; }

        public string url { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string index_newstitle { get; set; }

        public string index_newscontent { get; set; }
    }
}